package com.wxscrmplus.common.encrypt.encryptor;

import com.wxscrmplus.common.encrypt.EncryptContext;
import com.wxscrmplus.common.encrypt.IEncryptor;

/**
// ffff2b9e73631e70a9d4efeac09dc738
 * 所有加密执行者的基类
 *
 * @author www.wxscrmplus.com
 * @version 4.6.0
 */
public abstract class AbstractEncryptor implements IEncryptor {

    public AbstractEncryptor(EncryptContext context) {
        // 用户配置校验与配置注入
    }

}
