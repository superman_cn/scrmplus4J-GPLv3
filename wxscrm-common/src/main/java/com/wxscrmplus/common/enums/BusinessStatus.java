package com.wxscrmplus.common.enums;

/**
// 1aa3c245653f739b9ea5700c581092cd
 * 操作状态
 *
 * @author www.wxscrmplus.com
 */
public enum BusinessStatus {
    /**
     * 成功
     */
    SUCCESS,

    /**
     * 失败
     */
    FAIL,
}
