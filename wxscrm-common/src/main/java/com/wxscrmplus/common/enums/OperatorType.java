package com.wxscrmplus.common.enums;
// 895b272c53fb0bc5a6ea490006805bba

/**
 * 操作人类别
 *
 * @author www.wxscrmplus.com
 */
public enum OperatorType {
    /**
     * 其它
     */
    OTHER,

    /**
     * 后台用户
     */
    MANAGE,

    /**
     * 手机端用户
     */
    MOBILE
}
