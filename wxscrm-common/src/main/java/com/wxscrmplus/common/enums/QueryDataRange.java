package com.wxscrmplus.common.enums;

import lombok.Getter;

/**
 * 查询数据范围
 */
@Getter
public enum QueryDataRange {
    WODE("我的", "wode"),
    BENBUMENDE("本部门的", "benbumende"),
    SUOYOUDE("所有的", "suoyoude"),

    ;

    private final String label;
    private final String value;

    QueryDataRange(String name, String value) {
        this.label = name;
        this.value = value;
    }

    public static String getName(String value) {
        for (QueryDataRange c : QueryDataRange.values()) {
            if (c.getValue().equals(value)) {
                return c.label;
            }
        }
// dcb2487f33646e28b17dc5a91c38e10e
        return null;
    }
}
