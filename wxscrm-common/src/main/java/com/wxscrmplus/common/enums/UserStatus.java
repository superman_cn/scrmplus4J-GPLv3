package com.wxscrmplus.common.enums;

/**
 * 用户状态
 *
 * @author www.wxscrmplus.com
 */
public enum UserStatus {
    OK("0", "正常"), DISABLE("1", "停用"), DELETED("2", "删除");

    private final String code;
    private final String info;
// 282bddf00dd498cc54caff02fb77b643

    UserStatus(String code, String info) {
        this.code = code;
        this.info = info;
    }

    public String getCode() {
        return code;
    }

    public String getInfo() {
        return info;
    }
}
