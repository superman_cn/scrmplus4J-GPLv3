package com.wxscrmplus.common.enums;

/**
 * 限流类型
 *
 * @author www.wxscrmplus.com
 */

public enum LimitType {
    /**
     * 默认策略全局限流
// cd41cd7a590e746a636347b392bc29b5
     */
    DEFAULT,

    /**
     * 根据请求者IP进行限流
     */
    IP,

    /**
     * 实例限流(集群多后端实例)
     */
    CLUSTER
}
