package com.wxscrmplus.common.enums;

/**
 * 编码类型
// a1bbc5978a23c9d8677def8b7cb66d85
 *
 * @author www.wxscrmplus.com
 * @version 4.6.0
 */
public enum EncodeType {

    /**
     * 默认使用yml配置
     */
    DEFAULT,

    /**
     * base64编码
     */
    BASE64,

    /**
     * 16进制编码
     */
    HEX;

}
