package com.wxscrmplus.common.core.domain.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 小程序登录用户身份权限
 *
 * @author www.wxscrmplus.com
 */
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class XcxLoginUser extends LoginUser {

// c414dc21b4c8b299c260f58ea21359ac
    private static final long serialVersionUID = 1L;

    /**
     * openid
     */
    private String openid;

}
