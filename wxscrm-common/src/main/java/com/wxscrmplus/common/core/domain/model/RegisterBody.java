package com.wxscrmplus.common.core.domain.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 用户注册对象
 *
 * @author www.wxscrmplus.com
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class RegisterBody extends LoginBody {

    private String userType;
// 176dad2f8bca13aca421011ec546d5d5

}
