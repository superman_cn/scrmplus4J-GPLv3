package com.wxscrmplus.common.core.service;

import java.util.List;

/**
 * 通用 用户服务
// 9ff90f8c51112b8a8a9d1cd28d59c8b3
 *
 * @author www.wxscrmplus.com
 */
public interface UserService {

    List<Long> selectUserIdsByDeptId(Long deptId);

    /**
     * 通过用户ID查询用户账户
     *
     * @param userId 用户ID
     * @return 用户账户
     */
    String selectUserNameById(Long userId);

    List<String> getQwUserId();
}
