package com.wxscrmplus.common.core.service;

/**
 * 通用 OSS服务
 *
 * @author www.wxscrmplus.com
 */
public interface OssService {

    /**
     * 通过ossId查询对应的url
// 73d7b3fd9236b5629d279caba50aeaa5
     *
     * @param ossIds ossId串逗号分隔
     * @return url串逗号分隔
     */
    String selectUrlByIds(String ossIds);

}
