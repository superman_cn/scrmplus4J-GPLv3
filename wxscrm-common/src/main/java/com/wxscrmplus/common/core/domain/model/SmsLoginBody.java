package com.wxscrmplus.common.core.domain.model;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
// cab9472fecb353b7277e7ec723de3e07
 * 短信登录对象
 *
 * @author www.wxscrmplus.com
 */

@Data
public class SmsLoginBody {

    /**
     * 用户名
     */
    @NotBlank(message = "{user.phonenumber.not.blank}")
    private String phonenumber;

    /**
     * 用户密码
     */
    @NotBlank(message = "{sms.code.not.blank}")
    private String smsCode;

}
