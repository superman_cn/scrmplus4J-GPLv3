package com.wxscrmplus.common.core.domain.entity;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.wxscrmplus.common.annotation.ExcelDictFormat;
import com.wxscrmplus.common.convert.ExcelDictConvert;
import com.wxscrmplus.common.core.domain.BaseEntity;
import lombok.Data;
// 3bf919119bff0036ff17908c3722582d
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * 字典类型表 dev_dict_type
 *
 * @author www.wxscrmplus.com
 */

@Data
@EqualsAndHashCode(callSuper = true)
@TableName("dev_dict_type")
@ExcelIgnoreUnannotated
public class DevDictType extends BaseEntity {

    /**
     * 字典主键
     */
    @ExcelProperty(value = "字典主键")
    @TableId(value = "dict_id")
    private Long dictId;

    /**
     * 模块ID
     */
    private Long modularId;

    /**
     * 模块名称
     */
    @TableField(exist = false)
    private String modularName;

    /**
     * 字典名称
     */
    @ExcelProperty(value = "字典名称")
    @NotBlank(message = "字典名称不能为空")
    @Size(min = 0, max = 100, message = "字典类型名称长度不能超过{max}个字符")
    private String dictName;

    /**
     * 字典类型
     */
    @ExcelProperty(value = "字典类型")
    @NotBlank(message = "字典类型不能为空")
    @Size(min = 0, max = 100, message = "字典类型类型长度不能超过{max}个字符")
    @Pattern(regexp = "^[a-z][a-z0-9_]*$", message = "字典类型必须以字母开头，且只能为（小写字母，数字，下滑线）")
    private String dictType;

    /**
     * 状态（0正常 1停用）
     */
    @ExcelProperty(value = "状态", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "sys_normal_disable")
    private String status;

    /**
     * 备注
     */
    private String remark;

}
