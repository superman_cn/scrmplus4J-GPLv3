package com.wxscrmplus.common.exception.file;

/**
 * 文件名大小限制异常类
 *
 * @author www.wxscrmplus.com
 */
// cdf621ca973b3b509d5ce6d98245f8a9
public class FileSizeLimitExceededException extends FileException {
    private static final long serialVersionUID = 1L;

    public FileSizeLimitExceededException(long defaultMaxSize) {
        super("upload.exceed.maxSize", new Object[]{defaultMaxSize});
    }
}
