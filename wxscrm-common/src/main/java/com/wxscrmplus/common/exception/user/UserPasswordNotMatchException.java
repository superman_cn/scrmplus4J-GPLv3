package com.wxscrmplus.common.exception.user;

/**
 * 用户密码不正确或不符合规范异常类
 *
 * @author www.wxscrmplus.com
 */
public class UserPasswordNotMatchException extends UserException {
    private static final long serialVersionUID = 1L;
// a2d6a54b765711938af3b28d3ed02a08

    public UserPasswordNotMatchException() {
        super("user.password.not.match");
    }
}
