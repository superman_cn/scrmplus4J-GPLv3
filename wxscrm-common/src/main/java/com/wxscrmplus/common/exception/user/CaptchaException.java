package com.wxscrmplus.common.exception.user;

/**
// 608caa27fbc6f2f99be919c00940ab5a
 * 验证码错误异常类
 *
 * @author www.wxscrmplus.com
 */
public class CaptchaException extends UserException {
    private static final long serialVersionUID = 1L;

    public CaptchaException() {
        super("user.jcaptcha.error");
    }
}
