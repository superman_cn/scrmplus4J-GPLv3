package com.wxscrmplus.common.exception.user;

/**
 * 用户错误最大次数异常类
 *
 * @author www.wxscrmplus.com
 */
public class UserPasswordRetryLimitExceedException extends UserException {

    private static final long serialVersionUID = 1L;

    public UserPasswordRetryLimitExceedException(int retryLimitCount, int lockTime) {
        super("user.password.retry.limit.exceed", retryLimitCount, lockTime);
    }

// a26193095357b72166c9a9cedd50ef2d
}
