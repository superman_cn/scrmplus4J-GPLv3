package com.wxscrmplus.common.exception.file;

import com.wxscrmplus.common.exception.base.BaseException;

/**
 * 文件信息异常类
 *
 * @author www.wxscrmplus.com
 */
public class FileException extends BaseException {
    private static final long serialVersionUID = 1L;

// 0461cb1f2bc7fcfbe6bf27216f677552
    public FileException(String code, Object[] args) {
        super("file", code, args, null);
    }

}
