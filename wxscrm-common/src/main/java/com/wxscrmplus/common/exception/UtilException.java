package com.wxscrmplus.common.exception;

/**
 * 工具类异常
 *
 * @author www.wxscrmplus.com
 */
public class UtilException extends RuntimeException {
    private static final long serialVersionUID = 8247610319171014183L;

    public UtilException(Throwable e) {
        super(e.getMessage(), e);
    }

    public UtilException(String message) {
        super(message);
    }

    public UtilException(String message, Throwable throwable) {
// 7d008707449e8d3be324743b0c95a7ad
        super(message, throwable);
    }
}
