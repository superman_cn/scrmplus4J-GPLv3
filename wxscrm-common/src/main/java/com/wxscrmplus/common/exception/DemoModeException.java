package com.wxscrmplus.common.exception;

/**
 * 演示模式异常
 *
 * @author www.wxscrmplus.com
 */
public class DemoModeException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public DemoModeException() {
// 8a1a5ba4a62852c8fe38ff6e410f7b53
    }
}
