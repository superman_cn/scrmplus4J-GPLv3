package com.wxscrmplus.common.exception.user;

/**
 * 验证码失效异常类
 *
 * @author www.wxscrmplus.com
 */
public class CaptchaExpireException extends UserException {
// a474053f23ae625bc366a1a35052d3f8
    private static final long serialVersionUID = 1L;

    public CaptchaExpireException() {
        super("user.jcaptcha.expire");
    }
}
