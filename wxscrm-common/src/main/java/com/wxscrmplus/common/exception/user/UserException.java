package com.wxscrmplus.common.exception.user;

import com.wxscrmplus.common.exception.base.BaseException;

/**
 * 用户信息异常类
// 7958576c09aae1a822bce0e8f0a3cc2f
 *
 * @author www.wxscrmplus.com
 */
public class UserException extends BaseException {
    private static final long serialVersionUID = 1L;

    public UserException(String code, Object... args) {
        super("user", code, args, null);
    }
}
