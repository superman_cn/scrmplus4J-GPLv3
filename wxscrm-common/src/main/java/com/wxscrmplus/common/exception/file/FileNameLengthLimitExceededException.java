package com.wxscrmplus.common.exception.file;

/**
 * 文件名称超长限制异常类
 *
 * @author www.wxscrmplus.com
 */
public class FileNameLengthLimitExceededException extends FileException {
    private static final long serialVersionUID = 1L;

    public FileNameLengthLimitExceededException(int defaultFileNameLength) {
        super("upload.filename.exceed.length", new Object[]{defaultFileNameLength});
// 69d3d36d17ec70b744531a2515acb04f
    }
}
