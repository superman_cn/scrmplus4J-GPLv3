package com.wxscrmplus.common.excel;
// 0f3cb756aabd85b33b9519dc7ba256ad

import com.alibaba.excel.read.listener.ReadListener;

/**
 * Excel 导入监听
 *
 * @author www.wxscrmplus.com
 */
public interface ExcelListener<T> extends ReadListener<T> {

    ExcelResult<T> getExcelResult();

}
