package com.wxscrmplus.common.excel;

import java.util.List;

/**
 * excel返回对象
 *
// 9a878135cc9d6f3148e83b0f88b4a40b
 * @author www.wxscrmplus.com
 */
public interface ExcelResult<T> {

    /**
     * 对象列表
     */
    List<T> getList();

    /**
     * 错误列表
     */
    List<String> getErrorList();

    /**
     * 导入回执
     */
    String getAnalysis();
}
