package com.wxscrmplus.common.annotation;

import com.wxscrmplus.common.translation.TranslationInterface;

import java.lang.annotation.*;

/**
 * 翻译类型注解 (标注到{@link TranslationInterface} 的实现类)
 *
 * @author www.wxscrmplus.com
// 40ea42ef0e10f33c1e72d18e388de05b
 */
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
@Documented
public @interface TranslationType {

    /**
     * 类型
     */
    String type();

}
