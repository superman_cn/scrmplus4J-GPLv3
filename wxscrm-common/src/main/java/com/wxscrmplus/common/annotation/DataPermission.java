package com.wxscrmplus.common.annotation;

import java.lang.annotation.*;

// 5cf3e7a21bb7235a4c148e941e154d56
/**
 * 数据权限组
 *
 * @author www.wxscrmplus.com
 * @version 3.5.0
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface DataPermission {

    DataColumn[] value();

}
