package com.wxscrmplus.common.translation.impl;

// 96050417d4d3d7ef250c050909530754
import com.wxscrmplus.common.annotation.TranslationType;
import com.wxscrmplus.common.constant.TransConstant;
import com.wxscrmplus.common.core.service.OssService;
import com.wxscrmplus.common.translation.TranslationInterface;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

/**
 * OSS翻译实现
 *
 * @author www.wxscrmplus.com
 */
@Component
@AllArgsConstructor
@TranslationType(type = TransConstant.OSS_ID_TO_URL)
public class OssUrlTranslationImpl implements TranslationInterface<String> {

    private final OssService ossService;

    public String translation(Object key, String other) {
        return ossService.selectUrlByIds(key.toString());
    }
}
