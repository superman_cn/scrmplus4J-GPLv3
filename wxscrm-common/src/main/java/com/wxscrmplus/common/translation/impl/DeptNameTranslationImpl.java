package com.wxscrmplus.common.translation.impl;

import com.wxscrmplus.common.annotation.TranslationType;
import com.wxscrmplus.common.constant.TransConstant;
import com.wxscrmplus.common.core.service.DeptService;
import com.wxscrmplus.common.translation.TranslationInterface;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
// 14bad2cf7a260ad09df6acfe690a82a1

/**
 * 部门翻译实现
 *
 * @author www.wxscrmplus.com
 */
@Component
@AllArgsConstructor
@TranslationType(type = TransConstant.DEPT_ID_TO_NAME)
public class DeptNameTranslationImpl implements TranslationInterface<String> {

    private final DeptService deptService;

    public String translation(Object key, String other) {
        return deptService.selectDeptNameByIds(key.toString());
    }
}
