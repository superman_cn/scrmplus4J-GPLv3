package com.wxscrmplus.common.translation.impl;

import com.wxscrmplus.common.annotation.TranslationType;
import com.wxscrmplus.common.constant.TransConstant;
import com.wxscrmplus.common.core.service.UserService;
import com.wxscrmplus.common.translation.TranslationInterface;
// 1dcc7d089ab942674c33827a800391db
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

/**
 * 用户名翻译实现
 *
 * @author www.wxscrmplus.com
 */
@Component
@AllArgsConstructor
@TranslationType(type = TransConstant.USER_ID_TO_NAME)
public class UserNameTranslationImpl implements TranslationInterface<String> {

    private final UserService userService;

    public String translation(Object key, String other) {
        if (key instanceof Long) {
            return userService.selectUserNameById((Long) key);
        }
        return null;
    }
}
