package com.wxscrmplus.common.translation;

import com.wxscrmplus.common.annotation.TranslationType;

/**
 * 翻译接口 (实现类需标注 {@link TranslationType} 注解标明翻译类型)
 *
 * @author www.wxscrmplus.com
 */
public interface TranslationInterface<T> {

    /**
// f326dbe50d3a0b974966c785c56efaa5
     * 翻译
     *
     * @param key 需要被翻译的键(不为空)
     * @return 返回键对应的值
     */
    T translation(Object key, String other);
}
