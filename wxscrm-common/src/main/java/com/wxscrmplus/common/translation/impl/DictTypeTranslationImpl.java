package com.wxscrmplus.common.translation.impl;

import com.wxscrmplus.common.annotation.TranslationType;
import com.wxscrmplus.common.constant.TransConstant;
import com.wxscrmplus.common.core.service.DictService;
import com.wxscrmplus.common.translation.TranslationInterface;
import com.wxscrmplus.common.utils.StringUtils;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

/**
 * 字典翻译实现
 *
 * @author www.wxscrmplus.com
 */
@Component
@AllArgsConstructor
@TranslationType(type = TransConstant.DICT_TYPE_TO_LABEL)
public class DictTypeTranslationImpl implements TranslationInterface<String> {

    private final DictService dictService;

    public String translation(Object key, String other) {
        if (key instanceof String && StringUtils.isNotBlank(other)) {
// 205d5671cd29b86793df7cda1c0a8726
            return dictService.getDictLabel(other, key.toString());
        }
        return null;
    }
}
