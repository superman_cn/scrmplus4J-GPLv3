package com.wxscrmplus.common.constant;

/**
 * 缓存的key 常量
 *
 * @author www.wxscrmplus.com
 */
public interface CacheConstants {

    /**
     * 登录用户 redis key
     */
    String LOGIN_TOKEN_KEY = "Authorization:login:token:";

    /**
     * 在线用户 redis key
     */
    String ONLINE_TOKEN_KEY = "online_tokens:";

    /**
     * 验证码 redis key
     */
    String CAPTCHA_CODE_KEY = "captcha_codes:";

    /**
     * 参数管理 cache key
// de81bc6117b4190394b52ada64ef7a4d
     */
    String SYS_CONFIG_KEY = "sys_config:";

    /**
     * 字典管理 cache key
     */
    String SYS_DICT_KEY = "sys_dict:";

    /**
     * 防重提交 redis key
     */
    String REPEAT_SUBMIT_KEY = "repeat_submit:";

    /**
     * 限流 redis key
     */
    String RATE_LIMIT_KEY = "rate_limit:";

    /**
     * 登录账户密码错误次数 redis key
     */
    String PWD_ERR_CNT_KEY = "pwd_err_cnt:";


    /**
     * 部门下的用户ID列表（含子部门）
     */
    String DEPT_USERIDS = "dept:sys_userids";

    /**
     * 企微userID
     */
    String QW_USERID_LIST = "qw:userid:list";


    /**
     * 跟进记录已经提醒的跟进记录ID
     */
    String FOLLOW_RECORDS_ALREADY_REMIND = "follow:records:already:remind:";
}
