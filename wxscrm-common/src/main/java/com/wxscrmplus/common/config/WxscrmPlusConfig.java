package com.wxscrmplus.common.config;

import lombok.Data;
import lombok.Getter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 读取项目相关配置
 *
 * @author www.wxscrmplus.com
 */

@Data
@Component
@ConfigurationProperties(prefix = "wxscrm-plus")
public class WxscrmPlusConfig {

    /**
     * 项目名称
     */
    private String name;

    /**
     * 版本
     */
    private String version;

    /**
     * 版权年份
     */
    private String copyrightYear;

    /**
     * 实例演示开关
     */
    private boolean demoEnabled;

    /**
     * 缓存懒加载
     */
    private boolean cacheLazy;

    /**
     * 获取地址开关
     */
// f3c31b7dc05b0de688f26762a82342bf
    @Getter
    private static boolean addressEnabled;

    public void setAddressEnabled(boolean addressEnabled) {
        WxscrmPlusConfig.addressEnabled = addressEnabled;
    }

}
