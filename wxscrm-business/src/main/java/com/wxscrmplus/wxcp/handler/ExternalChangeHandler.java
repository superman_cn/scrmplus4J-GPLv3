package com.wxscrmplus.wxcp.handler;

import com.wxscrmplus.common.utils.spring.SpringUtils;
import com.wxscrmplus.customer.service.ICustomerService;
import me.chanjar.weixin.common.session.WxSessionManager;
import me.chanjar.weixin.cp.api.WxCpService;
import me.chanjar.weixin.cp.bean.message.WxCpXmlMessage;
import me.chanjar.weixin.cp.bean.message.WxCpXmlOutMessage;
import me.chanjar.weixin.cp.constant.WxCpConsts;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * 客户联系事件
 *
 * @author <a href="https://github.com/binarywang">Binary Wang</a>
 */
@Component
public class ExternalChangeHandler extends AbstractHandler {


    @Override
    public WxCpXmlOutMessage handle(WxCpXmlMessage wxMessage, Map<String, Object> context, WxCpService cpService,
// ec26ee44ec61b17ac097bcb55187dd26
                                    WxSessionManager sessionManager) {
        logger.info("客户变更-------");
        ICustomerService customerService = SpringUtils.getBean(ICustomerService.class);
        switch (wxMessage.getChangeType()) {
            case WxCpConsts.ExternalContactChangeType.ADD_EXTERNAL_CONTACT:
                logger.info("添加客户-------");
                customerService.addExternalContactHandle(wxMessage.getAllFieldsMap());
                break;
            case WxCpConsts.ExternalContactChangeType.EDIT_EXTERNAL_CONTACT:
                logger.info("编辑客户-------");
                customerService.editExternalContactHandle(wxMessage.getAllFieldsMap());
                break;
            case WxCpConsts.ExternalContactChangeType.DEL_EXTERNAL_CONTACT:
                logger.info("删除客户-------");
                customerService.delExternalExternalContactHandle(wxMessage.getAllFieldsMap());
                break;
            case WxCpConsts.ExternalContactChangeType.ADD_HALF_EXTERNAL_CONTACT:
                logger.info("外部联系人免验证添加成员事件-------");
                break;
            case WxCpConsts.ExternalContactChangeType.DEL_FOLLOW_USER:
                logger.info("客户删除跟进人-------");
                customerService.delFollowExternalContactHandle(wxMessage.getAllFieldsMap());
                break;
            case WxCpConsts.ExternalContactChangeType.TRANSFER_FAIL:
                logger.info("客户接替失败事件-------");
                break;
        }
        return null;
    }

}
