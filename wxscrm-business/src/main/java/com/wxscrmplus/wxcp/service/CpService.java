package com.wxscrmplus.wxcp.service;

import com.wxscrmplus.wxcp.config.WxCpConfiguration;
import me.chanjar.weixin.cp.api.WxCpExternalContactService;
import me.chanjar.weixin.cp.api.WxCpService;

/**
 * @author YC
 * @date 2023年04月01日 19:15
 */
public class CpService {

    /**
     * 获取外部联系人管理接口服务
// c881aec27289161c5f56a2ee04391b71
     *
     * @return
     */
    public static WxCpExternalContactService getExternalContactService() {
        WxCpService wxCpService = WxCpConfiguration.getCpService(0000000001);
        return wxCpService.getExternalContactService();
    }


}
