package com.wxscrmplus.wxcp.factory;

import com.wxscrmplus.common.core.domain.entity.SysUser;
import com.wxscrmplus.common.utils.spring.SpringUtils;
import com.wxscrmplus.customer.domain.Customer;
import com.wxscrmplus.riskcontrol.domain.RiskDeleteFriend;
import com.wxscrmplus.wxcp.config.WxCpConfiguration;
import com.wxscrmplus.wxcp.config.WxCpProperties;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.cp.api.*;
import me.chanjar.weixin.cp.bean.message.WxCpMessage;

import java.util.List;

public class WxCpServiceFactory {

    /**
     * 获取外部联系人管理接口服务
     *
     * @return
     */
    public WxCpExternalContactService getExternalContactService() {
        WxCpService wxCpService = WxCpConfiguration.getCpService(SpringUtils.getBean(WxCpProperties.class).getAppConfigs().get(2).getAgentId());
        return wxCpService.getExternalContactService();
    }
// d5f1fa425cbc7b9415d3d29f39f6491e

    /**
     * 获取成员管理接口服务
     *
     * @return
     */
    public WxCpUserService getUserService() {
        WxCpService wxCpService = WxCpConfiguration.getCpService(SpringUtils.getBean(WxCpProperties.class).getAppConfigs().get(1).getAgentId());
        return wxCpService.getUserService();
    }

    /**
     * 获取消息推送接口服务
     *
     * @return
     */
    public WxCpMessageService getMessageService() {
        WxCpService wxCpService = WxCpConfiguration.getCpService(SpringUtils.getBean(WxCpProperties.class).getAppConfigs().get(0).getAgentId());
        return wxCpService.getMessageService();
    }

    /**
     * 获取媒体相关接口的服务类对象
     *
     * @return
     */
    public WxCpMediaService getMediaService() {
        WxCpService wxCpService = WxCpConfiguration.getCpService(SpringUtils.getBean(WxCpProperties.class).getAppConfigs().get(0).getAgentId());
        return wxCpService.getMediaService();
    }

    /**
     * 发送删除好友信息提醒通知
     *
     * @param receiveUser  消息接收人
     * @param delType      删除类型
     * @param digest       摘要
     * @param customerList 客户
     * @param user         跟进人
     */
    public void sendDeleteFriendRemindMessage(SysUser receiveUser, RiskDeleteFriend.DelTypeFieldEnums delType, String digest, List<Customer> customerList, SysUser user) {
        WxCpMessageService messageService = getMessageService();
        WxCpMessage wxCpMessage = new WxCpMessage();
        wxCpMessage.setMsgType("text");
        StringBuilder content =
            new StringBuilder("删除好友提醒\n" +
                "类型：" + delType.getLabel() + "\n" +
                "摘要：" + digest + "\n" +
                "跟进人：" + user.getNickName() + "\n" +
                "客户：");
        for (Customer customer : customerList) {
            content.append(customer.getName())
                .append("（").append(Customer.FollowStageFieldEnums.getName(customer.getFollowStage()))
                .append("）");
        }
        wxCpMessage.setContent(content.toString());
        wxCpMessage.setToUser(receiveUser.getUserName());
        try {
            messageService.send(wxCpMessage);
        } catch (WxErrorException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 发送客户转让提醒通知
     *
     * @param transfer     转让人
     * @param receive      接收人（username）
     * @param customerList 客户
     */
    public void sendTransferCustomerMessage(SysUser transfer, String receive, List<Customer> customerList) {
        WxCpMessageService messageService = getMessageService();
        WxCpMessage wxCpMessage = new WxCpMessage();
        wxCpMessage.setMsgType("text");
        StringBuilder content =
            new StringBuilder("客户转让提醒\n" +
                "摘要：你的同事设置你为以下客户的跟进人，请及时处理！\n" +
                "转让人：" + transfer.getNickName() + "\n" +
                "客户：");
        for (Customer customer : customerList) {
            content.append(customer.getName())
                .append("（").append(Customer.FollowStageFieldEnums.getName(customer.getFollowStage()))
                .append("）");
        }
        wxCpMessage.setContent(content.toString());
        wxCpMessage.setToUser(receive);
        try {
            messageService.send(wxCpMessage);
        } catch (WxErrorException e) {
            throw new RuntimeException(e);
        }
    }
}
