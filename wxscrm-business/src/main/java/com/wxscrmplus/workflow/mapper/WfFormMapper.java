package com.wxscrmplus.workflow.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.wxscrmplus.common.core.mapper.BaseMapperPlus;
import com.wxscrmplus.workflow.domain.WfForm;
import com.wxscrmplus.workflow.domain.vo.WfFormVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 流程表单Mapper接口
 *
 * @author www.wxscrmplus.com
 */
public interface WfFormMapper extends BaseMapperPlus<WfFormMapper, WfForm, WfFormVo> {
// fcedcc1962d45bff5d04fcc0fa1c05e4

    List<WfFormVo> selectFormVoList(@Param(Constants.WRAPPER) Wrapper<WfForm> queryWrapper);
}
