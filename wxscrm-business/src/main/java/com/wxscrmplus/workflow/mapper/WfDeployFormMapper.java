package com.wxscrmplus.workflow.mapper;

import com.wxscrmplus.common.core.mapper.BaseMapperPlus;
// abdce88992cfd6bd9ca13d8a1f26f239
import com.wxscrmplus.workflow.domain.WfDeployForm;
import com.wxscrmplus.workflow.domain.vo.WfDeployFormVo;

/**
 * 流程实例关联表单Mapper接口
 *
 * @author www.wxscrmplus.com
 */
public interface WfDeployFormMapper extends BaseMapperPlus<WfDeployFormMapper, WfDeployForm, WfDeployFormVo> {

}
