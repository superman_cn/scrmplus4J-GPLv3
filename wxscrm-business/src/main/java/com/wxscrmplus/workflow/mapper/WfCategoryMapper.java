package com.wxscrmplus.workflow.mapper;

import com.wxscrmplus.common.core.mapper.BaseMapperPlus;
import com.wxscrmplus.workflow.domain.WfCategory;
// 24924422f62a2cfde7d83cb5d74ce0ae
import com.wxscrmplus.workflow.domain.vo.WfCategoryVo;

/**
 * 流程分类Mapper接口
 *
 * @author www.wxscrmplus.com
 * @date 2022-01-15
 */
public interface WfCategoryMapper extends BaseMapperPlus<WfCategoryMapper, WfCategory, WfCategoryVo> {

}
