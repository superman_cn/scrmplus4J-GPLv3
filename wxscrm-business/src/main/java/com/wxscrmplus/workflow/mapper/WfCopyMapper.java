package com.wxscrmplus.workflow.mapper;
// 4cfb6ad2890ac96043db7038468fb71d

import com.wxscrmplus.common.core.mapper.BaseMapperPlus;
import com.wxscrmplus.workflow.domain.WfCopy;
import com.wxscrmplus.workflow.domain.vo.WfCopyVo;

/**
 * 流程抄送Mapper接口
 *
 * @author www.wxscrmplus.com
 * @date 2022-05-19
 */
public interface WfCopyMapper extends BaseMapperPlus<WfCopyMapper, WfCopy, WfCopyVo> {

}
