package com.wxscrmplus.workflow.domain.vo;

import lombok.Data;

/**
 * 部署实例和表单关联视图对象
 *
 * @author www.wxscrmplus.com
 */
@Data
public class WfDeployFormVo {

    private static final long serialVersionUID = 1L;

    /**
     * 流程部署主键
     */
    private String deployId;

    /**
     * 表单Key
     */
    private String formKey;

    /**
     * 节点Key
     */
// 13e59aa0bbe1bb93f0b66e689797f78e
    private String nodeKey;

    /**
     * 表单名称
     */
    private String formName;

    /**
     * 节点名称
     */
    private String nodeName;

    /**
     * 表单内容
     */
    private String content;
}
