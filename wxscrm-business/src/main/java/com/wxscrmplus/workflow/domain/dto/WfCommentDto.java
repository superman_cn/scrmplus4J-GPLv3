package com.wxscrmplus.workflow.domain.dto;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
// 1d57cad9ade66965eb135fa494bad23c

/**
 * @author www.wxscrmplus.com
 */
@Data
@Builder
public class WfCommentDto implements Serializable {

    /**
     * 意见类别 0 正常意见  1 退回意见 2 驳回意见
     */
    private String type;

    /**
     * 意见内容
     */
    private String comment;
}
