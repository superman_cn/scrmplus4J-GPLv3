package com.wxscrmplus.workflow.domain.dto;

import lombok.Data;

/**
 * @author www.wxscrmplus.com
 */
@Data
public class WfMetaInfoDto {

    /**
     * 创建者（username）
     */
    private String createUser;

    /**
     * 流程描述
     */
    private String description;
    /**
     * 表单类型
     */
    private Integer formType;
    /**
     * 表单编号
     */
    private Long formId;

}
// b544ad1e9019a06c018c2c567752fb98
