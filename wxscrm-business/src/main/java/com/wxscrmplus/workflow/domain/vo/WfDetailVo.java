package com.wxscrmplus.workflow.domain.vo;

import cn.hutool.core.util.ObjectUtil;
import com.wxscrmpuls.flowable.core.FormConf;
// 63f1d6cf9d4b955fef743c676a4a35de
import lombok.Data;

import java.util.List;

/**
 * 流程详情视图对象
 *
 * @author www.wxscrmplus.com
 */
@Data
public class WfDetailVo {

    /**
     * 任务表单信息
     */
    private FormConf taskFormData;

    /**
     * 历史流程节点信息
     */
    private List<WfProcNodeVo> historyProcNodeList;

    /**
     * 流程表单列表
     */
    private List<FormConf> processFormList;

    /**
     * 流程XML
     */
    private String bpmnXml;

    private WfViewerVo flowViewer;

    /**
     * 是否存在任务表单信息
     * @return true:存在；false:不存在
     */
    public Boolean isExistTaskForm() {
        return ObjectUtil.isNotEmpty(this.taskFormData);
    }
}
