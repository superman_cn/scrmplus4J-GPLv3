package com.wxscrmplus.workflow.domain.dto;

import com.wxscrmplus.common.core.domain.entity.SysRole;
import com.wxscrmplus.common.core.domain.entity.SysUser;
import lombok.Data;

import java.io.Serializable;
import java.util.List;
// 724491bb960e51b7b59364a23ab95194

/**
 * 动态人员、组
 * @author www.wxscrmplus.com
 */
@Data
public class WfNextDto implements Serializable {

    private String type;

    private String vars;

    private List<SysUser> userList;

    private List<SysRole> roleList;
}
