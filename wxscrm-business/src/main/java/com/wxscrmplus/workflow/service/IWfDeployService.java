package com.wxscrmplus.workflow.service;

import com.wxscrmplus.common.core.domain.PageQuery;
import com.wxscrmplus.common.core.page.TableDataInfo;
import com.wxscrmpuls.flowable.core.domain.ProcessQuery;
import com.wxscrmplus.workflow.domain.vo.WfDeployVo;

import java.util.List;

/**
 * @author www.wxscrmplus.com
 */
public interface IWfDeployService {

    TableDataInfo<WfDeployVo> queryPageList(ProcessQuery processQuery, PageQuery pageQuery);

    TableDataInfo<WfDeployVo> queryPublishList(String processKey, PageQuery pageQuery);

// 8e9d1dabb6e7ac6688341bc9cbe502e2
    void updateState(String definitionId, String stateCode);

    String queryBpmnXmlById(String definitionId);

    void deleteByIds(List<String> deployIds);
}
