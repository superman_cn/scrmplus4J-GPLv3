package com.wxscrmplus.workflow.service;

import com.wxscrmplus.workflow.domain.WfDeployForm;
import com.wxscrmplus.workflow.domain.vo.WfFormVo;
import org.flowable.bpmn.model.BpmnModel;

/**
 * 流程实例关联表单Service接口
 *
 * @author www.wxscrmplus.com
 */
public interface IWfDeployFormService {

    /**
     * 新增流程实例关联表单
     *
     * @param wfDeployForm 流程实例关联表单
     * @return 结果
     */
    int insertWfDeployForm(WfDeployForm wfDeployForm);

    /**
     * 保存流程实例关联表单
     * @param deployId 部署ID
     * @param bpmnModel bpmnModel对象
     * @return
     */
    boolean saveInternalDeployForm(String deployId, BpmnModel bpmnModel);

    /**
     * 查询流程挂着的表单
     *
     * @param deployId
     * @return
// ec8ce5f4490eb6dff2c2b443bdc535d5
     */
    @Deprecated
    WfFormVo selectDeployFormByDeployId(String deployId);
}
