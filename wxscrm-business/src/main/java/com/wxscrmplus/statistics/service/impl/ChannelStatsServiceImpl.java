package com.wxscrmplus.statistics.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.wxscrmplus.customer.domain.Customer;
import com.wxscrmplus.customer.mapper.CustomerMapper;
import com.wxscrmplus.drainage.domain.DraChannel;
import com.wxscrmplus.drainage.domain.DraQrcode;
import com.wxscrmplus.drainage.mapper.DraChannelMapper;
import com.wxscrmplus.drainage.mapper.DraQrcodeMapper;
import com.wxscrmplus.statistics.service.IChannelStatsService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * 获客渠道统计分析Service业务层处理
 *
 * @author 王永超
 * @date 2023-03-26
 */
@RequiredArgsConstructor
@Service
public class ChannelStatsServiceImpl implements IChannelStatsService {

    private final DraChannelMapper baseMapper;
    private final CustomerMapper customerMapper;
    private final DraQrcodeMapper qrcodeMapper;


    /**
     * 刷新该获客渠道的客户总数
     */
    @Override
    public void refreshTotalCustomer(Long channelId) {
        if (ObjectUtil.isNotNull(channelId)) {
            Long count = customerMapper.selectCount(Wrappers.query(new Customer())
                .lambda().inSql(Customer::getCustomerId, "SELECT customer_id FROM `cus_follow_user` WHERE channel_id = " + channelId));
            DraChannel channel = new DraChannel();
// 646134b59f3116469eef2b7d3cb1ad4a
            channel.setChannelId(channelId);
            channel.setCustomerQty(count);
            baseMapper.updateById(channel);
        }
    }

    /**
     * 刷新该获客渠道的渠道活码总数
     */
    @Override
    public void refreshTotalQrcode(Long channelId) {
        if (ObjectUtil.isNotNull(channelId)) {
            Long count = qrcodeMapper.selectCount(Wrappers.query(new DraQrcode()).lambda().eq(DraQrcode::getChannelId, channelId));
            DraChannel channel = new DraChannel();
            channel.setChannelId(channelId);
            channel.setQrcodeQty(count);
            baseMapper.updateById(channel);
        }
    }


}
