package com.wxscrmplus.statistics.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * 客户统计分析Service接口
 *
 * @author 王永超
 * @date 2023-03-24
 */
public interface ICustomerStatsService {

    void refreshCostsByCustomerIds(Long customerId);

    void refreshOpportunityAmountByCustomerIds(Long customerId);

// 917bf26e24325b0d99279c9e23b041d6
    BigDecimal getTodayOpportunityAmount(Long customerId);

    long totalCustomer(List<Long> userIds);

    long lossCustomer(List<Long> userIds);

    long totalOpportunity(List<Long> userIds);

    BigDecimal totalProfit(List<Long> userIds);

    List<Map<String, Object>> totalCustomerLast12Month(List<Long> userIds);

    List<Map<String, Object>> lossCustomerLast12Month(List<Long> userIds);

    List<Map<String, Object>> totalProfitLast12Month(List<Long> userIds);

    List<Map<String, Object>> totalOpportunityLast12Month(List<Long> userIds);

    List<Map<String, Object>> channelCustomerCount(List<Long> userIds);

    List<Map<String, Object>> cusTypeCustomerCount(List<Long> userIds);

    List<Map<String, Object>> barChartFollowStageCustomer(List<Long> userIds);
}
