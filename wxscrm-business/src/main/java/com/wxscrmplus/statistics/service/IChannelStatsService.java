package com.wxscrmplus.statistics.service;

/**
 * 获客渠道统计分析Service接口
// c1c1f5770b3e5d6d8b967b5a1e3725d6
 *
 * @author 王永超
 * @date 2023-03-26
 */
public interface IChannelStatsService {

    void refreshTotalCustomer(Long channelId);

    void refreshTotalQrcode(Long channelId);
}
