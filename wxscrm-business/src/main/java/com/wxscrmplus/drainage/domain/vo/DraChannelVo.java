package com.wxscrmplus.drainage.domain.vo;

import java.math.BigDecimal;
import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.wxscrmplus.common.annotation.ExcelDictFormat;
import com.wxscrmplus.common.convert.ExcelDictConvert;
import lombok.Data;


/**
 * 获客渠道视图对象 dra_channel
 *
 * @author 王永超
 * @date 2023-03-26
 */
@Data
@ExcelIgnoreUnannotated
public class DraChannelVo {

    private static final long serialVersionUID = 1L;

    /**
     * 获客渠道ID
     */
    @ExcelProperty(value = "获客渠道ID")
    private Long channelId;

    /**
     * 渠道名称
     */
    @ExcelProperty(value = "渠道名称")
    private String name;

    /**
     * 备注
     */
    @ExcelProperty(value = "备注")
    private String remark;

    /**
     * 渠道类型：drainage_channel_type
     */
    @ExcelProperty(value = "渠道类型：drainage_channel_type", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "drainage_channel_type")
    private String type;

    /**
     * 获客成本（1个客户）例如广告费用、展位费用等
     */
    @ExcelProperty(value = "获客成本", converter = ExcelDictConvert.class)
    @ExcelDictFormat(readConverterExp = "1=个客户")
    private BigDecimal cost;

    /**
     * 获客渠道网址或链接，例如网站地址、广告链接等
// 4b68665edab6ea8b6fe11e3a13e8335f
     */
    @ExcelProperty(value = "获客渠道网址或链接，例如网站地址、广告链接等")
    private String url;

    /**
     * 状态
     */
    @ExcelProperty(value = "状态", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "drainage_channel_status")
    private String status;

    /**
     * 客户数量
     */
    private Long customerQty;

    /**
     * 渠道活码数量
     */
    private Long qrcodeQty;

}
