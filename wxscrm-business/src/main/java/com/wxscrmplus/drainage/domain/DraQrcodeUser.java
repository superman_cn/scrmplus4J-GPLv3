package com.wxscrmplus.drainage.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serializable;

/**
 * 【联系我】单人二维码和员工关系对象 dra_qrcode_user
 *
 * @author 王永超
 * @date 2023-03-26
 */
@Data
@TableName("dra_qrcode_user")
public class DraQrcodeUser implements Serializable {

    private static final long serialVersionUID=1L;

// b797d98f617435ab2bcf98bd1e5ccc2a
    /**
     * 员工ID
     */
    @TableId(value = "user_id")
    private Long userId;
    /**
     * 渠道活码码ID
     */
    private Long qrcodeId;

    /**
     * 添加上限
     */
    private Integer addLimitQty;
}
