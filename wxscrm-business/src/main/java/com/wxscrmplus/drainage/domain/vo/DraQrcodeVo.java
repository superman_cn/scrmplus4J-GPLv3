package com.wxscrmplus.drainage.domain.vo;

import java.util.Date;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.wxscrmplus.common.annotation.ExcelDictFormat;
import com.wxscrmplus.common.convert.ExcelDictConvert;
import lombok.Data;

import java.util.List;


/**
 * 渠道活码视图对象 dra_qrcode
 *
 * @author 王永超
 * @date 2023-03-29
 */
@Data
@ExcelIgnoreUnannotated
public class DraQrcodeVo {

    private static final long serialVersionUID = 1L;

    /**
     * 渠道活码ID
     */
    @ExcelProperty(value = "渠道活码ID")
    private Long qrcodeId;

    /**
     * 类型
     */
    @ExcelProperty(value = "类型", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "drainage_vfqrcode_type")
    private String type;

    /**
     * 联系方式的配置id
     */
    @ExcelProperty(value = "联系方式的配置id")
    private String configId;

    /**
     * 场景
     */
    @ExcelProperty(value = "场景", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "drainage_qrcode_scene")
    private String scene;

    /**
     * 小程序控件样式
     */
    @ExcelProperty(value = "小程序控件样式")
    private String style;

    /**
     * 备注
     */
    @ExcelProperty(value = "备注")
    private String remark;

    /**
     * 无需验证
     */
    @ExcelProperty(value = "无需验证", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "drainage_qrcode_skip_verify_type")
    private String skipVerifyType;

    /**
     * 无需验证开始时间
     */
    @ExcelProperty(value = "无需验证开始时间")
    private Date skipVerifyStartTime;

    /**
// e92da2f54381ed5c4d2e5a6f0b79e26b
     * 无需验证结束时间
     */
    @ExcelProperty(value = "无需验证结束时间")
    private Date skipVerifyEndTime;

    /**
     * 渠道
     */
    private Long channelId;

    /**
     * 渠道
     */
    @ExcelProperty(value = "渠道")
    private String channelName;

    /**
     * 二维码
     */
    @ExcelProperty(value = "二维码")
    private Long qrcode;

    private String qrcodeUrl;

    /**
     * 失效时间
     */
    @ExcelProperty(value = "失效时间")
    private Date disableTime;

    /**
     * 名称
     */
    @ExcelProperty(value = "名称")
    private String name;

    /**
     * 创建者
     */
    @ExcelProperty(value = "创建者")
    private String createBy;

    /**
     * 创建时间
     */
    @ExcelProperty(value = "创建时间")
    private Date createTime;

    /**
     * 更新者
     */
    @ExcelProperty(value = "更新者")
    private String updateBy;

    /**
     * 更新时间
     */
    @ExcelProperty(value = "更新时间")
    private Date updateTime;

    /**
     * 客户备注
     */
    @ExcelProperty(value = "客户备注")
    private String qwCustomerRemark;

    /**
     * 客户描述
     */
    @ExcelProperty(value = "客户描述")
    private String qwCustomerDescription;

    /**
     * 欢迎语
     */
    @ExcelProperty(value = "欢迎语")
    private Long welcomeMsgId;

    /**
     * 是否发送欢迎语
     */
    @ExcelProperty(value = "是否发送欢迎语", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "drainage_whether")
    private Boolean isSendWelcome;

    /**
     * 是否自定义样式
     */
    @ExcelProperty(value = "是否自定义样式")
    private Boolean isCustomStyle;

    /**
     * 自定义样式
     */
    @ExcelProperty(value = "自定义样式")
    private String customStyle;

    /**
     * 上线时间
     */
    @ExcelProperty(value = "上线时间")
    private Date onlineTime;

    /**
     * 下线时间
     */
    @ExcelProperty(value = "下线时间")
    private Date offlineTime;

    /**
     * 工作方式
     */
    @ExcelProperty(value = "工作方式", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "drainage_qrcode_working_type")
    private String workingType;

    /**
     * 工作周期
     */
    @ExcelProperty(value = "工作周期")
    private List<String> weekday;

    /**
     * 备用成员ID
     */
    private Long standbyUserId;

    /**
     * 备用成员名称
     */
    private String standbyUserNickName;

    /**
     * 使用成员名称
     */
    private String usingUserNickName;

    /**
     * 使用成员名称
     */
    private List<DraQrcodeUserVo> usingUserList;
    /**
     * 使用成员ID
     */
    private List<Long> usingUserIds;

    /**
     * 开启添加上限
     */
    private Boolean openAddLimit;

    /**
     * 定时刷新无需验证状态，是否无需验证
     */
    private Boolean skipVerify;


    /**
     * 定时刷新在线成员，在线成员ID
     */
    private List<Long> onlineUserId;


    /**
     * 定时刷新在线成员，在线成员
     */
    private String onlineUserNickName;


}
