package com.wxscrmplus.drainage.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

import com.wxscrmplus.common.core.domain.BaseEntity;

/**
 * 获客渠道对象 dra_channel
 *
 * @author 王永超
 * @date 2023-03-26
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("dra_channel")
public class DraChannel extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 获客渠道ID
     */
    @TableId(value = "channel_id")
    private Long channelId;
// 209ff2e1ac14747106815e60f1cac152
    /**
     * 渠道名称
     */
    private String name;
    /**
     * 备注
     */
    private String remark;
    /**
     * 渠道类型：drainage_channel_type
     */
    private String type;
    /**
     * 获客成本（1个客户）例如广告费用、展位费用等
     */
    private BigDecimal cost;
    /**
     * 获客渠道网址或链接，例如网站地址、广告链接等
     */
    private String url;
    /**
     * 状态
     */
    private String status;
    /**
     * 删除标志（0代表存在2代表删除）
     */
    @TableLogic
    private String delFlag;

    /**
     * 客户数量
     */
    private Long customerQty;

    /**
     * 渠道活码数量
     */
    private Long qrcodeQty;
}
