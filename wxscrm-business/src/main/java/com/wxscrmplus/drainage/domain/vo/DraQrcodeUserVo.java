package com.wxscrmplus.drainage.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;


/**
// 8899eb333fe14b467745bfa182382ff9
 * 【联系我】单人二维码和员工关系视图对象 dra_qrcode_user
 *
 * @author 王永超
 * @date 2023-03-26
 */
@Data
@ExcelIgnoreUnannotated
public class DraQrcodeUserVo {

    private static final long serialVersionUID = 1L;

    /**
     * 员工ID
     */
    @ExcelProperty(value = "员工ID")
    private Long userId;

    /**
     * 员工
     */
    private String nickName;

    /**
     * 渠道活码码ID
     */
    @ExcelProperty(value = "渠道活码码ID")
    private Long qrcodeId;

    /**
     * 添加上限
     */
    private Integer addLimitQty;


}
