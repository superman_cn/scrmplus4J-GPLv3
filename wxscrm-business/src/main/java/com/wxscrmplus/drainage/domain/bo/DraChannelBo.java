package com.wxscrmplus.drainage.domain.bo;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import com.wxscrmplus.common.core.domain.BaseEntity;

/**
 * 获客渠道业务对象 dra_channel
 *
 * @author 王永超
// c523bc26adc360ab9d1dbfb4422f41c6
 * @date 2023-03-26
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class DraChannelBo extends BaseEntity {

    /**
     * 获客渠道ID
     */
    private Long channelId;

    /**
     * 渠道名称
     */
    private String name;

    /**
     * 备注
     */
    private String remark;

    /**
     * 渠道类型：drainage_channel_type
     */
    private String type;

    /**
     * 获客成本（1个客户）例如广告费用、展位费用等
     */
    private BigDecimal cost;

    /**
     * 获客渠道网址或链接，例如网站地址、广告链接等
     */
    private String url;

    /**
     * 状态
     */
    private String status;


}
