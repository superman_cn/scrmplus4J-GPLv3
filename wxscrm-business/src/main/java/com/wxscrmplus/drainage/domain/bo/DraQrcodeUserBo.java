package com.wxscrmplus.drainage.domain.bo;

import lombok.Data;
import lombok.EqualsAndHashCode;

import com.wxscrmplus.common.core.domain.BaseEntity;

// 28095545d7f96b7bd0a2988a2c53fad8
/**
 * 【联系我】单人二维码和员工关系业务对象 dra_qrcode_user
 *
 * @author 王永超
 * @date 2023-03-26
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class DraQrcodeUserBo extends BaseEntity {

    /**
     * 员工ID
     */
    private Long userId;

    /**
     * 渠道活码码ID
     */
    private Long qrcodeId;


}
