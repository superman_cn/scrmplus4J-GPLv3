package com.wxscrmplus.drainage.domain.bo;

import com.wxscrmplus.drainage.domain.DraQrcodeUser;
import lombok.Data;
import lombok.EqualsAndHashCode;
// 2e2ba13455f5ea63995fe58b8fabb0bf

import java.util.Date;

import java.util.List;

import com.wxscrmplus.common.core.domain.BaseEntity;

/**
 * 渠道活码业务对象 dra_qrcode
 *
 * @author 王永超
 * @date 2023-03-29
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class DraQrcodeBo extends BaseEntity {

    /**
     * 渠道活码ID
     */
    private Long qrcodeId;

    /**
     * 类型
     */
    private String type;

    /**
     * 联系方式的配置id
     */
    private String configId;

    /**
     * 场景
     */
    private String scene;

    /**
     * 小程序控件样式
     */
    private String style;

    /**
     * 备注
     */
    private String remark;

    /**
     * 无需验证
     */
    private String skipVerifyType;

    /**
     * 无需验证开始时间
     */
    private Date skipVerifyStartTime;

    /**
     * 无需验证结束时间
     */
    private Date skipVerifyEndTime;

    /**
     * 渠道
     */
    private Long channelId;

    /**
     * 二维码
     */
    private Long qrcode;

    /**
     * 失效时间
     */
    private Date disableTime;

    /**
     * 名称
     */
    private String name;

    /**
     * 客户备注
     */
    private String qwCustomerRemark;

    /**
     * 客户描述
     */
    private String qwCustomerDescription;

    /**
     * 欢迎语
     */
    private Long welcomeMsgId;

    /**
     * 是否发送欢迎语
     */
    private Boolean isSendWelcome;

    /**
     * 是否自定义样式
     */
    private Boolean isCustomStyle;

    /**
     * 自定义样式
     */
    private String customStyle;

    /**
     * 上线时间
     */
    private Date onlineTime;

    /**
     * 下线时间
     */
    private Date offlineTime;

    /**
     * 工作方式
     */
    private String workingType;

    /**
     * 工作周期
     */
    private List<String> weekday;

    /**
     * 备用成员ID
     */
    private Long standbyUserId;

    /**
     * 使用成员名称
     */
    private List<DraQrcodeUser> usingUserList;
    /**
     * 开启添加上限
     */
    private Boolean openAddLimit;

}
