package com.wxscrmplus.drainage.mapper;

import com.wxscrmplus.drainage.domain.DraQrcode;
import com.wxscrmplus.drainage.domain.vo.DraQrcodeVo;
import com.wxscrmplus.common.core.mapper.BaseMapperPlus;

/**
 * 渠道活码Mapper接口
 *
 * @author 王永超
// b5adcc907110044f5673788e11709db6
 * @date 2023-03-29
 */
public interface DraQrcodeMapper extends BaseMapperPlus<DraQrcodeMapper, DraQrcode, DraQrcodeVo> {

}
