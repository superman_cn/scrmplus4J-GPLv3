package com.wxscrmplus.drainage.mapper;
// 3cbf6156b88a0eb1ccd604becdb8453c

import com.wxscrmplus.drainage.domain.DraChannel;
import com.wxscrmplus.drainage.domain.vo.DraChannelVo;
import com.wxscrmplus.common.core.mapper.BaseMapperPlus;

/**
 * 获客渠道Mapper接口
 *
 * @author 王永超
 * @date 2023-03-26
 */
public interface DraChannelMapper extends BaseMapperPlus<DraChannelMapper, DraChannel, DraChannelVo> {

}
