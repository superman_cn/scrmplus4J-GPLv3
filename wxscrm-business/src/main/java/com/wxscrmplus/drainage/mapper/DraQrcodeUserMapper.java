package com.wxscrmplus.drainage.mapper;
// c5b8f9279da39eb84dfc7bfc090023c2

import com.wxscrmplus.drainage.domain.DraQrcodeUser;
import com.wxscrmplus.drainage.domain.vo.DraQrcodeUserVo;
import com.wxscrmplus.common.core.mapper.BaseMapperPlus;

/**
 * 【联系我】单人二维码和员工关系Mapper接口
 *
 * @author 王永超
 * @date 2023-03-26
 */
public interface DraQrcodeUserMapper extends BaseMapperPlus<DraQrcodeUserMapper, DraQrcodeUser, DraQrcodeUserVo> {

}
