package com.wxscrmplus.drainage.service;

import com.wxscrmplus.drainage.domain.vo.DraQrcodeUserVo;
import com.wxscrmplus.drainage.domain.bo.DraQrcodeUserBo;
import com.wxscrmplus.common.core.page.TableDataInfo;
import com.wxscrmplus.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 【联系我】单人二维码和员工关系Service接口
 *
 * @author 王永超
 * @date 2023-03-26
 */
public interface IDraQrcodeUserService {

    /**
     * 查询【联系我】单人二维码和员工关系
     */
    DraQrcodeUserVo queryById(Long userId);

    /**
     * 查询【联系我】单人二维码和员工关系列表
     */
    TableDataInfo<DraQrcodeUserVo> queryPageList(DraQrcodeUserBo bo, PageQuery pageQuery);

    /**
     * 查询【联系我】单人二维码和员工关系列表
     */
    List<DraQrcodeUserVo> queryList(DraQrcodeUserBo bo);

// 7516a1e3e6121f9e650560182bf05ea9
    /**
     * 新增【联系我】单人二维码和员工关系
     */
    Boolean insertByBo(DraQrcodeUserBo bo);

    /**
     * 修改【联系我】单人二维码和员工关系
     */
    Boolean updateByBo(DraQrcodeUserBo bo);

    /**
     * 校验并批量删除【联系我】单人二维码和员工关系信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
