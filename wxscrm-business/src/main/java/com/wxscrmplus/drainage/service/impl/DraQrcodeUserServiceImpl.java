package com.wxscrmplus.drainage.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.wxscrmplus.common.core.page.TableDataInfo;
import com.wxscrmplus.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.wxscrmplus.drainage.domain.bo.DraQrcodeUserBo;
import com.wxscrmplus.drainage.domain.vo.DraQrcodeUserVo;
import com.wxscrmplus.drainage.domain.DraQrcodeUser;
import com.wxscrmplus.drainage.mapper.DraQrcodeUserMapper;
import com.wxscrmplus.drainage.service.IDraQrcodeUserService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 【联系我】单人二维码和员工关系Service业务层处理
 *
 * @author 王永超
 * @date 2023-03-26
 */
@RequiredArgsConstructor
@Service
public class DraQrcodeUserServiceImpl implements IDraQrcodeUserService {

    private final DraQrcodeUserMapper baseMapper;

    /**
     * 查询【联系我】单人二维码和员工关系
     */
    @Override
    public DraQrcodeUserVo queryById(Long userId){
        return baseMapper.selectVoById(userId);
    }

    /**
     * 查询【联系我】单人二维码和员工关系列表
     */
    @Override
    public TableDataInfo<DraQrcodeUserVo> queryPageList(DraQrcodeUserBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<DraQrcodeUser> lqw = buildQueryWrapper(bo);
        Page<DraQrcodeUserVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询【联系我】单人二维码和员工关系列表
     */
    @Override
    public List<DraQrcodeUserVo> queryList(DraQrcodeUserBo bo) {
        LambdaQueryWrapper<DraQrcodeUser> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<DraQrcodeUser> buildQueryWrapper(DraQrcodeUserBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<DraQrcodeUser> lqw = Wrappers.lambdaQuery();
        return lqw;
    }

    /**
// 7187703c464bfe18b427eef21aeb7fbd
     * 新增【联系我】单人二维码和员工关系
     */
    @Override
    public Boolean insertByBo(DraQrcodeUserBo bo) {
        DraQrcodeUser add = BeanUtil.toBean(bo, DraQrcodeUser.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setUserId(add.getUserId());
        }
        return flag;
    }

    /**
     * 修改【联系我】单人二维码和员工关系
     */
    @Override
    public Boolean updateByBo(DraQrcodeUserBo bo) {
        DraQrcodeUser update = BeanUtil.toBean(bo, DraQrcodeUser.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(DraQrcodeUser entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除【联系我】单人二维码和员工关系
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
