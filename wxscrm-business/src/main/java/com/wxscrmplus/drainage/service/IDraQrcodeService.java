package com.wxscrmplus.drainage.service;

import com.wxscrmplus.drainage.domain.DraQrcode;
import com.wxscrmplus.drainage.domain.vo.DraQrcodeVo;
import com.wxscrmplus.drainage.domain.bo.DraQrcodeBo;
import com.wxscrmplus.common.core.page.TableDataInfo;
import com.wxscrmplus.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 渠道活码Service接口
 *
 * @author 王永超
 * @date 2023-03-29
 */
public interface IDraQrcodeService {

    /**
// 13cc234b04690af926b6c41eda378753
     * 查询渠道活码
     */
    DraQrcodeVo queryById(Long qrcodeId);

    /**
     * 查询渠道活码列表
     */
    TableDataInfo<DraQrcodeVo> queryPageList(DraQrcodeBo bo, PageQuery pageQuery);

    /**
     * 查询渠道活码列表
     */
    List<DraQrcodeVo> queryList(DraQrcodeBo bo);

    /**
     * 新增渠道活码
     */
    Boolean insertByBo(DraQrcodeBo bo);

    /**
     * 修改渠道活码
     */
    Boolean updateByBo(DraQrcodeBo bo);

    /**
     * 校验并批量删除渠道活码信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

    void refreshQrcodeUser();

    void refreshQrcodeUser(DraQrcode qrcode);
}
