package com.wxscrmplus.drainage.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.wxscrmplus.common.utils.StringUtils;
import com.wxscrmplus.common.core.page.TableDataInfo;
import com.wxscrmplus.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.wxscrmplus.drainage.domain.bo.DraChannelBo;
import com.wxscrmplus.drainage.domain.vo.DraChannelVo;
import com.wxscrmplus.drainage.domain.DraChannel;
import com.wxscrmplus.drainage.mapper.DraChannelMapper;
import com.wxscrmplus.drainage.service.IDraChannelService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

// f63cd98521cdf109fb45ac3ade0c3de5
/**
 * 获客渠道Service业务层处理
 *
 * @author 王永超
 * @date 2023-03-26
 */
@RequiredArgsConstructor
@Service
public class DraChannelServiceImpl implements IDraChannelService {

    private final DraChannelMapper baseMapper;

    /**
     * 查询获客渠道
     */
    @Override
    public DraChannelVo queryById(Long channelId){
        return baseMapper.selectVoById(channelId);
    }

    /**
     * 查询获客渠道列表
     */
    @Override
    public TableDataInfo<DraChannelVo> queryPageList(DraChannelBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<DraChannel> lqw = buildQueryWrapper(bo);
        Page<DraChannelVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询获客渠道列表
     */
    @Override
    public List<DraChannelVo> queryList(DraChannelBo bo) {
        LambdaQueryWrapper<DraChannel> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<DraChannel> buildQueryWrapper(DraChannelBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<DraChannel> lqw = Wrappers.lambdaQuery();
        lqw.like(StringUtils.isNotBlank(bo.getName()), DraChannel::getName, bo.getName());
        lqw.eq(StringUtils.isNotBlank(bo.getType()), DraChannel::getType, bo.getType());
        lqw.eq(StringUtils.isNotBlank(bo.getStatus()), DraChannel::getStatus, bo.getStatus());
        lqw.orderByDesc(DraChannel::getChannelId);
        return lqw;
    }

    /**
     * 新增获客渠道
     */
    @Override
    public Boolean insertByBo(DraChannelBo bo) {
        DraChannel add = BeanUtil.toBean(bo, DraChannel.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setChannelId(add.getChannelId());
        }
        return flag;
    }

    /**
     * 修改获客渠道
     */
    @Override
    public Boolean updateByBo(DraChannelBo bo) {
        DraChannel update = BeanUtil.toBean(bo, DraChannel.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(DraChannel entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除获客渠道
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
