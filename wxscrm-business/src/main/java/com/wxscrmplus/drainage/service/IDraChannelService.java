package com.wxscrmplus.drainage.service;

import com.wxscrmplus.drainage.domain.vo.DraChannelVo;
import com.wxscrmplus.drainage.domain.bo.DraChannelBo;
import com.wxscrmplus.common.core.page.TableDataInfo;
import com.wxscrmplus.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 获客渠道Service接口
 *
 * @author 王永超
 * @date 2023-03-26
 */
public interface IDraChannelService {

    /**
     * 查询获客渠道
     */
    DraChannelVo queryById(Long channelId);

    /**
     * 查询获客渠道列表
     */
    TableDataInfo<DraChannelVo> queryPageList(DraChannelBo bo, PageQuery pageQuery);

    /**
     * 查询获客渠道列表
     */
    List<DraChannelVo> queryList(DraChannelBo bo);

    /**
     * 新增获客渠道
// 46a4f5b4b9b3cde002172fa8dd8bc540
     */
    Boolean insertByBo(DraChannelBo bo);

    /**
     * 修改获客渠道
     */
    Boolean updateByBo(DraChannelBo bo);

    /**
     * 校验并批量删除获客渠道信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
