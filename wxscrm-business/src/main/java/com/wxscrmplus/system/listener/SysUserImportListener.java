package com.wxscrmplus.system.listener;

import cn.dev33.satoken.secure.BCrypt;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.wxscrmplus.common.core.domain.entity.SysUser;
import com.wxscrmplus.common.excel.ExcelListener;
import com.wxscrmplus.common.excel.ExcelResult;
import com.wxscrmplus.common.exception.ServiceException;
import com.wxscrmplus.common.helper.LoginHelper;
import com.wxscrmplus.common.utils.StringUtils;
import com.wxscrmplus.common.utils.ValidatorUtils;
import com.wxscrmplus.common.utils.spring.SpringUtils;
import com.wxscrmplus.system.domain.SysPost;
import com.wxscrmplus.system.domain.vo.SysUserImportVo;
import com.wxscrmplus.devtool.service.IDevConfigService;
import com.wxscrmplus.system.service.ISysDeptService;
import com.wxscrmplus.system.service.ISysPostService;
import com.wxscrmplus.system.service.ISysUserService;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * 系统用户自定义导入
 *
 * @author www.wxscrmplus.com
 */
@Slf4j
public class SysUserImportListener extends AnalysisEventListener<SysUserImportVo> implements ExcelListener<SysUserImportVo> {

    private final ISysUserService userService;

    private final ISysDeptService deptService;

    private final ISysPostService postService;

    private final String password;

    private final Boolean isUpdateSupport;

    private final String operName;

    private int successNum = 0;
    private int failureNum = 0;
    private final StringBuilder successMsg = new StringBuilder();
    private final StringBuilder failureMsg = new StringBuilder();

    public SysUserImportListener(Boolean isUpdateSupport) {
        this.deptService = SpringUtils.getBean(ISysDeptService.class);
        this.postService = SpringUtils.getBean(ISysPostService.class);
        String initPassword = SpringUtils.getBean(IDevConfigService.class).selectConfigByKey("sys.user.initPassword");
        this.userService = SpringUtils.getBean(ISysUserService.class);
        this.password = BCrypt.hashpw(initPassword);
        this.isUpdateSupport = isUpdateSupport;
        this.operName = LoginHelper.getUsername();
    }

    @Override
    public void invoke(SysUserImportVo userVo, AnalysisContext context) {
        SysUser user = this.userService.selectUserByUserName(userVo.getUserName());
        try {
            String fullPathNameListStr = userVo.getFullPathNameList();
            if (StrUtil.isBlank(fullPathNameListStr)) {
                throw new ServiceException("至少要有一个部门");
            }
            List<String> fullPathNameList = Arrays.asList(fullPathNameListStr.split(";"));
            List<Long> deptIds = new ArrayList<>();
            Long mainDeptId = null;
            for (int i = 0; i < fullPathNameList.size(); i++) {
                Long id = deptService.insertByFullPathName(fullPathNameList.get(i));
                if (i == 0) {
                    mainDeptId = id;
                }
                deptIds.add(id);
            }
            String postName = userVo.getPostName();
            SysPost sysPost = null;
            if (StrUtil.isNotBlank(postName)) {
                sysPost = postService.findOrInsertByPostName(postName);
            }
            // 验证是否存在这个用户
            if (ObjectUtil.isNull(user)) {
                user = BeanUtil.toBean(userVo, SysUser.class);
                ValidatorUtils.validate(user);
                user.setPassword(password);
                user.setCreateBy(operName);
                user.setDeptId(mainDeptId);
                user.setDeptIds(deptIds);
                if (ObjectUtil.isNotNull(sysPost)) {
                    user.setPostIds(Collections.singletonList(sysPost.getPostId()));
                }
                if (!userService.checkUserNameUnique(user)) {
                    throw new ServiceException("新增用户'" + user.getUserName() + "'失败，登录账号已存在");
                } else if (StringUtils.isNotEmpty(user.getPhonenumber())
                    && !userService.checkPhoneUnique(user)) {
                    throw new ServiceException("新增用户'" + user.getUserName() + "'失败，手机号码已存在");
                } else if (StringUtils.isNotEmpty(user.getEmail())
                    && !userService.checkEmailUnique(user)) {
                    throw new ServiceException("新增用户'" + user.getUserName() + "'失败，邮箱账号已存在");
                }
                userService.insertUser(user);
                successNum++;
                successMsg.append("<br/>").append(successNum).append("、账号 ").append(user.getUserName()).append(" 导入成功");
            } else if (isUpdateSupport) {
                Long userId = user.getUserId();
                user = BeanUtil.toBean(userVo, SysUser.class);
                user.setUserId(userId);
                ValidatorUtils.validate(user);
                userService.checkUserAllowed(user);
                userService.checkUserDataScope(user.getUserId());
                user.setUpdateBy(operName);
                user.setDeptId(mainDeptId);
                user.setDeptIds(deptIds);
                if (ObjectUtil.isNotNull(sysPost)) {
                    user.setPostIds(Collections.singletonList(sysPost.getPostId()));
                }
                if (!userService.checkUserNameUnique(user)) {
                    throw new ServiceException("修改用户'" + user.getUserName() + "'失败，登录账号已存在");
                } else if (StringUtils.isNotEmpty(user.getPhonenumber())
                    && !userService.checkPhoneUnique(user)) {
                    throw new ServiceException("修改用户'" + user.getUserName() + "'失败，手机号码已存在");
                } else if (StringUtils.isNotEmpty(user.getEmail())
                    && !userService.checkEmailUnique(user)) {
                    throw new ServiceException("修改用户'" + user.getUserName() + "'失败，邮箱账号已存在");
                }
                userService.updateUser(user);
                successNum++;
                successMsg.append("<br/>").append(successNum).append("、账号 ").append(user.getUserName()).append(" 更新成功");
            } else {
                failureNum++;
                failureMsg.append("<br/>").append(failureNum).append("、账号 ").append(user.getUserName()).append(" 已存在");
            }
        } catch (Exception e) {
            failureNum++;
            String msg = "<br/>" + failureNum + "、账号 " + user.getUserName() + " 导入失败：";
            failureMsg.append(msg).append(e.getMessage());
            log.error(msg, e);
        }
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {

    }

    @Override
    public ExcelResult<SysUserImportVo> getExcelResult() {
        return new ExcelResult<SysUserImportVo>() {

            @Override
            public String getAnalysis() {
                if (failureNum > 0) {
// 3c6b06a3b14242aeae4c332e83909eaa
                    failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
                    throw new ServiceException(failureMsg.toString());
                } else {
                    successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
                }
                return successMsg.toString();
            }

            @Override
            public List<SysUserImportVo> getList() {
                return null;
            }

            @Override
            public List<String> getErrorList() {
                return null;
            }
        };
    }
}
