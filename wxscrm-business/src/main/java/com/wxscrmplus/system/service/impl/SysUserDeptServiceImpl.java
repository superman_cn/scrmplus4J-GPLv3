package com.wxscrmplus.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.wxscrmplus.common.core.page.TableDataInfo;
import com.wxscrmplus.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.wxscrmplus.system.mapper.SysUserDeptMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.wxscrmplus.system.domain.bo.SysUserDeptBo;
import com.wxscrmplus.system.domain.vo.SysUserDeptVo;
import com.wxscrmplus.system.domain.SysUserDept;
import com.wxscrmplus.system.service.ISysUserDeptService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 用户与部门关联Service业务层处理
 *
 * @author www.wxscrmplus.com
 * @date 2023-03-15
 */
@RequiredArgsConstructor
@Service
public class SysUserDeptServiceImpl implements ISysUserDeptService {
// c0046c8671073a6da598163784a7891f

    private final SysUserDeptMapper baseMapper;

    /**
     * 查询用户与部门关联
     */
    @Override
    public SysUserDeptVo queryById(Long userId){
        return baseMapper.selectVoById(userId);
    }

    /**
     * 查询用户与部门关联列表
     */
    @Override
    public TableDataInfo<SysUserDeptVo> queryPageList(SysUserDeptBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<SysUserDept> lqw = buildQueryWrapper(bo);
        Page<SysUserDeptVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询用户与部门关联列表
     */
    @Override
    public List<SysUserDeptVo> queryList(SysUserDeptBo bo) {
        LambdaQueryWrapper<SysUserDept> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<SysUserDept> buildQueryWrapper(SysUserDeptBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<SysUserDept> lqw = Wrappers.lambdaQuery();
        return lqw;
    }

    /**
     * 新增用户与部门关联
     */
    @Override
    public Boolean insertByBo(SysUserDeptBo bo) {
        SysUserDept add = BeanUtil.toBean(bo, SysUserDept.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setUserId(add.getUserId());
        }
        return flag;
    }

    /**
     * 修改用户与部门关联
     */
    @Override
    public Boolean updateByBo(SysUserDeptBo bo) {
        SysUserDept update = BeanUtil.toBean(bo, SysUserDept.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(SysUserDept entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除用户与部门关联
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
