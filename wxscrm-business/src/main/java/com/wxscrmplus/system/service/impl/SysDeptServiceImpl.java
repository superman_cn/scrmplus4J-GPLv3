package com.wxscrmplus.system.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.lang.tree.Tree;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.wxscrmplus.common.constant.CacheNames;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.wxscrmplus.common.constant.UserConstants;
import com.wxscrmplus.common.core.domain.entity.SysDept;
import com.wxscrmplus.common.core.domain.entity.SysRole;
import com.wxscrmplus.common.core.domain.entity.SysUser;
import com.wxscrmplus.common.core.service.DeptService;
import com.wxscrmplus.common.exception.ServiceException;
import com.wxscrmplus.common.helper.DataBaseHelper;
import com.wxscrmplus.common.helper.LoginHelper;
import com.wxscrmplus.common.utils.StringUtils;
import com.wxscrmplus.common.utils.TreeBuildUtils;
import com.wxscrmplus.system.domain.SysUserDept;
import com.wxscrmplus.common.utils.redis.CacheUtils;
import com.wxscrmplus.common.utils.spring.SpringUtils;
import com.wxscrmplus.system.mapper.SysDeptMapper;
import com.wxscrmplus.system.mapper.SysRoleMapper;
import com.wxscrmplus.system.mapper.SysUserDeptMapper;
import com.wxscrmplus.system.mapper.SysUserMapper;
import com.wxscrmplus.system.service.ISysDeptService;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 部门管理 服务实现
 *
 * @author www.wxscrmplus.com
 */
@RequiredArgsConstructor
@Service
public class SysDeptServiceImpl implements ISysDeptService, DeptService {

    private final SysDeptMapper baseMapper;
    private final SysRoleMapper roleMapper;
    private final SysUserMapper userMapper;
    private final SysUserDeptMapper userDeptMapper;

    /**
     * 查询部门管理数据
     *
     * @param dept 部门信息
     * @return 部门信息集合
     */
    @Override
    public List<SysDept> selectDeptList(SysDept dept) {
        LambdaQueryWrapper<SysDept> lqw = new LambdaQueryWrapper<>();
// 7313d00b8fb68de1b175d6dedd012dcd
        lqw.eq(SysDept::getDelFlag, "0")
            .eq(ObjectUtil.isNotNull(dept.getDeptId()), SysDept::getDeptId, dept.getDeptId())
            .eq(ObjectUtil.isNotNull(dept.getParentId()), SysDept::getParentId, dept.getParentId())
            .like(StringUtils.isNotBlank(dept.getDeptName()), SysDept::getDeptName, dept.getDeptName())
            .eq(StringUtils.isNotBlank(dept.getStatus()), SysDept::getStatus, dept.getStatus())
            .orderByAsc(SysDept::getParentId)
            .orderByAsc(SysDept::getOrderNum);
        return baseMapper.selectDeptList(lqw);
    }

    /**
     * 查询部门树结构信息
     *
     * @param dept 部门信息
     * @return 部门树信息集合
     */
    @Override
    public List<Tree<Long>> selectDeptTreeList(SysDept dept) {
        List<SysDept> depts = this.selectDeptList(dept);
        return buildDeptTreeSelect(depts);
    }

    /**
     * 构建前端所需要下拉树结构
     *
     * @param depts 部门列表
     * @return 下拉树结构列表
     */
    @Override
    public List<Tree<Long>> buildDeptTreeSelect(List<SysDept> depts) {
        if (CollUtil.isEmpty(depts)) {
            return CollUtil.newArrayList();
        }
        return TreeBuildUtils.build(depts, (dept, tree) ->
            tree.setId(dept.getDeptId())
                .setParentId(dept.getParentId())
                .setName(dept.getDeptName())
                .setWeight(dept.getOrderNum()));
    }

    /**
     * 根据角色ID查询部门树信息
     *
     * @param roleId 角色ID
     * @return 选中部门列表
     */
    @Override
    public List<Long> selectDeptListByRoleId(Long roleId) {
        SysRole role = roleMapper.selectById(roleId);
        return baseMapper.selectDeptListByRoleId(roleId, role.getDeptCheckStrictly());
    }

    /**
     * 根据部门ID查询信息
     *
     * @param deptId 部门ID
     * @return 部门信息
     */
    @Cacheable(cacheNames = CacheNames.SYS_DEPT, key = "#deptId")
    @Override
    public SysDept selectDeptById(Long deptId) {
        SysDept dept = baseMapper.selectById(deptId);
        if (ObjectUtil.isNull(dept)) {
            return null;
        }
        SysDept parentDept = baseMapper.selectOne(new LambdaQueryWrapper<SysDept>()
            .select(SysDept::getDeptName).eq(SysDept::getDeptId, dept.getParentId()));
        dept.setParentName(ObjectUtil.isNotNull(parentDept) ? parentDept.getDeptName() : null);
        return dept;
    }

    /**
     * 通过部门ID查询部门名称
     *
     * @param deptIds 部门ID串逗号分隔
     * @return 部门名称串逗号分隔
     */
    @Override
    public String selectDeptNameByIds(String deptIds) {
        List<String> list = new ArrayList<>();
        for (Long id : StringUtils.splitTo(deptIds, Convert::toLong)) {
            SysDept dept = SpringUtils.getAopProxy(this).selectDeptById(id);
            if (ObjectUtil.isNotNull(dept)) {
                list.add(dept.getDeptName());
            }
        }
        return String.join(StringUtils.SEPARATOR, list);
    }

    /**
     * 根据ID查询所有子部门数（正常状态）
     *
     * @param deptId 部门ID
     * @return 子部门数
     */
    @Override
    public long selectNormalChildrenDeptById(Long deptId) {
        return baseMapper.selectCount(new LambdaQueryWrapper<SysDept>()
            .eq(SysDept::getStatus, UserConstants.DEPT_NORMAL)
            .apply(DataBaseHelper.findInSet(deptId, "ancestors")));
    }

    /**
     * 是否存在子节点
     *
     * @param deptId 部门ID
     * @return 结果
     */
    @Override
    public boolean hasChildByDeptId(Long deptId) {
        return baseMapper.exists(new LambdaQueryWrapper<SysDept>()
            .eq(SysDept::getParentId, deptId));
    }

    /**
     * 查询部门是否存在用户
     *
     * @param deptId 部门ID
     * @return 结果 true 存在 false 不存在
     */
    @Override
    public boolean checkDeptExistUser(Long deptId) {
        return userMapper.exists(new LambdaQueryWrapper<SysUser>()
            .eq(SysUser::getDeptId, deptId));
    }

    /**
     * 校验部门名称是否唯一
     *
     * @param dept 部门信息
     * @return 结果
     */
    @Override
    public boolean checkDeptNameUnique(SysDept dept) {
        boolean exist = baseMapper.exists(new LambdaQueryWrapper<SysDept>()
            .eq(SysDept::getDeptName, dept.getDeptName())
            .eq(SysDept::getParentId, dept.getParentId())
            .ne(ObjectUtil.isNotNull(dept.getDeptId()), SysDept::getDeptId, dept.getDeptId()));
        return !exist;
    }

    /**
     * 校验部门是否有数据权限
     *
     * @param deptId 部门id
     */
    @Override
    public void checkDeptDataScope(Long deptId) {
        if (!LoginHelper.isAdmin()) {
            SysDept dept = new SysDept();
            dept.setDeptId(deptId);
            List<SysDept> depts = this.selectDeptList(dept);
            if (CollUtil.isEmpty(depts)) {
                throw new ServiceException("没有权限访问部门数据！");
            }
        }
    }

    /**
     * 新增保存部门信息
     *
     * @param dept 部门信息
     * @return 结果
     */
    @Override
    public int insertDept(SysDept dept) {
        SysDept info = baseMapper.selectById(dept.getParentId());
        if (ObjectUtil.isNull(info)) {
            dept.setAncestors(dept.getParentId() + "");
            dept.setFullPathName(dept.getDeptName());
            return baseMapper.insert(dept);
        } else {
            // 如果父节点不为正常状态,则不允许新增子节点
            if (!UserConstants.DEPT_NORMAL.equals(info.getStatus())) {
                throw new ServiceException("部门停用，不允许新增");
            }
            dept.setAncestors(info.getAncestors() + "," + dept.getParentId());
            if (StrUtil.isNotBlank(info.getFullPathName())) {
                dept.setFullPathName(info.getFullPathName() + "/" + dept.getDeptName());
            } else {
                dept.setFullPathName(info.getDeptName() + "/" + dept.getDeptName());
            }
            return baseMapper.insert(dept);
        }
    }

    /**
     * 修改保存部门信息
     *
     * @param dept 部门信息
     * @return 结果
     */
    @CacheEvict(cacheNames = CacheNames.SYS_DEPT, key = "#dept.deptId")
    @Override
    public int updateDept(SysDept dept) {
        SysDept newParentDept = baseMapper.selectById(dept.getParentId());
        SysDept oldDept = baseMapper.selectById(dept.getDeptId());
        if (ObjectUtil.isNotNull(newParentDept) && ObjectUtil.isNotNull(oldDept)) {
            String newAncestors = newParentDept.getAncestors() + StringUtils.SEPARATOR + newParentDept.getDeptId();
            String oldAncestors = oldDept.getAncestors();
            dept.setAncestors(newAncestors);
            String newFullPathName;
            if (StrUtil.isNotBlank(newParentDept.getFullPathName())) {
                newFullPathName = newParentDept.getFullPathName() + "/" + dept.getDeptName();
            } else {
                newFullPathName = newParentDept.getDeptName() + "/" + dept.getDeptName();
            }
            String oldFullPathName = oldDept.getFullPathName();
            dept.setFullPathName(newFullPathName);
            updateDeptChildren(dept.getDeptId(), newAncestors, oldAncestors, newFullPathName, oldFullPathName);
        }
        if (ObjectUtil.isNull(newParentDept)) {
            dept.setFullPathName(dept.getDeptName());
        }
        int result = baseMapper.updateById(dept);
        if (UserConstants.DEPT_NORMAL.equals(dept.getStatus()) && StringUtils.isNotEmpty(dept.getAncestors())
            && !StringUtils.equals(UserConstants.DEPT_NORMAL, dept.getAncestors())) {
            // 如果该部门是启用状态，则启用该部门的所有上级部门
            updateParentDeptStatusNormal(dept);
        }
        return result;
    }

    /**
     * 修改该部门的父级部门状态
     *
     * @param dept 当前部门
     */
    private void updateParentDeptStatusNormal(SysDept dept) {
        String ancestors = dept.getAncestors();
        Long[] deptIds = Convert.toLongArray(ancestors);
        baseMapper.update(null, new LambdaUpdateWrapper<SysDept>()
            .set(SysDept::getStatus, UserConstants.DEPT_NORMAL)
            .in(SysDept::getDeptId, Arrays.asList(deptIds)));
    }

    /**
     * 修改子元素关系
     *
     * @param deptId       被修改的部门ID
     * @param newAncestors 新的父ID集合
     * @param oldAncestors 旧的父ID集合
     */
    public void updateDeptChildren(Long deptId, String newAncestors, String oldAncestors, String newFullPathName, String oldFullPathName) {
        List<SysDept> children = baseMapper.selectList(new LambdaQueryWrapper<SysDept>()
            .apply(DataBaseHelper.findInSet(deptId, "ancestors")));
        List<SysDept> list = new ArrayList<>();
        for (SysDept child : children) {
            SysDept dept = new SysDept();
            dept.setDeptId(child.getDeptId());
            dept.setAncestors(child.getAncestors().replaceFirst(oldAncestors, newAncestors));
            dept.setFullPathName(child.getFullPathName().replaceFirst(oldFullPathName, newFullPathName));
            list.add(dept);
        }
        if (CollUtil.isNotEmpty(list)) {
            if (baseMapper.updateBatchById(list)) {
                list.forEach(dept -> CacheUtils.evict(CacheNames.SYS_DEPT, dept.getDeptId()));
            }
        }
    }

    /**
     * 删除部门管理信息
     *
     * @param deptId 部门ID
     * @return 结果
     */
    @CacheEvict(cacheNames = CacheNames.SYS_DEPT, key = "#deptId")
    @Override
    public int deleteDeptById(Long deptId) {
        return baseMapper.deleteById(deptId);
    }

    @Override
    public List<Long> selectDeptListByUserId(Long userId) {
        List<Long> resultIds = new ArrayList<>();
        List<SysUserDept> sysUserDepts = userDeptMapper.selectList(Wrappers.query(SysUserDept.builder().userId(userId).build()));
        //获取主部门ID
        SysUser user = userMapper.selectById(userId);
        resultIds.add(user.getDeptId());
        if (CollUtil.isNotEmpty(sysUserDepts)) {
            List<Long> ids = sysUserDepts.stream().map(SysUserDept::getDeptId).collect(Collectors.toList());
            resultIds.addAll(ids);
        }
        return resultIds.stream().distinct().collect(Collectors.toList());
    }

    @Override
    public Long insertByFullPathName(String fullPathName) {
        Long resultId = null;
        List<String> nameList = Arrays.asList(fullPathName.split("/"));
        String tmpFullPathName = "";
        Long parentId = 0L;
        for (String name : nameList) {
            SysDept oldDept = null;
            if (parentId.equals(0L)) {
                //根部门使用系统的根部门
                oldDept = baseMapper.selectOne(Wrappers.query(new SysDept()).lambda().eq(SysDept::getParentId, parentId));
                tmpFullPathName = oldDept.getDeptName();
            } else {
                tmpFullPathName = tmpFullPathName + "/" + name;
                oldDept = baseMapper.selectOne(Wrappers.query(SysDept.builder().fullPathName(tmpFullPathName).build()));
            }
            SysDept saveDept = new SysDept();
            saveDept.setDeptName(name);
            saveDept.setParentId(parentId);
            saveDept.setStatus("0");
            saveDept.setOrderNum(1);
            if (ObjectUtil.isNull(oldDept)) {
                this.insertDept(saveDept);
                parentId = saveDept.getDeptId();
            } else {
                parentId = oldDept.getDeptId();
            }
            resultId = parentId;
        }
        return resultId;
    }

}
