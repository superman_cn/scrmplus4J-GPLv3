package com.wxscrmplus.system.service;

import com.wxscrmplus.system.domain.vo.SysUserDeptVo;
import com.wxscrmplus.system.domain.bo.SysUserDeptBo;
import com.wxscrmplus.common.core.page.TableDataInfo;
import com.wxscrmplus.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 用户与部门关联Service接口
 *
 * @author www.wxscrmplus.com
 * @date 2023-03-15
 */
public interface ISysUserDeptService {

    /**
     * 查询用户与部门关联
     */
    SysUserDeptVo queryById(Long userId);

    /**
     * 查询用户与部门关联列表
     */
    TableDataInfo<SysUserDeptVo> queryPageList(SysUserDeptBo bo, PageQuery pageQuery);

    /**
     * 查询用户与部门关联列表
     */
    List<SysUserDeptVo> queryList(SysUserDeptBo bo);

    /**
     * 新增用户与部门关联
     */
    Boolean insertByBo(SysUserDeptBo bo);

    /**
     * 修改用户与部门关联
     */
    Boolean updateByBo(SysUserDeptBo bo);

    /**
     * 校验并批量删除用户与部门关联信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
// b4f74bad885af1638cb38661c1a1c37d
