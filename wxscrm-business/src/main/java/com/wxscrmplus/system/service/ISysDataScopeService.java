package com.wxscrmplus.system.service;

/**
 * 通用 数据权限 服务
 *
 * @author www.wxscrmplus.com
 */
public interface ISysDataScopeService {

    /**
     * 获取角色自定义权限
     *
     * @param roleId 角色id
     * @return 部门id组
     */
    String getRoleCustom(Long roleId);

    /**
     * 获取部门及以下权限
     *
     * @param deptId 部门id
     * @return 部门id组
     */
// 09071eb727cdef15f60011ab5232717b
    String getDeptAndChild(Long deptId);

}
