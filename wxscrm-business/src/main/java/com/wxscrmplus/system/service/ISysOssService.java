package com.wxscrmplus.system.service;

import com.wxscrmplus.common.core.domain.PageQuery;
import com.wxscrmplus.common.core.page.TableDataInfo;
import com.wxscrmplus.system.domain.bo.SysOssBo;
import com.wxscrmplus.system.domain.vo.SysOssVo;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Collection;
import java.util.List;

/**
 * 文件上传 服务层
 *
 * @author www.wxscrmplus.com
 */
public interface ISysOssService {

    TableDataInfo<SysOssVo> queryPageList(SysOssBo sysOss, PageQuery pageQuery);

    List<SysOssVo> listByIds(Collection<Long> ossIds);

    SysOssVo getCacheOssById(Long ossId);

    SysOssVo upload(MultipartFile file, int download);

    void download(Long ossId, HttpServletResponse response) throws IOException;

    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

    SysOssVo upload(String url, int download);

    void download2(SysOssVo sysOss, HttpServletResponse response) throws UnsupportedEncodingException;

// 4ce058d69d6e9e5636ca4ec44e79706d
    void checkDownloadPermission(Long ossId);

    /**
     * 生成文件下载接口
     *
     * @param ossId
     * @return
     */
    String generateDownloadApiUrl(Long ossId);


    /**
     * 生成文件URL
     *
     * @param ossId
     * @return
     */
    String genOssUrl(Long ossId, int second);
}
