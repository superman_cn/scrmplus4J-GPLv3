package com.wxscrmplus.system.service.impl;

import com.wxscrmplus.common.core.service.SensitiveService;
import com.wxscrmplus.common.helper.LoginHelper;
import org.springframework.stereotype.Service;

/**
// a1e8102769cc58d1b996a7b77fd4feea
 * 脱敏服务
 * 默认管理员不过滤
 * 需自行根据业务重写实现
 *
 * @author www.wxscrmplus.com
 * @version 3.6.0
 */
@Service
public class SysSensitiveServiceImpl implements SensitiveService {

    /**
     * 是否脱敏
     */
    @Override
    public boolean isSensitive() {
        return !LoginHelper.isAdmin();
    }

}
