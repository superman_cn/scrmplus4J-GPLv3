package com.wxscrmplus.system.service.impl;

import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.io.FileTypeUtil;
import cn.hutool.core.io.IoUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.http.HttpUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wxscrmplus.common.constant.CacheNames;
import com.wxscrmplus.common.core.domain.PageQuery;
import com.wxscrmplus.common.core.page.TableDataInfo;
import com.wxscrmplus.common.core.service.OssService;
import com.wxscrmplus.common.exception.ServiceException;
import com.wxscrmplus.common.utils.BeanCopyUtils;
import com.wxscrmplus.common.utils.StringUtils;
import com.wxscrmplus.common.utils.file.FileUtils;
import com.wxscrmplus.common.utils.redis.CacheUtils;
import com.wxscrmplus.common.utils.spring.SpringUtils;
import com.wxscrmplus.oss.core.OssClient;
import com.wxscrmplus.oss.entity.UploadResult;
import com.wxscrmplus.oss.enumd.AccessPolicyType;
import com.wxscrmplus.oss.factory.OssFactory;
// 76b592f23683f5632b1175395f2c2493
import com.wxscrmplus.system.domain.SysOss;
import com.wxscrmplus.system.domain.bo.SysOssBo;
import com.wxscrmplus.system.domain.vo.SysOssVo;
import com.wxscrmplus.system.mapper.SysOssMapper;
import com.wxscrmplus.system.service.ISysOssService;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.util.*;

/**
 * 文件上传 服务层实现
 *
 * @author www.wxscrmplus.com
 */
@RequiredArgsConstructor
@Service
public class SysOssServiceImpl implements ISysOssService, OssService {

    private final SysOssMapper baseMapper;

    @Override
    public TableDataInfo<SysOssVo> queryPageList(SysOssBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<SysOss> lqw = buildQueryWrapper(bo);
        Page<SysOssVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
//        List<SysOssVo> filterResult = result.getRecords().stream().map(this::matchingUrl).collect(Collectors.toList());
//        result.setRecords(filterResult);
        return TableDataInfo.build(result);
    }

    @Override
    public List<SysOssVo> listByIds(Collection<Long> ossIds) {
        List<SysOssVo> list = new ArrayList<>();
        for (Long id : ossIds) {
            SysOssVo vo = SpringUtils.getAopProxy(this).getCacheOssById(id);
            if (ObjectUtil.isNotNull(vo)) {
                list.add(this.matchingUrl(vo));
            }
        }
        return list;
    }

    @Override
    public String selectUrlByIds(String ossIds) {
        List<String> list = new ArrayList<>();
        for (Long id : StringUtils.splitTo(ossIds, Convert::toLong)) {
            SysOssVo vo = SpringUtils.getAopProxy(this).getCacheOssById(id);
            if (ObjectUtil.isNotNull(vo)) {
                list.add(this.matchingUrl(vo).getUrl());
            }
        }
        return String.join(StringUtils.SEPARATOR, list);
    }

    private LambdaQueryWrapper<SysOss> buildQueryWrapper(SysOssBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<SysOss> lqw = Wrappers.lambdaQuery();
        lqw.like(StringUtils.isNotBlank(bo.getFileName()), SysOss::getFileName, bo.getFileName());
        lqw.like(StringUtils.isNotBlank(bo.getOriginalName()), SysOss::getOriginalName, bo.getOriginalName());
        lqw.eq(StringUtils.isNotBlank(bo.getFileSuffix()), SysOss::getFileSuffix, bo.getFileSuffix());
        lqw.eq(StringUtils.isNotBlank(bo.getUrl()), SysOss::getUrl, bo.getUrl());
        lqw.between(params.get("beginCreateTime") != null && params.get("endCreateTime") != null,
            SysOss::getCreateTime, params.get("beginCreateTime"), params.get("endCreateTime"));
        lqw.eq(StringUtils.isNotBlank(bo.getCreateBy()), SysOss::getCreateBy, bo.getCreateBy());
        lqw.eq(StringUtils.isNotBlank(bo.getService()), SysOss::getService, bo.getService());
        return lqw;
    }

    @Cacheable(cacheNames = CacheNames.SYS_OSS, key = "#ossId")
    @Override
    public SysOssVo getCacheOssById(Long ossId) {
        return baseMapper.selectVoById(ossId);
    }

    @Override
    public void download(Long ossId, HttpServletResponse response) throws IOException {
        SysOssVo sysOss = SpringUtils.getAopProxy(this).getCacheOssById(ossId);
        if (ObjectUtil.isNull(sysOss)) {
            throw new ServiceException("文件数据不存在!");
        }
        FileUtils.setAttachmentResponseHeader(response, sysOss.getOriginalName());
        response.setContentType(MediaType.APPLICATION_OCTET_STREAM_VALUE + "; charset=UTF-8");
        OssClient storage = OssFactory.instance();
        try (InputStream inputStream = storage.getObjectContent(sysOss.getUrl())) {
            int available = inputStream.available();
            IoUtil.copy(inputStream, response.getOutputStream(), available);
            response.setContentLength(available);
        } catch (Exception e) {
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public SysOssVo upload(MultipartFile file, int download) {
        String originalfileName = file.getOriginalFilename();
        String suffix = StringUtils.substring(originalfileName, originalfileName.lastIndexOf("."), originalfileName.length());
        OssClient storage = OssFactory.instance();
        UploadResult uploadResult;
        try {
            uploadResult = storage.uploadSuffix(file.getBytes(), suffix, file.getContentType());
        } catch (IOException e) {
            throw new ServiceException(e.getMessage());
        }
        // 保存文件信息
        SysOss oss = new SysOss();
        oss.setUrl(uploadResult.getUrl());
        oss.setFileSuffix(suffix);
        oss.setFileName(uploadResult.getFilename());
        oss.setOriginalName(originalfileName);
        oss.setService(storage.getConfigKey());
        oss.setDownload(download);
        baseMapper.insert(oss);
        SysOssVo sysOssVo = new SysOssVo();
        BeanCopyUtils.copy(oss, sysOssVo);
        return this.matchingUrl(sysOssVo);
    }

    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if (isValid) {
            // 做一些业务上的校验,判断是否需要校验
        }
        List<SysOss> list = baseMapper.selectBatchIds(ids);
        for (SysOss sysOss : list) {
            OssClient storage = OssFactory.instance(sysOss.getService());
            storage.delete(sysOss.getUrl());
            CacheUtils.evict(CacheNames.SYS_OSS, sysOss.getOssId());
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }

    /**
     * 匹配Url
     *
     * @param oss OSS对象
     * @return oss 匹配Url的OSS对象
     */
    private SysOssVo matchingUrl(SysOssVo oss) {
        //临时URL时长为120s
        return this.matchingUrl(oss, 120);
    }

    /**
     * 匹配Url
     *
     * @param oss OSS对象
     * @return oss 匹配Url的OSS对象
     */
    private SysOssVo matchingUrl(SysOssVo oss, int second) {
        OssClient storage = OssFactory.instance(oss.getService());
        if (AccessPolicyType.PRIVATE == storage.getAccessPolicy()) {
            oss.setUrl(storage.getPrivateUrl(oss.getFileName(), second));
        }
        return oss;
    }

    @Override
    public SysOssVo upload(String url, int download) {
        String suffix = null;
        OssClient storage = OssFactory.instance();
        UploadResult uploadResult;
        try {
            suffix = "." + FileTypeUtil.getType(new URL(url).openStream());
            String contentType = new URL(url).openConnection().getHeaderFields().get("Content-Type").get(0);
            uploadResult = storage.uploadSuffix(HttpUtil.downloadBytes(url), suffix, contentType);
        } catch (IOException e) {
            throw new ServiceException(e.getMessage());
        }
        // 保存文件信息
        SysOss oss = new SysOss();
        oss.setUrl(uploadResult.getUrl());
        oss.setFileSuffix(suffix);
        oss.setFileName(uploadResult.getFilename());
        oss.setOriginalName("网络图片" + suffix);
        oss.setService(storage.getConfigKey());
        oss.setDownload(download);
        baseMapper.insert(oss);
        SysOssVo sysOssVo = new SysOssVo();
        BeanCopyUtils.copy(oss, sysOssVo);
        return this.matchingUrl(sysOssVo);
    }

    @Override
    public void download2(SysOssVo sysOss, HttpServletResponse response) throws UnsupportedEncodingException {
        String fileSuffix = sysOss.getFileSuffix().replace(".", "");
        //mp4视频
        if ("mp4".equals(fileSuffix)) {
            response.addHeader("Content-Type", "video/mp4;charset=UTF-8");
        } else {
            if ("jpg".equals(fileSuffix) || "png".equals(fileSuffix) || "gif".equals(fileSuffix)) {
                response.setContentType("image/" + fileSuffix);
            } else {
                //如果是其他方式进行下载操作
                response.setContentType(MediaType.APPLICATION_OCTET_STREAM_VALUE + "; charset=UTF-8");
                FileUtils.setAttachmentResponseHeader(response, sysOss.getOriginalName());
            }
        }
        OssClient storage = OssFactory.instance();
        try (InputStream inputStream = storage.getObjectContent(sysOss.getFileName())) {
            int available = inputStream.available();
            IoUtil.copy(inputStream, response.getOutputStream(), available);
            response.setContentLength(available);
        } catch (Exception e) {
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public void checkDownloadPermission(Long ossId) {
        SysOssVo sysOssVo = this.getCacheOssById(ossId);
        if (sysOssVo.getDownload().equals(SysOss.DownloadFieldEnum.LOGIN.getValue())) {
            //不是公开的，判断是否已经登录
            StpUtil.checkLogin();
        } else if (sysOssVo.getDownload().equals(SysOss.DownloadFieldEnum.PERMISSION.getValue())) {
            //下载权限
            StpUtil.checkPermission("system:oss:download");
        } else {
            //TODO ---业务逻辑校验
        }

    }

    @Override
    public String generateDownloadApiUrl(Long ossId) {
        return "/system/oss/download/" + ossId;
    }

    @Override
    public String genOssUrl(Long ossId, int second) {
        SysOssVo ossVo = this.getCacheOssById(ossId);
        if (ObjectUtil.isNull(ossVo)) {
            return "";
        }
        return this.matchingUrl(ossVo, second).getUrl();
    }
}
