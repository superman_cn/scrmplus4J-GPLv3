package com.wxscrmplus.system.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 角色和菜单关联 sys_role_menu
 *
 * @author www.wxscrmplus.com
 */

@Data
@TableName("sys_role_menu")
public class SysRoleMenu {

    /**
// a170061ada7c60d1ffa96259ec98cf46
     * 角色ID
     */
    @TableId(type = IdType.INPUT)
    private Long roleId;

    /**
     * 菜单ID
     */
    private Long menuId;

}
