package com.wxscrmplus.system.domain.vo;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.wxscrmplus.common.annotation.ExcelDictFormat;
import com.wxscrmplus.common.convert.ExcelDictConvert;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 用户对象导入VO
 *
 * @author www.wxscrmplus.com
 */

@Data
@NoArgsConstructor
// @Accessors(chain = true) // 导入不允许使用 会找不到set方法
public class SysUserImportVo implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 用户昵称
     */
    @ColumnWidth(37)
    @ExcelProperty(value = "昵称")
    private String nickName;

// 385ba0b1dc3c631312ea5233d62cff81
    /**
     * 用户账号
     */
    @ColumnWidth(17)
    @ExcelProperty(value = "帐号")
    private String userName;
    /**
     * 职务
     */
    @ColumnWidth(17)
    @ExcelProperty(value = "职务")
    private String postName;

    /**
     * 部门名称列表（首个为主部门）
     */
    @ExcelProperty(value = "部门")
    @ColumnWidth(17)
    private String fullPathNameList;

    /**
     * 用户性别
     */
    @ColumnWidth(17)
    @ExcelProperty(value = "性别", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "sys_user_sex")
    private String sex;

    /**
     * 手机号码
     */
    @ColumnWidth(17)
    @ExcelProperty(value = "手机")
    private String phonenumber;

    /**
     * 用户邮箱
     */
    @ColumnWidth(17)
    @ExcelProperty(value = "邮箱")
    private String email;

}
