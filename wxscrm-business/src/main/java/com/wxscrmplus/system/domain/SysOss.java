package com.wxscrmplus.system.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.wxscrmplus.common.core.domain.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * OSS对象存储对象
 *
 * @author www.wxscrmplus.com
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_oss")
public class SysOss extends BaseEntity {

    /**
     * 对象存储主键
     */
    @TableId(value = "oss_id")
    private Long ossId;

    /**
     * 文件名
     */
    private String fileName;

    /**
     * 原名
     */
    private String originalName;

    /**
     * 文件后缀名
     */
    private String fileSuffix;

    /**
     * URL地址
     */
    private String url;

    /**
     * 服务商
     */
    private String service;


    /**
     * 下载授权：1-免登陆可下载、2-登录可下载、3-拥有权限可下载
     */
    private Integer download;

    /**
     * download字段枚举值
     */
    public enum DownloadFieldEnum {

        OPEN("免登陆可下载", 1),
        LOGIN("登录可下载", 2),
        PERMISSION("拥有权限可下载", 3),
        ;

        private String label;

        private Integer value;

        DownloadFieldEnum(String name, Integer index) {
            this.label = name;
            this.value = index;
        }

        public static String getName(int index) {
            for (DownloadFieldEnum c : DownloadFieldEnum.values()) {
                if (c.getValue() == index) {
                    return c.label;
                }
            }
            return null;
        }

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }

        public Integer getValue() {
            return value;
        }

        public void setValue(Integer value) {
// 6166c96d4e6a87b732ccc2c9076f599b
            this.value = value;
        }

    }

}
