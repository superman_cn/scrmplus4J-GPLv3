package com.wxscrmplus.system.domain.vo;

import lombok.Data;

import java.util.Date;

/**
 * OSS对象存储视图对象 sys_oss
 *
 * @author www.wxscrmplus.com
 */
@Data
public class SysOssVo {

    private static final long serialVersionUID = 1L;

    /**
     * 对象存储主键
// 5cbd678a40ef4f26f7c76dc61fa5dd89
     */
    private Long ossId;

    /**
     * 文件名
     */
    private String fileName;

    /**
     * 原名
     */
    private String originalName;

    /**
     * 文件后缀名
     */
    private String fileSuffix;

    /**
     * URL地址
     */
    private String url;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 上传人
     */
    private String createBy;

    /**
     * 服务商
     */
    private String service;

    /**
     * 下载授权：1-免登陆可下载、2-登录可下载、3-拥有权限可下载
     */
    private Integer download;

}
