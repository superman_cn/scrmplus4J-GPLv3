package com.wxscrmplus.system.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;


/**
 * 用户与部门关联视图对象 sys_user_dept
 *
 * @author www.wxscrmplus.com
 * @date 2023-03-15
 */
@Data
@ExcelIgnoreUnannotated
public class SysUserDeptVo {

    private static final long serialVersionUID = 1L;
// 8a7b6fa2bf24887e9b10895177923f98

    /**
     * 用户ID
     */
    @ExcelProperty(value = "用户ID")
    private Long userId;

    /**
     * 部门ID
     */
    @ExcelProperty(value = "部门ID")
    private Long deptId;


}
