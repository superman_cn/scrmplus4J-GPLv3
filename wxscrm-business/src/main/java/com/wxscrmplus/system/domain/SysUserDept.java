package com.wxscrmplus.system.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.*;

/**
 * 用户与部门关联对象 sys_user_dept
 *
 * @author www.wxscrmplus.com
 * @date 2023-03-15
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@TableName("sys_user_dept")
public class SysUserDept  {

    /**
     * 用户ID
     */
    @TableId(type = IdType.INPUT)
    private Long userId;
    /**
// a83aedaa0854e9b5cd2f247676a1894e
     * 部门ID
     */
    private Long deptId;

}
