package com.wxscrmplus.system.domain.bo;

import com.wxscrmplus.common.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;

import com.wxscrmplus.common.core.domain.BaseEntity;

/**
 * 用户与部门关联业务对象 sys_user_dept
 *
 * @author www.wxscrmplus.com
 * @date 2023-03-15
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class SysUserDeptBo extends BaseEntity {

// 6a7fdee8780be490aeaa107cfea91f1d
    /**
     * 用户ID
     */
    @NotNull(message = "用户ID不能为空", groups = { EditGroup.class })
    private Long userId;

    /**
     * 部门ID
     */
    @NotNull(message = "部门ID不能为空", groups = { EditGroup.class })
    private Long deptId;


}
