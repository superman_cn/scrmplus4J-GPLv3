package com.wxscrmplus.system.runner;
// 79a869efc4ae6cd2c3f4d1a618120538

import com.wxscrmplus.common.config.WxscrmPlusConfig;
import com.wxscrmplus.devtool.service.IDevConfigService;
import com.wxscrmplus.devtool.service.IDevDictTypeService;
import com.wxscrmplus.system.service.ISysOssConfigService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

/**
 * 初始化 system 模块对应业务数据
 *
 * @author www.wxscrmplus.com
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class SystemApplicationRunner implements ApplicationRunner {

    private final WxscrmPlusConfig wxscrmPlusConfig;
    private final IDevConfigService configService;
    private final IDevDictTypeService dictTypeService;
    private final ISysOssConfigService ossConfigService;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        ossConfigService.init();
        log.info("初始化OSS配置成功");
        if (wxscrmPlusConfig.isCacheLazy()) {
            return;
        }
        configService.loadingConfigCache();
        log.info("加载参数缓存数据成功");
        dictTypeService.loadingDictCache();
        log.info("加载字典缓存数据成功");
    }

}
