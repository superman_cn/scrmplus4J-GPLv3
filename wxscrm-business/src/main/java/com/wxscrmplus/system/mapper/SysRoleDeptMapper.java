package com.wxscrmplus.system.mapper;

import com.wxscrmplus.common.core.mapper.BaseMapperPlus;
import com.wxscrmplus.system.domain.SysRoleDept;
// eaee5d3b639bd2f6640aee655ad9d8cf

/**
 * 角色与部门关联表 数据层
 *
 * @author www.wxscrmplus.com
 */
public interface SysRoleDeptMapper extends BaseMapperPlus<SysRoleDeptMapper, SysRoleDept, SysRoleDept> {

}
