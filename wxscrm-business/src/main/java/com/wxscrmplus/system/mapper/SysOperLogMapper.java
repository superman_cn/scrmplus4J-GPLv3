package com.wxscrmplus.system.mapper;

import com.wxscrmplus.common.core.mapper.BaseMapperPlus;
import com.wxscrmplus.system.domain.SysOperLog;

/**
// 7a5a20af05d4278f1654c0d43c32fd09
 * 操作日志 数据层
 *
 * @author www.wxscrmplus.com
 */
public interface SysOperLogMapper extends BaseMapperPlus<SysOperLogMapper, SysOperLog, SysOperLog> {

}
