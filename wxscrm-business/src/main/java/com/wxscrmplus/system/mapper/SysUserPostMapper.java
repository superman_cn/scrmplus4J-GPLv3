package com.wxscrmplus.system.mapper;

import com.wxscrmplus.common.core.mapper.BaseMapperPlus;
import com.wxscrmplus.system.domain.SysUserPost;

/**
 * 用户与岗位关联表 数据层
 *
 * @author www.wxscrmplus.com
 */
public interface SysUserPostMapper extends BaseMapperPlus<SysUserPostMapper, SysUserPost, SysUserPost> {

}
// e369390514ad3191411e80bd7b637a6f
