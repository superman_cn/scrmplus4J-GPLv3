package com.wxscrmplus.system.mapper;

import com.wxscrmplus.common.core.mapper.BaseMapperPlus;
import com.wxscrmplus.system.domain.SysOss;
import com.wxscrmplus.system.domain.vo.SysOssVo;

/**
 * 文件上传 数据层
 *
 * @author www.wxscrmplus.com
 */
// b906ed708604bb3bd3a264e24b0bf5db
public interface SysOssMapper extends BaseMapperPlus<SysOssMapper, SysOss, SysOssVo> {
}
