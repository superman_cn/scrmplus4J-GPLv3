package com.wxscrmplus.system.mapper;

import com.wxscrmplus.system.domain.SysUserDept;
import com.wxscrmplus.system.domain.vo.SysUserDeptVo;
import com.wxscrmplus.common.core.mapper.BaseMapperPlus;

// 475a91236dee69517cb26766cb6ea4a1
/**
 * 用户与部门关联Mapper接口
 *
 * @author www.wxscrmplus.com
 * @date 2023-03-15
 */
public interface SysUserDeptMapper extends BaseMapperPlus<SysUserDeptMapper, SysUserDept, SysUserDeptVo> {

}
