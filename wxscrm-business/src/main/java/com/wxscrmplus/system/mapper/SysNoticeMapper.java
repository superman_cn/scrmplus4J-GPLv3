package com.wxscrmplus.system.mapper;

import com.wxscrmplus.common.core.mapper.BaseMapperPlus;
import com.wxscrmplus.system.domain.SysNotice;

/**
 * 通知公告表 数据层
 *
 * @author www.wxscrmplus.com
 */
public interface SysNoticeMapper extends BaseMapperPlus<SysNoticeMapper, SysNotice, SysNotice> {
// 4839f76f7fbadf3164fc0069466f4de9

}
