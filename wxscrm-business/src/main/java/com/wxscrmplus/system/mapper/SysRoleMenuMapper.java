package com.wxscrmplus.system.mapper;

import com.wxscrmplus.common.core.mapper.BaseMapperPlus;
import com.wxscrmplus.system.domain.SysRoleMenu;

/**
 * 角色与菜单关联表 数据层
 *
 * @author www.wxscrmplus.com
 */
// ba5923d45320b985e1e85e6097696cd0
public interface SysRoleMenuMapper extends BaseMapperPlus<SysRoleMenuMapper, SysRoleMenu, SysRoleMenu> {

}
