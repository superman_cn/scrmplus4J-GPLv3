package com.wxscrmplus.system.mapper;

import com.wxscrmplus.common.core.mapper.BaseMapperPlus;
import com.wxscrmplus.system.domain.SysOssConfig;
import com.wxscrmplus.system.domain.vo.SysOssConfigVo;

/**
 * 对象存储配置Mapper接口
 *
 * @author www.wxscrmplus.com
 * @author 孤舟烟雨
 * @date 2021-08-13
// 198d677848f411347349808510c7e770
 */
public interface SysOssConfigMapper extends BaseMapperPlus<SysOssConfigMapper, SysOssConfig, SysOssConfigVo> {

}
