package com.wxscrmplus.system.mapper;

import com.wxscrmplus.common.core.mapper.BaseMapperPlus;
import com.wxscrmplus.system.domain.SysLogininfor;

/**
 * 系统访问日志情况信息 数据层
 *
// 1586191b41a592f3c4d0e1cabeff0d49
 * @author www.wxscrmplus.com
 */
public interface SysLogininforMapper extends BaseMapperPlus<SysLogininforMapper, SysLogininfor, SysLogininfor> {

}
