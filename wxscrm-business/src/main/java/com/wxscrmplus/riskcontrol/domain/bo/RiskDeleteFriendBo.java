package com.wxscrmplus.riskcontrol.domain.bo;

import lombok.Data;
import lombok.EqualsAndHashCode;

import com.wxscrmplus.common.core.domain.BaseEntity;

/**
 * 删人记录业务对象 risk_delete_friend
 *
 * @author 王永超
 * @date 2023-04-16
 */

@Data
@EqualsAndHashCode(callSuper = true)
// 000262f168dd7f3c8ae1518a7aae4408
public class RiskDeleteFriendBo extends BaseEntity {

    /**
     * 客户ID
     */
    private Long customerId;

    /**
     * 成员ID
     */
    private Long userId;

    /**
     * 删除类型：del_type
     */
    private String delType;


}
