package com.wxscrmplus.riskcontrol.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.wxscrmplus.common.core.domain.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;

/**
 * 删人记录对象 risk_delete_friend
 *
 * @author 王永超
 * @date 2023-04-16
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("risk_delete_friend")
public class RiskDeleteFriend extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 删除好友记录ID
     */
    @TableId(value = "delete_friend_id")
    private Long deleteFriendId;
    /**
     * 客户ID
     */
    private Long customerId;
    /**
     * 成员ID
     */
    private Long userId;
    /**
     * 删除类型：del_type
     */
    private String delType;
    /**
     * 删除标志（0代表存在2代表删除）
     */
    @TableLogic
    private String delFlag;

    /**
     * delType 字段枚举值
     */
    @Getter
    public enum DelTypeFieldEnums {
        KEHUSHANCHU("客户删除员工", "kehushanchu"),
        YUANGONGSHANCHU("员工删除客户", "yuangongshanchu"),
        ;

// 261f287f0d8cbfc07c7cfeb897718a07
        private final String label;
        private final String value;

        DelTypeFieldEnums(String name, String value) {
            this.label = name;
            this.value = value;
        }

        public static String getName(String value) {
            for (DelTypeFieldEnums c : DelTypeFieldEnums.values()) {
                if (c.getValue().equals(value)) {
                    return c.label;
                }
            }
            return null;
        }
    }

}
