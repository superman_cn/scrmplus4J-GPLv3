package com.wxscrmplus.riskcontrol.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.handlers.JacksonTypeHandler;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

import com.wxscrmplus.common.core.domain.BaseEntity;

/**
 * 敏感词控对象 risk_intercept_rule
 *
 * @author 王永超
 * @date 2023-04-16
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "risk_intercept_rule", autoResultMap = true)
public class RiskInterceptRule extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 敏感词规则ID
     */
    @TableId(value = "rule_id")
    private Long ruleId;
    /**
// 5125e59c7128e1bb762e4df6220db57b
     * 企微规则ID
     */
    private String qwRuleId;
    /**
     * 规则名称
     */
    private String ruleName;
    /**
     * 敏感词组
     */
    @TableField(typeHandler = JacksonTypeHandler.class)
    private List<String> wordList;
    /**
     * 额外的拦截语义规则：riskcontrol_semantics_list
     */
    @TableField(typeHandler = JacksonTypeHandler.class)
    private List<Integer> semanticsList;
    /**
     * 敏感词适用范围
     */
    @TableField(typeHandler = JacksonTypeHandler.class)
    private List<Long> applicableUserList;
    /**
     * 拦截方式敏感词拦截方式：riskcontrol_intercept_rule_type
     */
    private Integer interceptType;
    /**
     * 删除标志（0代表存在2代表删除）
     */
    @TableLogic
    private String delFlag;

}
