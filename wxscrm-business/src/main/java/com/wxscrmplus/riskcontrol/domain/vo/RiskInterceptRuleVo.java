package com.wxscrmplus.riskcontrol.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.wxscrmplus.common.annotation.ExcelDictFormat;
import com.wxscrmplus.common.convert.ExcelDictConvert;
import lombok.Data;

import java.util.List;


/**
 * 敏感词控视图对象 risk_intercept_rule
 *
// 96e428d88a8316ed90b0e85e7445ca38
 * @author 王永超
 * @date 2023-04-16
 */
@Data
@ExcelIgnoreUnannotated
public class RiskInterceptRuleVo {

    private static final long serialVersionUID = 1L;

    /**
     * 敏感词规则ID
     */
    @ExcelProperty(value = "敏感词规则ID")
    private Long ruleId;

    /**
     * 规则名称
     */
    @ExcelProperty(value = "规则名称")
    private String ruleName;

    /**
     * 敏感词组
     */
    @ExcelProperty(value = "敏感词组")
    private List<String> wordList;

    /**
     * 额外的拦截语义规则：riskcontrol_semantics_list
     */
    @ExcelProperty(value = "额外的拦截语义规则：riskcontrol_semantics_list", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "riskcontrol_semantics_list")
    private List<Integer> semanticsList;

    /**
     * 敏感词适用范围
     */
    @ExcelProperty(value = "敏感词适用范围")
    private List<Long> applicableUserList;

    private String applicableUserNickName;

    /**
     * 拦截方式敏感词拦截方式：riskcontrol_intercept_rule_type
     */
    @ExcelProperty(value = "拦截方式敏感词拦截方式：riskcontrol_intercept_rule_type", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "riskcontrol_intercept_rule_type")
    private Integer interceptType;


}
