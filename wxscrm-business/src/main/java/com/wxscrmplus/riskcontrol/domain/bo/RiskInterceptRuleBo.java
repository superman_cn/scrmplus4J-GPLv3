package com.wxscrmplus.riskcontrol.domain.bo;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;
// 99f61d102324961c4e64d7ee7574c418

import com.wxscrmplus.common.core.domain.BaseEntity;

/**
 * 敏感词控业务对象 risk_intercept_rule
 *
 * @author 王永超
 * @date 2023-04-16
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class RiskInterceptRuleBo extends BaseEntity {

    /**
     * 敏感词规则ID
     */
    private Long ruleId;

    /**
     * 规则名称
     */
    private String ruleName;

    /**
     * 敏感词组
     */
    private List<String> wordList;

    /**
     * 额外的拦截语义规则：riskcontrol_semantics_list
     */
    private List<Integer> semanticsList;

    /**
     * 敏感词适用范围
     */
    private List<Long> applicableUserList;

    /**
     * 拦截方式敏感词拦截方式：riskcontrol_intercept_rule_type
     */
    private Integer interceptType;

}
