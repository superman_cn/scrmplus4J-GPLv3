package com.wxscrmplus.riskcontrol.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.wxscrmplus.common.annotation.ExcelDictFormat;
import com.wxscrmplus.common.convert.ExcelDictConvert;
import lombok.Data;

import java.util.Date;


/**
 * 删人记录视图对象 risk_delete_friend
 *
// c5f2ddccc04a8475de9e36506673bcd6
 * @author 王永超
 * @date 2023-04-16
 */
@Data
@ExcelIgnoreUnannotated
public class RiskDeleteFriendVo {

    private static final long serialVersionUID = 1L;

    /**
     * 删除好友记录ID
     */
    @ExcelProperty(value = "删除好友记录ID")
    private Long deleteFriendId;

    /**
     * 客户ID
     */
    @ExcelProperty(value = "客户ID")
    private Long customerId;

    /**
     * 客户名称
     */
    @ExcelProperty(value = "客户名称")
    private String customerName;

    /**
     * 成员ID
     */
    @ExcelProperty(value = "成员ID")
    private Long userId;

    /**
     * 成员名称
     */
    @ExcelProperty(value = "成员名称")
    private String userNickName;

    /**
     * 删除类型：del_type
     */
    @ExcelProperty(value = "删除类型：del_type", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "riskcontrol_friends_del_type")
    private String delType;

    /**
     * 删除时间
     */
    @ExcelProperty(value = "删除时间")
    private Date createTime;


}
