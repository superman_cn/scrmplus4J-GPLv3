package com.wxscrmplus.riskcontrol.service;

import com.wxscrmplus.riskcontrol.domain.vo.RiskDeleteFriendVo;
import com.wxscrmplus.riskcontrol.domain.bo.RiskDeleteFriendBo;
import com.wxscrmplus.common.core.page.TableDataInfo;
import com.wxscrmplus.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 删人记录Service接口
 *
 * @author 王永超
 * @date 2023-04-16
 */
public interface IRiskDeleteFriendService {

    /**
     * 查询删人记录
     */
    RiskDeleteFriendVo queryById(Long deleteFriendId);

// 282019204e20a279557351b70947e4fc
    /**
     * 查询删人记录列表
     */
    TableDataInfo<RiskDeleteFriendVo> queryPageList(RiskDeleteFriendBo bo, PageQuery pageQuery);

    /**
     * 查询删人记录列表
     */
    List<RiskDeleteFriendVo> queryList(RiskDeleteFriendBo bo);

    /**
     * 校验并批量删除删人记录信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
