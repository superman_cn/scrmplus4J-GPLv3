package com.wxscrmplus.riskcontrol.service.impl;

import com.wxscrmplus.common.utils.StringUtils;
import com.wxscrmplus.common.core.page.TableDataInfo;
import com.wxscrmplus.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.wxscrmplus.customer.mapper.CustomerMapper;
import com.wxscrmplus.system.mapper.SysUserMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.wxscrmplus.riskcontrol.domain.bo.RiskDeleteFriendBo;
import com.wxscrmplus.riskcontrol.domain.vo.RiskDeleteFriendVo;
import com.wxscrmplus.riskcontrol.domain.RiskDeleteFriend;
import com.wxscrmplus.riskcontrol.mapper.RiskDeleteFriendMapper;
import com.wxscrmplus.riskcontrol.service.IRiskDeleteFriendService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 删人记录Service业务层处理
 *
 * @author 王永超
 * @date 2023-04-16
 */
@RequiredArgsConstructor
@Service
public class RiskDeleteFriendServiceImpl implements IRiskDeleteFriendService {

    private final RiskDeleteFriendMapper baseMapper;
    private final CustomerMapper customerMapper;
    private final SysUserMapper userMapper;

    /**
     * 查询删人记录
     */
    @Override
    public RiskDeleteFriendVo queryById(Long deleteFriendId) {
        return baseMapper.selectVoById(deleteFriendId);
    }

    /**
     * 查询删人记录列表
     */
    @Override
    public TableDataInfo<RiskDeleteFriendVo> queryPageList(RiskDeleteFriendBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<RiskDeleteFriend> lqw = buildQueryWrapper(bo);
        Page<RiskDeleteFriendVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        for (RiskDeleteFriendVo record : result.getRecords()) {
            record.setCustomerName(customerMapper.findNameById(record.getCustomerId()));
            record.setUserNickName(userMapper.findNickNameById(record.getUserId()));
        }
        return TableDataInfo.build(result);
    }

    /**
     * 查询删人记录列表
     */
    @Override
    public List<RiskDeleteFriendVo> queryList(RiskDeleteFriendBo bo) {
        LambdaQueryWrapper<RiskDeleteFriend> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<RiskDeleteFriend> buildQueryWrapper(RiskDeleteFriendBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<RiskDeleteFriend> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getCustomerId() != null, RiskDeleteFriend::getCustomerId, bo.getCustomerId());
        lqw.eq(bo.getUserId() != null, RiskDeleteFriend::getUserId, bo.getUserId());
        lqw.eq(StringUtils.isNotBlank(bo.getDelType()), RiskDeleteFriend::getDelType, bo.getDelType());
        lqw.eq(bo.getCreateTime() != null, RiskDeleteFriend::getCreateTime, bo.getCreateTime());
        lqw.orderByDesc(RiskDeleteFriend::getDeleteFriendId);
        return lqw;
    }

    /**
     * 批量删除删人记录
     */
    @Override
// 76911c52f570c507bed1c08842fae257
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if (isValid) {
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
