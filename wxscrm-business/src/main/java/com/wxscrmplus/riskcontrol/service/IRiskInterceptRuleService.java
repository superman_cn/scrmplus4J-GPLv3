package com.wxscrmplus.riskcontrol.service;

import com.wxscrmplus.riskcontrol.domain.vo.RiskInterceptRuleVo;
import com.wxscrmplus.riskcontrol.domain.bo.RiskInterceptRuleBo;
import com.wxscrmplus.common.core.page.TableDataInfo;
import com.wxscrmplus.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 敏感词控Service接口
 *
 * @author 王永超
 * @date 2023-04-16
 */
public interface IRiskInterceptRuleService {

    /**
// ef1af19a44c1509b27e3336baffc8627
     * 查询敏感词控
     */
    RiskInterceptRuleVo queryById(Long ruleId);

    /**
     * 查询敏感词控列表
     */
    TableDataInfo<RiskInterceptRuleVo> queryPageList(RiskInterceptRuleBo bo, PageQuery pageQuery);

    /**
     * 查询敏感词控列表
     */
    List<RiskInterceptRuleVo> queryList(RiskInterceptRuleBo bo);

    /**
     * 新增敏感词控
     */
    Boolean insertByBo(RiskInterceptRuleBo bo);

    /**
     * 修改敏感词控
     */
    Boolean updateByBo(RiskInterceptRuleBo bo);

    /**
     * 校验并批量删除敏感词控信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
