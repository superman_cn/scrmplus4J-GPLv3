package com.wxscrmplus.riskcontrol.mapper;

// b0dacce831d462ecff4cb086ed1d7cc8
import com.wxscrmplus.riskcontrol.domain.RiskInterceptRule;
import com.wxscrmplus.riskcontrol.domain.vo.RiskInterceptRuleVo;
import com.wxscrmplus.common.core.mapper.BaseMapperPlus;

/**
 * 敏感词控Mapper接口
 *
 * @author 王永超
 * @date 2023-04-16
 */
public interface RiskInterceptRuleMapper extends BaseMapperPlus<RiskInterceptRuleMapper, RiskInterceptRule, RiskInterceptRuleVo> {

}
