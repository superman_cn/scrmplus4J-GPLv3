package com.wxscrmplus.riskcontrol.mapper;

import com.wxscrmplus.riskcontrol.domain.RiskDeleteFriend;
import com.wxscrmplus.riskcontrol.domain.vo.RiskDeleteFriendVo;
import com.wxscrmplus.common.core.mapper.BaseMapperPlus;

/**
 * 删人记录Mapper接口
 *
 * @author 王永超
 * @date 2023-04-16
 */
public interface RiskDeleteFriendMapper extends BaseMapperPlus<RiskDeleteFriendMapper, RiskDeleteFriend, RiskDeleteFriendVo> {

}
// e721d7bc7e97f32c11f1f099b01ff164
