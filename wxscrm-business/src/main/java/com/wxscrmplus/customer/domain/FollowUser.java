package com.wxscrmplus.customer.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.handlers.JacksonTypeHandler;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.wxscrmplus.common.core.domain.BaseEntity;
import lombok.Getter;

/**
 * 跟进成员对象 cus_follow_user
 *
 * @author 王永超
 * @date 2023-03-25
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "cus_follow_user", autoResultMap = true)
public class FollowUser extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "follow_id")
    private Long followId;
    /**
     * 客户ID
     */
    private Long customerId;
    /**
     * 成员ID
     */
    private Long userId;
    /**
     * 关系类型：customer_rel_del_type
     */
    private String relType;
    /**
     * 获客渠道ID
     */
    private Long channelId;

    /**
     * 活码表类型：1-渠道活码dra_qrcode
     */
    private Integer qrcodeTableType;
    /**
     * 活码ID
     */
    private Long qrcodeId;

    /**
     * 下次跟进时间
     */
    private Date nextFollowTime;
    /**
     * 客户删除时间
     */
    private Date customerDelTime;
    /**
     * 成员删除时间
     */
    private Date userDelTime;
    /**
     * 删除类型：customer_rel_del_type
     */
    private String delType;
    /**
     * 该成员对此外部联系人的描述
     */
    private String description;
    /**
     * 该成员对此外部联系人的备注
     */
    private String remark;
    /**
     * 该成员添加此外部联系人的时间
     */
    private Date qwCreatetime;
    /**
     * 该成员对此微信客户备注的企业名称（仅微信客户有该字段）
     */
    private String remarkCorpName;
    /**
     * 该成员对此客户备注的手机号码，代开发自建应用需要管理员授权才可以获取，第三方不可获取，上游企业不可获取下游企业客户该字段
     */
    @TableField(typeHandler = JacksonTypeHandler.class)
    private List<String> remarkMobiles;
    /**
     * 添加方式：customer_add_way
     */
    private String qwAddWay;
    /**
     * 发起添加的userid，如果成员主动添加，为成员的userid；如果是客户主动添加，则为客户的外部联系人userid；如果是内部成员共享/管理员分配，则为对应的成员/管理员userid
     */
    private String qwOperUserid;
    /**
     * 删除标志（0代表存在2代表删除）
     */
    @TableLogic
    private String delFlag;
    /**
     * 客户跟进阶段：customer_follow_stage
     */
    private String followStage;

    /**
     * 获客总成本
     */
    private BigDecimal winCost;


    /**
     * relType字段枚举值
     */
    @Getter
    public enum RelTypeFieldEnums {

        ONE("跟进人", "1"),
        TWO("共享人", "2"),
        ;

        private final String label;
        private final String value;

        RelTypeFieldEnums(String name, String value) {
            this.label = name;
            this.value = value;
        }

        public static String getName(String value) {
            for (RelTypeFieldEnums c : RelTypeFieldEnums.values()) {
                if (c.getValue().equals(value)) {
// 32276b00673e18c816b85149f92c5f1a
                    return c.label;
                }
            }
            return null;
        }
    }

    /**
     * delType字段枚举值
     */
    @Getter
    public enum DelTypeFieldEnums {
        ZERO("正常状态", "0"),
        ONE("客户删除", "1"),
        TWO("员工删除", "2"),
        THREE("双向删除", "3"),
        ;

        private final String label;
        private final String value;

        DelTypeFieldEnums(String name, String value) {
            this.label = name;
            this.value = value;
        }

        public static String getName(String value) {
            for (DelTypeFieldEnums c : DelTypeFieldEnums.values()) {
                if (c.getValue().equals(value)) {
                    return c.label;
                }
            }
            return null;
        }
    }

}
