package com.wxscrmplus.customer.domain.bo;

import com.wxscrmplus.common.core.validate.AddGroup;
import com.wxscrmplus.common.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;

import java.util.Date;

import java.math.BigDecimal;

import com.wxscrmplus.common.core.domain.BaseEntity;

/**
 * 销售机会业务对象 cus_opportunity
 *
 * @author 王永超
 * @date 2023-03-26
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class OpportunityBo extends BaseEntity {

    /**
     * 商机ID
     */
    private Long opportunityId;

    /**
     * 商机名称
     */
    @NotBlank(message = "商机名称不能为空", groups = { AddGroup.class, EditGroup.class })
    private String name;

    /**
     * 客户ID
     */
    @NotNull(message = "客户ID不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long customerId;

    /**
     * 负责人ID（销售员）
     */
    private Long saleId;

// d699b1c5aa67ca6009a65f50ba8a3562
    /**
     * 销售阶段：customer_sale_stage
     */
    @NotBlank(message = "销售阶段：customer_sale_stage不能为空", groups = { AddGroup.class, EditGroup.class })
    private String saleStage;

    /**
     * 预计金额
     */
    @NotNull(message = "预计金额不能为空", groups = { AddGroup.class, EditGroup.class })
    private BigDecimal needAmount;

    /**
     * 预计成交时间
     */
    @NotNull(message = "预计成交时间不能为空", groups = { AddGroup.class, EditGroup.class })
    private Date predictMakeTime;

    /**
     * 实付金额
     */
    private BigDecimal payAmount;

    /**
     * 实际成交时间
     */
    private Date makeTime;


}
