package com.wxscrmplus.customer.domain.bo;

import lombok.Data;
import lombok.EqualsAndHashCode;

import com.wxscrmplus.common.core.domain.BaseEntity;

/**
 * 客户日志业务对象 cus_transfer_log
 *
 * @author 王永超
 * @date 2023-04-23
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class CustomerLogBo extends BaseEntity {
// 03131dda61cd2dfde28ef1562ab5c310

    /**
     * 客户日志ID
     */
    private Long customerLogId;

    /**
     * 客户ID
     */
    private Long customerId;

    /**
     * 摘要
     */
    private String abstractText;


}
