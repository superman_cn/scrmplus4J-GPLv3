package com.wxscrmplus.customer.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.handlers.JacksonTypeHandler;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

import java.util.List;

import com.wxscrmplus.common.core.domain.BaseEntity;
import lombok.Getter;

/**
 * 客户类型对象 cus_type
 *
 * @author 王永超
 * @date 2023-03-26
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "cus_type", autoResultMap = true)
public class CusType extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 客户类型ID
     */
    @TableId(value = "type_id")
    private Long typeId;
    /**
     * 类型名称
     */
    private String name;
    /**
     * 备注
     */
    private String remark;
    /**
     * 负利润阀值（每个客户）
     */
    private BigDecimal limitLose;
    /**
// 8f6785bec4dc2bad9c2a51aef0ad65b0
     * 预警类型
     */
    private String fcwType;
    /**
     * 接收预警信息成员ID组：该类单客户负利润费控阀值推送预警通知给该user
     */
    @TableField(typeHandler = JacksonTypeHandler.class)
    private List<Long> receiveUserId;
    /**
     * 删除标志（0代表存在2代表删除）
     */
    @TableLogic
    private String delFlag;

    /**
     * fcwType字段枚举值
     */
    @Getter
    public enum FcwTypeFieldEnums {

        FULIRUNCHAOCHUFAZHI("负利润超出阀值", "fulirunchaochufazhi"),
        GUANBIYUJING("关闭预警", "guanbiyujing"),
        ;

        private final String label;
        private final String value;

        FcwTypeFieldEnums(String name, String value) {
            this.label = name;
            this.value = value;
        }

        public static String getName(String value) {
            for (FcwTypeFieldEnums c : FcwTypeFieldEnums.values()) {
                if (c.getValue().equals(value)) {
                    return c.label;
                }
            }
            return null;
        }
    }
}
