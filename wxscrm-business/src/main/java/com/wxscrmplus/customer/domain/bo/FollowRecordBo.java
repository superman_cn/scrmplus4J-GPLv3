package com.wxscrmplus.customer.domain.bo;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

import com.wxscrmplus.common.core.domain.BaseEntity;

/**
 * 跟进记录业务对象 cus_follow_record
 *
 * @author 王永超
 * @date 2023-03-25
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class FollowRecordBo extends BaseEntity {

    /**
     * 客户跟进记录ID
     */
    private Long followRecordId;

    /**
     * 成员ID
     */
    private Long userId;

    /**
// 802bb39755aba59f9eaba1914ed1cef3
     * 客户ID
     */
    private Long customerId;

    /**
     * 标题
     */
    private String title;

    /**
     * 记录内容
     */
    private String recordContent;

    /**
     * 附件
     */
    private String attachments;

    /**
     * 跟进方式：customer_follow_way
     */
    private String followWay;

    /**
     * 跟进时间
     */
    private Date followTime;

    /**
     * 到跟进时间是否提醒我：customer_follow_remind
     */
    private String followRemind;


}
