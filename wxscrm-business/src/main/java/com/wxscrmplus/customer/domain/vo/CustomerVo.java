package com.wxscrmplus.customer.domain.vo;

import java.math.BigDecimal;
import java.util.Date;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.wxscrmplus.common.annotation.ExcelDictFormat;
import com.wxscrmplus.common.convert.ExcelDictConvert;
import lombok.Data;

import java.util.List;


/**
 * 客户视图对象 cus_customer
 *
 * @author 王永超
 * @date 2023-03-24
 */
@Data
@ExcelIgnoreUnannotated
public class CustomerVo {

    private static final long serialVersionUID = 1L;

    /**
     * 客户ID
     */
    @ExcelProperty(value = "客户ID")
    private Long customerId;

    /**
     * 父节点ID（推荐人）
     */
    @ExcelProperty(value = "父节点ID", converter = ExcelDictConvert.class)
    @ExcelDictFormat(readConverterExp = "推=荐人")
    private Long parentId;

    /**
     * 外部联系人的名称
     */
    @ExcelProperty(value = "外部联系人的名称")
    private String name;

    /**
     * 外部联系人头像
     */
    @ExcelProperty(value = "外部联系人头像")
    private Long avatar;
    private String avatarUrl;

    /**
     * 外部联系人所在企业的简称，仅当联系人类型是企业微信用户时有此字段
     */
    @ExcelProperty(value = "外部联系人所在企业的简称，仅当联系人类型是企业微信用户时有此字段")
    private String corpName;

    /**
     * 外部联系人所在企业的主体名称，仅当联系人类型是企业微信用户时有此字段
     */
    @ExcelProperty(value = "外部联系人所在企业的主体名称，仅当联系人类型是企业微信用户时有此字段")
    private String corpFullName;

    /**
     * 外部联系人的职位，如果外部企业或用户选择隐藏职位，则不返回，仅当联系人类型是企业微信用户时有此字段
     */
    @ExcelProperty(value = "外部联系人的职位，如果外部企业或用户选择隐藏职位，则不返回，仅当联系人类型是企业微信用户时有此字段")
    private String positionName;

    /**
     * 外部联系人的类型，1表示该外部联系人是微信用户，2表示该外部联系人是企业微信用户；
     */
    @ExcelProperty(value = "外部联系人的类型，1表示该外部联系人是微信用户，2表示该外部联系人是企业微信用户；", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "customer_wxuser_type")
    private String type;

    /**
     * 外部联系人性别 0-未知 1-男性 2-女性
     */
    @ExcelProperty(value = "外部联系人性别 0-未知 1-男性 2-女性", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "customer_gender")
    private String gender;

    /**
     * 外部联系人在微信开放平台的唯一身份标识（微信unionid），通过此字段企业可将外部联系人与公众号/小程序用户关联起来。仅当联系人类型是微信用户，且企业或第三方服务商绑定了微信开发者ID有此字段
     */
    @ExcelProperty(value = "外部联系人在微信开放平台的唯一身份标识", converter = ExcelDictConvert.class)
    @ExcelDictFormat(readConverterExp = "微=信unionid")
    private String qwUnionid;

    /**
     * 外部联系人的userid
     */
    @ExcelProperty(value = "外部联系人的userid")
    private String qwExternalUserid;

    /**
     * 最近通话时间
     */
    @ExcelProperty(value = "最近通话时间")
    private Date laterChatTime;

    /**
     * 最近跟进时间
     */
    @ExcelProperty(value = "最近跟进时间")
    private Date laterFollowTime;

    /**
     * 录入类型：customer_entry_type
     */
    @ExcelProperty(value = "录入类型：customer_entry_type", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "customer_entry_type")
    private String entryType;

    /**
     * 维护客户总成本
     */
    @ExcelProperty(value = "维护客户总成本")
    private BigDecimal maintainCost;

    /**
     * 商机总金额
     */
    @ExcelProperty(value = "商机总金额")
// 8fd8b51b05f7fd4e6ea4a053fbe688c7
    private BigDecimal opportunityAmount;

    /**
     * 客户跟进阶段：customer_follow_stage
     */
    @ExcelProperty(value = "客户跟进阶段：customer_follow_stage", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "customer_follow_stage")
    private String followStage;

    /**
     * 客户类型ID
     */
    private Long cusTypeId;


    /**
     * 客户类型ID
     */
    @ExcelProperty(value = "客户类型")
    private String cusTypeName;

    /**
     * 跟进成员
     */
    private List<FollowUserVo> followUsers;
    /**
     * 跟进成员
     */
    private String followUserNickNames;

    /**
     * 当天商机金额
     */
    private BigDecimal todayOpportunityAmount;


    /**
     * 利润
     */
    private BigDecimal profit;
}
