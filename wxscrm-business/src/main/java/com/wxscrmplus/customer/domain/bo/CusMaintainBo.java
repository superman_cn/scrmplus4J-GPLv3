package com.wxscrmplus.customer.domain.bo;

import com.wxscrmplus.common.core.validate.AddGroup;
import com.wxscrmplus.common.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;

import java.util.Date;

import java.math.BigDecimal;

import com.wxscrmplus.common.core.domain.BaseEntity;

/**
 * 客户维护业务对象 cus_maintain
 *
 * @author 王永超
 * @date 2023-03-26
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class CusMaintainBo extends BaseEntity {

    /**
     * 维护ID
     */
    private Long maintainId;

// 8775103064b9089e044d3f5034207150
    /**
     * 成员ID
     */
    private Long userId;

    /**
     * 客户ID
     */
    @NotNull(message = "客户ID不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long customerId;

    /**
     * 备注
     */
    @NotBlank(message = "备注不能为空", groups = { AddGroup.class, EditGroup.class })
    private String remark;

    /**
     * 支出费用
     */
    @NotNull(message = "支出费用不能为空", groups = { AddGroup.class, EditGroup.class })
    private BigDecimal paymentSlip;

    /**
     * 附件
     */
    @NotBlank(message = "附件不能为空", groups = { AddGroup.class, EditGroup.class })
    private String attachments;

    /**
     * 维护方式：selling_costs_type
     */
    @NotBlank(message = "维护方式：selling_costs_type不能为空", groups = { AddGroup.class, EditGroup.class })
    private String maintainWay;

    /**
     * 维护时间
     */
    @NotNull(message = "维护时间不能为空", groups = { AddGroup.class, EditGroup.class })
    private Date maintainTime;


}
