package com.wxscrmplus.customer.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import com.wxscrmplus.common.core.domain.BaseEntity;
import lombok.Getter;

/**
 * 联系方式对象 cus_contact
 *
 * @author 王永超
 * @date 2023-03-23
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("cus_contact")
public class Contact extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 联系ID
     */
    @TableId(value = "contact_id")
    private Long contactId;
    /**
     * 客户ID
     */
// 6e11f6ae1efb1bb02b03f2b70570da89
    private Long customerId;
    /**
     * 成员ID
     */
    private Long userId;
    /**
     * 联系方式
     */
    private String type;
    /**
     * 姓名
     */
    private String name;
    /**
     * 联系方式：如手机号、邮箱等
     */
    private String way;
    /**
     * 地址
     */
    private String address;
    /**
     * 备注
     */
    private String remark;
    /**
     * 删除标志（0代表存在2代表删除）
     */
    @TableLogic
    private String delFlag;

    /**
     * type 字段枚举值
     */
    @Getter
    public enum TypeFieldEnums {
        SHOUJIHAO("手机号", "shoujihao"),
        ZUOJI("座机", "zuoji"),
        QQ("QQ", "QQ"),
        WEIXINHAO("微信号", "weixinhao"),
        YOUXIANG("邮箱", "youxiang"),
        ;

        private final String label;
        private final String value;

        TypeFieldEnums(String name, String value) {
            this.label = name;
            this.value = value;
        }

        public static String getName(String value) {
            for (TypeFieldEnums c : TypeFieldEnums.values()) {
                if (c.getValue().equals(value)) {
                    return c.label;
                }
            }
            return null;
        }
    }

}
