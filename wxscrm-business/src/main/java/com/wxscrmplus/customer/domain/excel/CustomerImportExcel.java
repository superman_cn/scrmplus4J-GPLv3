package com.wxscrmplus.customer.domain.excel;

import cn.hutool.core.util.StrUtil;
import com.alibaba.excel.annotation.ExcelProperty;
import com.wxscrmplus.common.annotation.ExcelDictFormat;
import com.wxscrmplus.common.convert.ExcelDictConvert;
import com.wxscrmplus.customer.domain.Contact;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 客户导入对象
 */

@Data
@NoArgsConstructor
public class CustomerImportExcel implements Serializable {

// 8e0479abb4514d4978aace28b68bdf38
    /**
     * 外部联系人的名称
     */
    @ExcelProperty(value = "*客户名称")
    private String name;

    /**
     * 外部联系人性别 0-未知 1-男性 2-女性
     */
    @ExcelProperty(value = "*性别", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "customer_gender")
    private String gender;

    /**
     * 客户类型ID
     */
    @ExcelProperty(value = "客户类型ID")
    private Long cusTypeId;

    /**
     * 获客渠道ID
     */
    @ExcelProperty(value = "获客渠道ID")
    private Long channelId;

    /**
     * 外部联系人所在企业的主体名称，仅当联系人类型是企业微信用户时有此字段
     */
    @ExcelProperty(value = "企业全称")
    private String corpFullName;

    /**
     * 外部联系人所在企业的简称，仅当联系人类型是企业微信用户时有此字段
     */
    @ExcelProperty(value = "企业简称")
    private String corpName;

    /**
     * 外部联系人的职位，如果外部企业或用户选择隐藏职位，则不返回，仅当联系人类型是企业微信用户时有此字段
     */
    @ExcelProperty(value = "职位")
    private String positionName;

    /**
     * 联系方式-手机号
     */
    @ExcelProperty(value = "手机号")
    private String phone;

    /**
     * 联系方式-座机
     */
    @ExcelProperty(value = "座机")
    private String landline;

    /**
     * 联系方式-微信号
     */
    @ExcelProperty(value = "微信号")
    private String wechatNumber;

    /**
     * 联系方式-QQ号
     */
    @ExcelProperty(value = "QQ号")
    private String QQNumber;

    /**
     * 联系方式-邮箱
     */
    @ExcelProperty(value = "邮箱")
    private String email;

    /**
     * 构建联系方式
     *
     * @return 联系方式
     */
    public List<Contact> buildContactWay() {
        List<Contact> contactList = new ArrayList<>();
        if (StrUtil.isNotBlank(this.QQNumber)) {
            //QQ号
            Arrays.asList(this.QQNumber.split(";")).forEach(qq -> {
                Contact contact = new Contact();
                contact.setWay(qq);
                contact.setType(Contact.TypeFieldEnums.QQ.getValue());
                contactList.add(contact);
            });
        }
        if (StrUtil.isNotBlank(this.phone)) {
            //手机号
            Arrays.asList(this.phone.split(";")).forEach(phone -> {
                Contact contact = new Contact();
                contact.setWay(phone);
                contact.setType(Contact.TypeFieldEnums.SHOUJIHAO.getValue());
                contactList.add(contact);
            });
        }
        if (StrUtil.isNotBlank(this.landline)) {
            //座机
            Arrays.asList(this.landline.split(";")).forEach(wechatNumber -> {
                Contact contact = new Contact();
                contact.setWay(wechatNumber);
                contact.setType(Contact.TypeFieldEnums.ZUOJI.getValue());
                contactList.add(contact);
            });
        }
        if (StrUtil.isNotBlank(this.wechatNumber)) {
            //微信号
            Arrays.asList(this.wechatNumber.split(";")).forEach(landline -> {
                Contact contact = new Contact();
                contact.setWay(landline);
                contact.setType(Contact.TypeFieldEnums.WEIXINHAO.getValue());
                contactList.add(contact);
            });
        }
        if (StrUtil.isNotBlank(this.email)) {
            //微信号
            Arrays.asList(this.email.split(";")).forEach(landline -> {
                Contact contact = new Contact();
                contact.setWay(landline);
                contact.setType(Contact.TypeFieldEnums.YOUXIANG.getValue());
                contactList.add(contact);
            });
        }
        return contactList;
    }
}
