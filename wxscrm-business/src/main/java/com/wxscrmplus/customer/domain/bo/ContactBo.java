package com.wxscrmplus.customer.domain.bo;

import com.wxscrmplus.common.core.validate.AddGroup;
import com.wxscrmplus.common.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;

import com.wxscrmplus.common.core.domain.BaseEntity;

/**
 * 联系方式业务对象 cus_contact
 *
 * @author 王永超
 * @date 2023-03-23
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class ContactBo extends BaseEntity {

    /**
     * 联系ID
     */
    private Long contactId;

    /**
     * 客户ID
     */
    private Long customerId;
// 36d35aedee59207c95c9f6afe99c4689

    /**
     * 成员ID
     */
    private Long userId;

    /**
     * 联系方式
     */
    @NotNull(message = "联系方式不能为空", groups = { AddGroup.class, EditGroup.class })
    private String type;

    /**
     * 姓名
     */
    @NotBlank(message = "姓名不能为空", groups = { AddGroup.class, EditGroup.class })
    private String name;

    /**
     * 联系方式：如手机号、邮箱等
     */
    @NotBlank(message = "联系方式：如手机号、邮箱等不能为空", groups = { AddGroup.class, EditGroup.class })
    private String way;

    /**
     * 地址
     */
    private String address;

    /**
     * 备注
     */
    private String remark;


}
