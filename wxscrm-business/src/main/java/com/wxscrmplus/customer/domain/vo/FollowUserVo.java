package com.wxscrmplus.customer.domain.vo;

import java.math.BigDecimal;
import java.util.Date;

import cn.hutool.core.collection.CollectionUtil;
import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.google.common.base.Joiner;
import com.wxscrmplus.common.annotation.ExcelDictFormat;
import com.wxscrmplus.common.convert.ExcelDictConvert;
import lombok.Data;

import java.util.List;


/**
 * 跟进成员视图对象 cus_follow_user
 *
 * @author 王永超
 * @date 2023-03-25
 */
@Data
@ExcelIgnoreUnannotated
public class FollowUserVo {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @ExcelProperty(value = "主键")
    private Long followId;

    /**
     * 客户ID
     */
    @ExcelProperty(value = "客户ID")
    private Long customerId;

    /**
     * 活码类型
     */
    private Integer qrcodeType;
    /**
     * 活码ID
     */
    private Long qrcodeId;
    /**
     * 成员ID
     */
    @ExcelProperty(value = "成员ID")
    private Long userId;
    /**
     * 成员
     */
    @ExcelProperty(value = "成员")
    private String userNickName;

    /**
     * 关系类型：customer_rel_del_type
     */
    @ExcelProperty(value = "关系类型：customer_rel_del_type", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "customer_follow_rel_type")
    private String relType;

    /**
     * 获客渠道ID
     */
    @ExcelProperty(value = "获客渠道ID")
    private Long channelId;

    /**
     * 获客渠道
     */
    @ExcelProperty(value = "获客渠道")
    private String channelName;

    /**
     * 下次跟进时间
     */
    @ExcelProperty(value = "下次跟进时间")
    private Date nextFollowTime;

    /**
     * 客户删除时间
     */
    @ExcelProperty(value = "客户删除时间")
    private Date customerDelTime;

    /**
     * 成员删除时间
     */
    @ExcelProperty(value = "成员删除时间")
// 07a0fdbf33097861f7b0fad0edd1aba9
    private Date userDelTime;

    /**
     * 删除类型：customer_follow_rel_type
     */
    @ExcelProperty(value = "删除类型：customer_follow_rel_type", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "customer_rel_del_type")
    private String delType;

    /**
     * 该成员对此外部联系人的描述
     */
    @ExcelProperty(value = "该成员对此外部联系人的描述")
    private String description;

    /**
     * 该成员对此外部联系人的备注
     */
    @ExcelProperty(value = "该成员对此外部联系人的备注")
    private String remark;

    /**
     * 该成员添加此外部联系人的时间
     */
    @ExcelProperty(value = "该成员添加此外部联系人的时间")
    private Date qwCreatetime;

    /**
     * 该成员对此微信客户备注的企业名称（仅微信客户有该字段）
     */
    @ExcelProperty(value = "该成员对此微信客户备注的企业名称", converter = ExcelDictConvert.class)
    @ExcelDictFormat(readConverterExp = "仅=微信客户有该字段")
    private String remarkCorpName;

    /**
     * 该成员对此客户备注的手机号码，代开发自建应用需要管理员授权才可以获取，第三方不可获取，上游企业不可获取下游企业客户该字段
     */
    @ExcelProperty(value = "该成员对此客户备注的手机号码，代开发自建应用需要管理员授权才可以获取，第三方不可获取，上游企业不可获取下游企业客户该字段")
    private List<String> remarkMobiles;

    private String remarkMobilesStr;

    public String getRemarkMobilesStr() {
        if (CollectionUtil.isNotEmpty(remarkMobiles)) {
            return Joiner.on(",").join(remarkMobiles);
        }
        return remarkMobilesStr;
    }

    /**
     * 添加方式：customer_add_way
     */
    @ExcelProperty(value = "添加方式：customer_add_way", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "customer_add_way")
    private String qwAddWay;

    /**
     * 发起添加的userid，如果成员主动添加，为成员的userid；如果是客户主动添加，则为客户的外部联系人userid；如果是内部成员共享/管理员分配，则为对应的成员/管理员userid
     */
    @ExcelProperty(value = "发起添加的userid，如果成员主动添加，为成员的userid；如果是客户主动添加，则为客户的外部联系人userid；如果是内部成员共享/管理员分配，则为对应的成员/管理员userid")
    private String qwOperUserid;

    /**
     * 客户跟进阶段：customer_follow_stage
     */
    @ExcelProperty(value = "客户跟进阶段：customer_follow_stage", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "customer_follow_stage")
    private String followStage;


    /**
     * 获客总成本
     */
    private BigDecimal winCost;


}
