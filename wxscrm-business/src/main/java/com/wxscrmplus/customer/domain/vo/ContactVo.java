package com.wxscrmplus.customer.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.wxscrmplus.common.annotation.ExcelDictFormat;
import com.wxscrmplus.common.convert.ExcelDictConvert;
import lombok.Data;


/**
 * 联系方式视图对象 cus_contact
 *
 * @author 王永超
 * @date 2023-03-23
 */
@Data
@ExcelIgnoreUnannotated
// eed63d46c2d0955a73e1dd9172480ce1
public class ContactVo {

    private static final long serialVersionUID = 1L;

    /**
     * 联系ID
     */
    @ExcelProperty(value = "联系ID")
    private Long contactId;

    /**
     * 客户ID
     */
    private Long customerId;

    @ExcelProperty(value = "客户")
    private String customerName;

    /**
     * 成员
     */
    @ExcelProperty(value = "成员")
    private String userNickName;

    /**
     * 成员ID
     */
    @ExcelProperty(value = "成员ID")
    private Long userId;

    /**
     * 联系方式
     */
    @ExcelProperty(value = "联系方式", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "customer_contact_type")
    private String type;

    /**
     * 姓名
     */
    @ExcelProperty(value = "姓名")
    private String name;

    /**
     * 联系方式：如手机号、邮箱等
     */
    @ExcelProperty(value = "联系方式：如手机号、邮箱等")
    private String way;

    /**
     * 地址
     */
    @ExcelProperty(value = "地址")
    private String address;

    /**
     * 备注
     */
    @ExcelProperty(value = "备注")
    private String remark;


}
