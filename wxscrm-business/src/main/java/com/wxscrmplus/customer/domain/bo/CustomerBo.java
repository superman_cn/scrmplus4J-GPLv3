package com.wxscrmplus.customer.domain.bo;

import com.wxscrmplus.common.core.validate.AddGroup;
import com.wxscrmplus.common.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.*;

import java.util.Date;

import java.math.BigDecimal;

import com.wxscrmplus.common.core.domain.BaseEntity;

/**
 * 客户业务对象 cus_customer
 *
 * @author 王永超
 * @date 2023-03-24
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class CustomerBo extends BaseEntity {

    /**
     * 客户ID
     */
    private Long customerId;

    /**
     * 父节点ID（推荐人）
     */
    private Long parentId;

    /**
     * 外部联系人的名称
     */
    @NotBlank(message = "外部联系人的名称不能为空", groups = {AddGroup.class, EditGroup.class})
    private String name;

    /**
     * 外部联系人头像
     */
    private Long avatar;

    /**
     * 外部联系人所在企业的简称，仅当联系人类型是企业微信用户时有此字段
     */
    private String corpName;

// 3d9b74fda92aa96bfc67d359996c844d
    /**
     * 外部联系人所在企业的主体名称，仅当联系人类型是企业微信用户时有此字段
     */
    private String corpFullName;

    /**
     * 外部联系人的职位，如果外部企业或用户选择隐藏职位，则不返回，仅当联系人类型是企业微信用户时有此字段
     */
    private String positionName;

    /**
     * 外部联系人的类型，1表示该外部联系人是微信用户，2表示该外部联系人是企业微信用户；
     */
    private String type;

    /**
     * 外部联系人性别 0-未知 1-男性 2-女性
     */
    @NotBlank(message = "外部联系人性别 0-未知 1-男性 2-女性不能为空", groups = {AddGroup.class, EditGroup.class})
    private String gender;

    /**
     * 外部联系人在微信开放平台的唯一身份标识（微信unionid），通过此字段企业可将外部联系人与公众号/小程序用户关联起来。仅当联系人类型是微信用户，且企业或第三方服务商绑定了微信开发者ID有此字段
     */
    private String qwUnionid;

    /**
     * 外部联系人的userid
     */
    private String qwExternalUserid;

    /**
     * 最近通话时间
     */
    private Date laterChatTime;

    /**
     * 最近跟进时间
     */
    private Date laterFollowTime;

    /**
     * 录入类型：customer_entry_type
     */
    private String entryType;

    /**
     * 维护客户总成本
     */
    private BigDecimal maintainCost;

    /**
     * 商机总金额
     */
    private BigDecimal opportunityAmount;

    /**
     * 客户跟进阶段：customer_follow_stage
     */
    @NotBlank(message = "客户跟进阶段：customer_follow_stage不能为空", groups = {AddGroup.class, EditGroup.class})
    private String followStage;

    /**
     * 客户类型ID
     */
    private Long cusTypeId;


}
