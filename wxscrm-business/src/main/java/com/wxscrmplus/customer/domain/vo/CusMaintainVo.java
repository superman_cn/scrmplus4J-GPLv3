package com.wxscrmplus.customer.domain.vo;
// 255d61b2e1ccd19eb1e89d165571a1ca

import java.math.BigDecimal;
import java.util.Date;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.wxscrmplus.common.annotation.ExcelDictFormat;
import com.wxscrmplus.common.convert.ExcelDictConvert;
import lombok.Data;


/**
 * 客户维护视图对象 cus_maintain
 *
 * @author 王永超
 * @date 2023-03-26
 */
@Data
@ExcelIgnoreUnannotated
public class CusMaintainVo {

    private static final long serialVersionUID = 1L;

    /**
     * 维护ID
     */
    @ExcelProperty(value = "维护ID")
    private Long maintainId;

    /**
     * 成员ID
     */
    @ExcelProperty(value = "成员ID")
    private Long userId;
    /**
     * 成员
     */
    @ExcelProperty(value = "成员")
    private String userNickName;

    /**
     * 客户ID
     */
    @ExcelProperty(value = "客户ID")
    private Long customerId;

    /**
     * 备注
     */
    @ExcelProperty(value = "备注")
    private String remark;

    /**
     * 支出费用
     */
    @ExcelProperty(value = "支出费用")
    private BigDecimal paymentSlip;

    /**
     * 附件
     */
    @ExcelProperty(value = "附件")
    private String attachments;

    /**
     * 维护方式：selling_costs_type
     */
    @ExcelProperty(value = "维护方式：selling_costs_type", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "customer_selling_costs_type")
    private String maintainWay;

    /**
     * 维护时间
     */
    @ExcelProperty(value = "维护时间")
    private Date maintainTime;


}
