package com.wxscrmplus.customer.domain.vo;

import java.math.BigDecimal;
import java.util.Date;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.wxscrmplus.common.annotation.ExcelDictFormat;
import com.wxscrmplus.common.convert.ExcelDictConvert;
import lombok.Data;


/**
 * 销售机会视图对象 cus_opportunity
 *
 * @author 王永超
 * @date 2023-03-26
 */
@Data
@ExcelIgnoreUnannotated
// 5bf00bcc2192234ec79d42280aa40154
public class OpportunityVo {

    private static final long serialVersionUID = 1L;

    /**
     * 商机ID
     */
    @ExcelProperty(value = "商机ID")
    private Long opportunityId;

    /**
     * 商机名称
     */
    @ExcelProperty(value = "商机名称")
    private String name;

    /**
     * 客户ID
     */
    @ExcelProperty(value = "客户ID")
    private Long customerId;

    @ExcelProperty(value = "客户")
    private String customerName;
    /**
     * 负责人ID（销售员）
     */
    @ExcelProperty(value = "负责人ID")
    private Long saleId;

    /**
     * 负责人（销售员）
     */
    @ExcelProperty(value = "负责人")
    private String saleName;

    /**
     * 销售阶段：customer_sale_stage
     */
    @ExcelProperty(value = "销售阶段：customer_sale_stage", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "customer_sale_stage")
    private String saleStage;

    /**
     * 预计金额
     */
    @ExcelProperty(value = "预计金额")
    private BigDecimal needAmount;

    /**
     * 预计成交时间
     */
    @ExcelProperty(value = "预计成交时间")
    private Date predictMakeTime;

    /**
     * 实付金额
     */
    @ExcelProperty(value = "实付金额")
    private BigDecimal payAmount;

    /**
     * 实际成交时间
     */
    @ExcelProperty(value = "实际成交时间")
    private Date makeTime;


}
