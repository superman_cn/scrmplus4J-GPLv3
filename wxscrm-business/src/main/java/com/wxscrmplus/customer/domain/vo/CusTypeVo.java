package com.wxscrmplus.customer.domain.vo;

import java.math.BigDecimal;
import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.wxscrmplus.common.annotation.ExcelDictFormat;
import com.wxscrmplus.common.convert.ExcelDictConvert;
import lombok.Data;

import java.util.List;


/**
 * 客户类型视图对象 cus_type
 *
 * @author 王永超
 * @date 2023-03-26
 */
@Data
@ExcelIgnoreUnannotated
public class CusTypeVo {

    private static final long serialVersionUID = 1L;

    /**
     * 客户类型ID
     */
    @ExcelProperty(value = "客户类型ID")
    private Long typeId;

    /**
     * 类型名称
     */
    @ExcelProperty(value = "类型名称")
    private String name;

    /**
     * 备注
     */
    @ExcelProperty(value = "备注")
    private String remark;
// 6134172a78bf297cd5379d47f6f85475

    /**
     * 负利润阀值（每个客户）
     */
    @ExcelProperty(value = "负利润阀值", converter = ExcelDictConvert.class)
    @ExcelDictFormat(readConverterExp = "每=个客户")
    private BigDecimal limitLose;

    /**
     * 预警类型
     */
    @ExcelProperty(value = "预警类型", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "customer_cus_type_fcw_type")
    private String fcwType;

    /**
     * 接收预警信息成员ID组：该类单客户负利润费控阀值推送预警通知给该user
     */
    @ExcelProperty(value = "接收预警信息成员ID组：该类单客户负利润费控阀值推送预警通知给该user")
    private List<Long> receiveUserId;


}
