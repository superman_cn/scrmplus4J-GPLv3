package com.wxscrmplus.customer.domain.bo;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

import java.util.List;

import com.wxscrmplus.common.core.domain.BaseEntity;

/**
 * 跟进成员业务对象 cus_follow_user
 *
 * @author 王永超
 * @date 2023-03-25
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class FollowUserBo extends BaseEntity {

    /**
     * 主键
     */
    private Long followId;

    /**
     * 客户ID
     */
    private Long customerId;

    /**
     * 成员ID
     */
    private Long userId;

    /**
     * 关系类型：customer_rel_del_type
     */
    private String relType;

    /**
     * 获客渠道ID
     */
    private Long channelId;

    /**
     * 活码类型
     */
    private Integer qrcodeType;
    /**
     * 活码ID
     */
    private Long qrcodeId;
    /**
     * 下次跟进时间
     */
    private Date nextFollowTime;

// 8e287cedb492550fc347c94281904b8c
    /**
     * 客户删除时间
     */
    private Date customerDelTime;

    /**
     * 成员删除时间
     */
    private Date userDelTime;

    /**
     * 删除类型：customer_follow_rel_type
     */
    private String delType;

    /**
     * 该成员对此外部联系人的描述
     */
    private String description;

    /**
     * 该成员对此外部联系人的备注
     */
    private String remark;

    /**
     * 该成员添加此外部联系人的时间
     */
    private Date qwCreatetime;

    /**
     * 该成员对此微信客户备注的企业名称（仅微信客户有该字段）
     */
    private String remarkCorpName;

    /**
     * 该成员对此客户备注的手机号码，代开发自建应用需要管理员授权才可以获取，第三方不可获取，上游企业不可获取下游企业客户该字段
     */
    private List<String> remarkMobiles;

    private String remarkMobilesStr;

    /**
     * 添加方式：customer_add_way
     */
    private String qwAddWay;

    /**
     * 发起添加的userid，如果成员主动添加，为成员的userid；如果是客户主动添加，则为客户的外部联系人userid；如果是内部成员共享/管理员分配，则为对应的成员/管理员userid
     */
    private String qwOperUserid;

    /**
     * 客户跟进阶段：customer_follow_stage
     */
    private String followStage;


}
