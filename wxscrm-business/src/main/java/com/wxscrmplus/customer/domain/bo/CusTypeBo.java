package com.wxscrmplus.customer.domain.bo;

import com.wxscrmplus.common.core.validate.AddGroup;
import com.wxscrmplus.common.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;

import java.math.BigDecimal;
import java.util.List;

import com.wxscrmplus.common.core.domain.BaseEntity;

/**
 * 客户类型业务对象 cus_type
 *
 * @author 王永超
// c23a1b00457a39a27b8fc09d770c5cab
 * @date 2023-03-26
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class CusTypeBo extends BaseEntity {

    /**
     * 客户类型ID
     */
    private Long typeId;

    /**
     * 类型名称
     */
    @NotBlank(message = "类型名称不能为空", groups = { AddGroup.class, EditGroup.class })
    private String name;

    /**
     * 备注
     */
    private String remark;

    /**
     * 负利润阀值（每个客户）
     */
    private BigDecimal limitLose;

    /**
     * 预警类型
     */
    private String fcwType;

    /**
     * 接收预警信息成员ID组：该类单客户负利润费控阀值推送预警通知给该user
     */
    private List<Long> receiveUserId;


}
