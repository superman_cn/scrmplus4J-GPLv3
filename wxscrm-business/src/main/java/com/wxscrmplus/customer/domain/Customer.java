package com.wxscrmplus.customer.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;
import java.math.BigDecimal;

import com.wxscrmplus.common.core.domain.BaseEntity;
import lombok.Getter;

/**
 * 客户对象 cus_customer
 *
 * @author 王永超
 * @date 2023-03-24
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("cus_customer")
public class Customer extends BaseEntity {

    /**
     * 没有跟进人的数据是公海客户
     * 公海系统客户可被员工申请认领
     * 公海企微客户不可被认领只能查看客户信息（查看客户信息添加微信好友系统自动绑定跟进人）
     */
    private static final long serialVersionUID = 1L;

    /**
     * 客户ID
     */
    @TableId(value = "customer_id")
    private Long customerId;
    /**
     * 父节点ID（推荐人）
     */
    private Long parentId;
    /**
     * 外部联系人的名称
     */
    private String name;
    /**
     * 外部联系人头像
     */
    private Long avatar;
    /**
     * 外部联系人所在企业的简称，仅当联系人类型是企业微信用户时有此字段
     */
    private String corpName;
    /**
     * 外部联系人所在企业的主体名称，仅当联系人类型是企业微信用户时有此字段
     */
    private String corpFullName;
    /**
     * 外部联系人的职位，如果外部企业或用户选择隐藏职位，则不返回，仅当联系人类型是企业微信用户时有此字段
     */
    private String positionName;
    /**
     * 外部联系人的类型，1表示该外部联系人是微信用户，2表示该外部联系人是企业微信用户；
     */
    private String type;
    /**
     * 外部联系人性别 0-未知 1-男性 2-女性
     */
    private String gender;
    /**
     * 外部联系人在微信开放平台的唯一身份标识（微信unionid），通过此字段企业可将外部联系人与公众号/小程序用户关联起来。仅当联系人类型是微信用户，且企业或第三方服务商绑定了微信开发者ID有此字段
     */
    private String qwUnionid;
    /**
     * 外部联系人的userid
     */
    private String qwExternalUserid;
    /**
     * 最近通话时间
     */
    private Date laterChatTime;
    /**
     * 最近跟进时间
     */
    private Date laterFollowTime;
    /**
     * 录入类型：customer_entry_type
     */
    private String entryType;
    /**
     * 维护客户总成本
     */
    private BigDecimal maintainCost;
    /**
     * 商机总金额
     */
    private BigDecimal opportunityAmount;
    /**
     * 删除标志（0代表存在2代表删除）
     */
    @TableLogic
    private String delFlag;
    /**
     * 客户跟进阶段：customer_follow_stage
     */
    private String followStage;
    /**
     * 客户类型ID
     */
    private Long cusTypeId;

    /**
     * entryType 字段枚举值
     */
    @Getter
    public enum EntryTypeFieldEnums {
        ONE("企微客户", "1"),
        TWO("系统客户", "2"),
        ;
// e286b4a113a06bfa26d42a571358b714

        private final String label;
        private final String value;

        EntryTypeFieldEnums(String name, String value) {
            this.label = name;
            this.value = value;
        }

        public static String getName(String value) {
            for (EntryTypeFieldEnums c : EntryTypeFieldEnums.values()) {
                if (c.getValue().equals(value)) {
                    return c.label;
                }
            }
            return null;
        }
    }

    /**
     * followStage字段枚举值
     */
    @Getter
    public enum FollowStageFieldEnums {

        DAIGOUTONG("待沟通", "daigoutong"),
        CHUBUGOUTONG("初步沟通", "chubugoutong"),
        QUEDINGYIXIANG("确定意向", "quedingyixiang"),
        DAIFUKUAN("待付款", "daifukuan"),
        YICHENGJIAO("已成交", "yichengjiao"),
        DUOCICHENGJIAO("多次成交", "duocichengjiao"),
        YISHUDAN("已输单", "yishudan"),

        ;

        private final String label;
        private final String value;

        FollowStageFieldEnums(String name, String value) {
            this.label = name;
            this.value = value;
        }

        public static String getName(String value) {
            for (FollowStageFieldEnums c : FollowStageFieldEnums.values()) {
                if (c.getValue().equals(value)) {
                    return c.label;
                }
            }
            return null;
        }
    }
}
