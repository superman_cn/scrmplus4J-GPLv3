package com.wxscrmplus.customer.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
// 5d9643ff301ee808bf505f27d803f197

import com.wxscrmplus.common.core.domain.BaseEntity;

/**
 * 客户日志对象 cus_transfer_log
 *
 * @author 王永超
 * @date 2023-04-23
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("cus_customer_log")
public class CustomerLog extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * 客户日志ID
     */
    @TableId(value = "customer_log_id")
    private Long customerLogId;
    /**
     * 客户ID
     */
    private Long customerId;
    /**
     * 摘要
     */
    private String abstractText;
    /**
     * 删除标志（0代表存在2代表删除）
     */
    @TableLogic
    private String delFlag;

}
