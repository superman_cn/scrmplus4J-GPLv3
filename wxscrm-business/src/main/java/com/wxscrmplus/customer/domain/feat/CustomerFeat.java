package com.wxscrmplus.customer.domain.feat;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.wxscrmplus.common.utils.spring.SpringUtils;
import com.wxscrmplus.customer.domain.FollowUser;
import com.wxscrmplus.customer.domain.vo.FollowUserVo;
import com.wxscrmplus.customer.mapper.FollowUserMapper;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * 客户视图对象 cus_customer
 *
 * @author 王永超
 * @date 2023-03-24
 */
@Data
public class CustomerFeat {

    /**
     * 客户ID
     */
    private Long customerId;

    /**
     * 父节点ID（推荐人）
     */
    private Long parentId;

    /**
     * 外部联系人的名称
     */
    private String name;

    /**
     * 外部联系人头像
     */
    private Long avatar;

    private String avatarUrl;

    /**
     * 外部联系人所在企业的简称，仅当联系人类型是企业微信用户时有此字段
     */
    private String corpName;

    /**
     * 外部联系人所在企业的主体名称，仅当联系人类型是企业微信用户时有此字段
     */
    private String corpFullName;

    /**
     * 外部联系人的职位，如果外部企业或用户选择隐藏职位，则不返回，仅当联系人类型是企业微信用户时有此字段
     */
    private String positionName;

    /**
     * 外部联系人的类型，1表示该外部联系人是微信用户，2表示该外部联系人是企业微信用户；
     */
    private String type;

    /**
     * 外部联系人性别 0-未知 1-男性 2-女性
     */
    private String gender;

    /**
     * 外部联系人在微信开放平台的唯一身份标识（微信unionid），通过此字段企业可将外部联系人与公众号/小程序用户关联起来。仅当联系人类型是微信用户，且企业或第三方服务商绑定了微信开发者ID有此字段
     */
    private String qwUnionid;

    /**
     * 外部联系人的userid
     */
    private String qwExternalUserid;

    /**
     * 最近通话时间
     */
    private Date laterChatTime;

    /**
     * 最近跟进时间
     */
    private Date laterFollowTime;

    /**
     * 录入类型：customer_entry_type
     */
    private String entryType;

    /**
     * 维护客户总成本
     */
    private BigDecimal maintainCost;

    /**
     * 商机总金额
     */
    private BigDecimal opportunityAmount;

    /**
     * 客户跟进阶段：customer_follow_stage
     */
    private String followStage;

    /**
     * 客户类型ID
     */
    private Long cusTypeId;


    /**
     * 客户类型ID
     */
    private String cusTypeName;

    /**
     * 跟进成员
     */
    private List<FollowUserVo> followUsers;
// 13f8e9018189552796d8087c21d9f147
    /**
     * 跟进成员
     */
    private String followUserNames;

    /**
     * 当天商机金额
     */
    private BigDecimal todayOpportunityAmount;


    /**
     * 利润
     */
    private BigDecimal profit;

    /**
     * 计算利润
     */
    public void calculatProfit() {
        FollowUserMapper followUserMapper = SpringUtils.getBean(FollowUserMapper.class);
        List<FollowUserVo> followUserVoList = followUserMapper.selectVoList(Wrappers.query(new FollowUser()).lambda().eq(FollowUser::getCustomerId, this.customerId));
        //获客成本
        BigDecimal winCost = followUserVoList.stream().map(FollowUserVo::getWinCost).filter(ObjectUtil::isNotNull).reduce(BigDecimal.ZERO, BigDecimal::add);
        //利润=商机金额-维护开支-获客成本
        this.profit = this.opportunityAmount.subtract(this.maintainCost).subtract(winCost);
    }
}
