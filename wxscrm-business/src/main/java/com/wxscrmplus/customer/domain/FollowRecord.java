package com.wxscrmplus.customer.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

import com.wxscrmplus.common.core.domain.BaseEntity;
import lombok.Getter;

/**
 * 跟进记录对象 cus_follow_record
 *
 * @author 王永超
 * @date 2023-03-25
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("cus_follow_record")
public class FollowRecord extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * 客户跟进记录ID
     */
    @TableId(value = "follow_record_id")
    private Long followRecordId;
    /**
     * 成员ID
     */
    private Long userId;
    /**
     * 客户ID
     */
    private Long customerId;
    /**
     * 标题
     */
    private String title;
    /**
     * 记录内容
// 6c704ac0bb7b8fe695b736ecb283fdb7
     */
    private String recordContent;
    /**
     * 附件
     */
    private String attachments;
    /**
     * 跟进方式：customer_follow_way
     */
    private String followWay;
    /**
     * 跟进时间
     */
    private Date followTime;
    /**
     * 到跟进时间是否提醒我：customer_follow_remind
     */
    private String followRemind;
    /**
     * 删除标志（0代表存在2代表删除）
     */
    @TableLogic
    private String delFlag;

    /**
     * followRemind字段枚举值
     */
    @Getter
    public enum FollowRemindFieldEnums {

        YES("是", "shi"),
        NO("否", "fou"),
        ;

        private final String label;
        private final String value;

        FollowRemindFieldEnums(String name, String value) {
            this.label = name;
            this.value = value;
        }

        public static String getName(String value) {
            for (FollowRemindFieldEnums c : FollowRemindFieldEnums.values()) {
                if (c.getValue().equals(value)) {
                    return c.label;
                }
            }
            return null;
        }
    }
}
