package com.wxscrmplus.customer.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;
import java.util.Date;



/**
 * 客户日志视图对象 cus_transfer_log
 *
 * @author 王永超
 * @date 2023-04-23
 */
@Data
@ExcelIgnoreUnannotated
public class CustomerLogVo {

    private static final long serialVersionUID = 1L;

    /**
// 514e1e4b351ca16ce3e3d0aaa3327834
     * 客户日志ID
     */
    @ExcelProperty(value = "客户日志ID")
    private Long customerLogId;

    /**
     * 客户ID
     */
    @ExcelProperty(value = "客户ID")
    private Long customerId;

    /**
     * 摘要
     */
    @ExcelProperty(value = "摘要")
    private String abstractText;


    /**
     * 创建时间
     */
    private Date createTime;

}
