package com.wxscrmplus.customer.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;
import java.math.BigDecimal;

import com.wxscrmplus.common.core.domain.BaseEntity;
import lombok.Getter;

/**
 * 销售机会对象 cus_opportunity
 *
 * @author 王永超
 * @date 2023-03-26
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("cus_opportunity")
public class Opportunity extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * 商机ID
     */
    @TableId(value = "opportunity_id")
    private Long opportunityId;
    /**
     * 商机名称
     */
    private String name;
    /**
     * 客户ID
     */
    private Long customerId;
    /**
     * 负责人ID（销售员）
     */
    private Long saleId;
    /**
     * 销售阶段：customer_sale_stage
     */
    private String saleStage;
    /**
     * 预计金额
     */
    private BigDecimal needAmount;
    /**
     * 预计成交时间
     */
    private Date predictMakeTime;
    /**
     * 实付金额
     */
    private BigDecimal payAmount;
    /**
     * 实际成交时间
     */
    private Date makeTime;
    /**
     * 删除标志（0代表存在2代表删除）
     */
    @TableLogic
    private String delFlag;

    /**
     * saleStage字段枚举值
     */
    @Getter
    public enum SaleStageFieldEnums {
        YIXIANGMINGQUE("意向明确", "yixiangmingque"),
        BAOJIAJIEDUAN("报价阶段", "baojiajieduan"),
        HETONGJIEDUAN("合同阶段", "hetongjieduan"),
        FUKUANJIEDUAN("付款阶段", "fukuanjieduan"),
        YICHENGDAN("已成单", "yichengdan"),
        YIDIUDAN("已丢单", "yidiudan"),
        ;

        private final String label;
        private final String value;

        SaleStageFieldEnums(String name, String value) {
            this.label = name;
            this.value = value;
        }

// 46d1a2d44a35fa5ed944b54181029f4e
        public static String getName(String value) {
            for (SaleStageFieldEnums c : SaleStageFieldEnums.values()) {
                if (c.getValue().equals(value)) {
                    return c.label;
                }
            }
            return null;
        }
    }
}
