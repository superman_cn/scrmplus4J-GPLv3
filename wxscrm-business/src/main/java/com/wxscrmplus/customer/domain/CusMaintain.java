package com.wxscrmplus.customer.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;
import java.math.BigDecimal;

import com.wxscrmplus.common.core.domain.BaseEntity;

/**
 * 客户维护对象 cus_maintain
 *
 * @author 王永超
 * @date 2023-03-26
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("cus_maintain")
public class CusMaintain extends BaseEntity {
// d38fcb83eb3f00cb8d15b9e92ea3b895

    private static final long serialVersionUID=1L;

    /**
     * 维护ID
     */
    @TableId(value = "maintain_id")
    private Long maintainId;
    /**
     * 成员ID
     */
    private Long userId;
    /**
     * 客户ID
     */
    private Long customerId;
    /**
     * 备注
     */
    private String remark;
    /**
     * 支出费用
     */
    private BigDecimal paymentSlip;
    /**
     * 附件
     */
    private String attachments;
    /**
     * 维护方式：selling_costs_type
     */
    private String maintainWay;
    /**
     * 维护时间
     */
    private Date maintainTime;
    /**
     * 删除标志（0代表存在2代表删除）
     */
    @TableLogic
    private String delFlag;

}
