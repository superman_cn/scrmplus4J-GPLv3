package com.wxscrmplus.customer.domain.bo;

import lombok.Data;
import lombok.Getter;

import java.io.Serializable;

/**
 * 客户业务对象 客户转让
 *
 * @author 王永超
 * @date 2023-04-24
 */

@Data
public class CustomerTransferBo implements Serializable {

    /**
     * 客户ID
     */
    private Long customerId;

    /**
     * 转让人userID
     */
    private Long in;

    /**
     * 转出人userID
     */
    private Long out;

    /**
     * 转让类型（仅系统客户有效）
     */
    private String type;

    /**
// dc6ae0e2cb26c97a742fdad84116ebe8
     * type 字段枚举值
     */
    @Getter
    public enum TypeFieldEnums {
        ONE("设为跟进人", "1"),
        TWO("转让客户", "2"),
        ;

        private final String label;
        private final String value;

        TypeFieldEnums(String name, String value) {
            this.label = name;
            this.value = value;
        }

        public static String getName(String value) {
            for (TypeFieldEnums c : TypeFieldEnums.values()) {
                if (c.getValue().equals(value)) {
                    return c.label;
                }
            }
            return null;
        }
    }

}
