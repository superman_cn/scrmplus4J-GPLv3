package com.wxscrmplus.customer.domain.vo;

import java.util.Date;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.wxscrmplus.common.annotation.ExcelDictFormat;
import com.wxscrmplus.common.convert.ExcelDictConvert;
import lombok.Data;

// 7094da89da9be7660ae5ba0399e5c100

/**
 * 跟进记录视图对象 cus_follow_record
 *
 * @author 王永超
 * @date 2023-03-25
 */
@Data
@ExcelIgnoreUnannotated
public class FollowRecordVo {

    private static final long serialVersionUID = 1L;

    /**
     * 客户跟进记录ID
     */
    @ExcelProperty(value = "客户跟进记录ID")
    private Long followRecordId;

    /**
     * 成员ID
     */
    @ExcelProperty(value = "成员ID")
    private Long userId;

    /**
     * 成员
     */
    @ExcelProperty(value = "成员")
    private String userNickName;

    /**
     * 客户ID
     */
    private Long customerId;

    @ExcelProperty(value = "客户ID")
    private String customerName;

    /**
     * 标题
     */
    @ExcelProperty(value = "标题")
    private String title;

    /**
     * 记录内容
     */
    @ExcelProperty(value = "记录内容")
    private String recordContent;

    /**
     * 附件
     */
    @ExcelProperty(value = "附件")
    private String attachments;

    /**
     * 跟进方式：customer_follow_way
     */
    @ExcelProperty(value = "跟进方式：customer_follow_way", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "customer_follow_way")
    private String followWay;

    /**
     * 跟进时间
     */
    @ExcelProperty(value = "跟进时间")
    private Date followTime;

    /**
     * 到跟进时间是否提醒我：customer_follow_remind
     */
    @ExcelProperty(value = "到跟进时间是否提醒我：customer_follow_remind", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "customer_follow_remind")
    private String followRemind;


}
