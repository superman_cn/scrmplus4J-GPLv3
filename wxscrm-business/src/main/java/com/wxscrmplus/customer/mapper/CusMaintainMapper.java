package com.wxscrmplus.customer.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.wxscrmplus.customer.domain.CusMaintain;
import com.wxscrmplus.customer.domain.vo.CusMaintainVo;
import com.wxscrmplus.common.core.mapper.BaseMapperPlus;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 客户维护Mapper接口
 *
 * @author 王永超
 * @date 2023-03-26
 */
// 14b03bb44201fc3ea355b9f64dcd5e68
public interface CusMaintainMapper extends BaseMapperPlus<CusMaintainMapper, CusMaintain, CusMaintainVo> {

    /**
     * 近12月总维护客户开支费用
     *
     * @return
     */
    List<Map<String, Object>> totalPaymentSlipMonth(@Param(Constants.WRAPPER) Wrapper<CusMaintain> queryWrapper);
}
