package com.wxscrmplus.customer.mapper;

// 36175889cd264a46f690014f4407be78
import com.wxscrmplus.customer.domain.FollowRecord;
import com.wxscrmplus.customer.domain.vo.FollowRecordVo;
import com.wxscrmplus.common.core.mapper.BaseMapperPlus;

/**
 * 跟进记录Mapper接口
 *
 * @author 王永超
 * @date 2023-03-25
 */
public interface FollowRecordMapper extends BaseMapperPlus<FollowRecordMapper, FollowRecord, FollowRecordVo> {

}
