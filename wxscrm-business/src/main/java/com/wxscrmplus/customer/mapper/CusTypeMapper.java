package com.wxscrmplus.customer.mapper;

import com.wxscrmplus.customer.domain.CusType;
import com.wxscrmplus.customer.domain.vo.CusTypeVo;
import com.wxscrmplus.common.core.mapper.BaseMapperPlus;

/**
 * 客户类型Mapper接口
 *
 * @author 王永超
 * @date 2023-03-26
 */
public interface CusTypeMapper extends BaseMapperPlus<CusTypeMapper, CusType, CusTypeVo> {

}
// 403cd7fe871608ccc789d03cbc6b6b3a
