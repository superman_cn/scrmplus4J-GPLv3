package com.wxscrmplus.customer.mapper;

// af9c39ca33e0dcd171a4b9e00963d9b2
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.wxscrmplus.customer.domain.FollowUser;
import com.wxscrmplus.customer.domain.vo.FollowUserVo;
import com.wxscrmplus.common.core.mapper.BaseMapperPlus;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 跟进成员Mapper接口
 *
 * @author 王永超
 * @date 2023-03-25
 */
public interface FollowUserMapper extends BaseMapperPlus<FollowUserMapper, FollowUser, FollowUserVo> {
    /**
     * 近12月获客成本统计分析
     *
     * @return
     */
    List<Map<String, Object>> totalWinCostLast12Month(@Param(Constants.WRAPPER) Wrapper<FollowUser> queryWrapper);

    /**
     * 渠道客户总数
     *
     * @return
     */
    List<Map<String, Object>> channelCustomerCount(@Param("userIds") String userIds);

    /**
     * 客户类型客户总数
     *
     * @return
     */
    List<Map<String, Object>> cusTypeCustomerCount(@Param("userIds") String userIds);

}
