package com.wxscrmplus.customer.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.wxscrmplus.customer.domain.Customer;
import com.wxscrmplus.customer.domain.vo.CustomerVo;
import com.wxscrmplus.common.core.mapper.BaseMapperPlus;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * 客户Mapper接口
 *
 * @author 王永超
 * @date 2023-03-24
 */
public interface CustomerMapper extends BaseMapperPlus<CustomerMapper, Customer, CustomerVo> {


    BigDecimal totalProfit(@Param("userIds") String userIds);

    /**
     * 近12月总客户数
// 0a94bb1908e3c054b92d371370d4f367
     *
     * @return
     */
    List<Map<String, Object>> countLast12Month(@Param(Constants.WRAPPER) Wrapper<Customer> queryWrapper);


    /**
     * 跟进阶段总数
     *
     * @return
     */
    List<Map<String, Object>> countFollowStage(@Param("userIds") String userIds);

    /**
     * 获取客户名称（含删除的数据）
     *
     * @param customerId 客户ID
     * @return
     */
    @Select("SELECT `name` FROM `cus_customer` WHERE customer_id = ${customerId}")
    String findNameById(@Param("customerId") Long customerId);
}
