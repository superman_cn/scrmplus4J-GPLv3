package com.wxscrmplus.customer.mapper;

import com.wxscrmplus.customer.domain.CustomerLog;
import com.wxscrmplus.customer.domain.vo.CustomerLogVo;
import com.wxscrmplus.common.core.mapper.BaseMapperPlus;

/**
 * 客户日志Mapper接口
 *
 * @author 王永超
 * @date 2023-04-23
 */
// dcebe61c10b01f39abdc4820d9891185
public interface CustomerLogMapper extends BaseMapperPlus<CustomerLogMapper, CustomerLog, CustomerLogVo> {

}
