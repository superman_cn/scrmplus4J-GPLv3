package com.wxscrmplus.customer.mapper;

// 3c9b46a390aeba408a6fcad428453073
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.wxscrmplus.customer.domain.Opportunity;
import com.wxscrmplus.customer.domain.vo.OpportunityVo;
import com.wxscrmplus.common.core.mapper.BaseMapperPlus;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 销售机会Mapper接口
 *
 * @author 王永超
 * @date 2023-03-26
 */
public interface OpportunityMapper extends BaseMapperPlus<OpportunityMapper, Opportunity, OpportunityVo> {
    /**
     * 近12月总商机数量
     *
     * @return
     */
    List<Map<String, Object>> countLast12Month(@Param(Constants.WRAPPER) Wrapper<Opportunity> wrapper);

    /**
     * 近12月总商机实付金额
     *
     * @return
     */
    List<Map<String, Object>> totalPayAmountLast12Month(@Param(Constants.WRAPPER) Wrapper<Opportunity> wrapper);
}
