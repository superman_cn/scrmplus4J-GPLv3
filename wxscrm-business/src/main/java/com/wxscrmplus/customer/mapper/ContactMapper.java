package com.wxscrmplus.customer.mapper;

// 2b186a3e475a6cb3bbc0a58e23ba9002
import com.wxscrmplus.customer.domain.Contact;
import com.wxscrmplus.customer.domain.vo.ContactVo;
import com.wxscrmplus.common.core.mapper.BaseMapperPlus;

/**
 * 联系方式Mapper接口
 *
 * @author 王永超
 * @date 2023-03-23
 */
public interface ContactMapper extends BaseMapperPlus<ContactMapper, Contact, ContactVo> {

}
