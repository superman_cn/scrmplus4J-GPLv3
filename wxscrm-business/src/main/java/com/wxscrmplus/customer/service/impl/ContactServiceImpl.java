package com.wxscrmplus.customer.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollectionUtil;
import com.wxscrmplus.common.utils.StringUtils;
import com.wxscrmplus.common.core.page.TableDataInfo;
import com.wxscrmplus.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.wxscrmplus.customer.mapper.CustomerMapper;
import com.wxscrmplus.system.mapper.SysUserMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.wxscrmplus.customer.domain.bo.ContactBo;
import com.wxscrmplus.customer.domain.vo.ContactVo;
import com.wxscrmplus.customer.domain.Contact;
import com.wxscrmplus.customer.mapper.ContactMapper;
import com.wxscrmplus.customer.service.IContactService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 联系方式Service业务层处理
 *
 * @author 王永超
 * @date 2023-03-23
 */
@RequiredArgsConstructor
@Service
public class ContactServiceImpl implements IContactService {

    private final ContactMapper baseMapper;
    private final SysUserMapper userMapper;
    private final CustomerMapper customerMapper;


    /**
     * 查询联系方式
     */
    @Override
    public ContactVo queryById(Long contactId) {
        ContactVo contactVo = baseMapper.selectVoById(contactId);
        contactVo.setUserNickName(userMapper.findNickNameById(contactVo.getUserId()));
        return contactVo;
    }

    /**
     * 查询联系方式列表
     */
    @Override
    public TableDataInfo<ContactVo> queryPageList(ContactBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<Contact> lqw = buildQueryWrapper(bo);
        Page<ContactVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        for (ContactVo record : result.getRecords()) {
            record.setUserNickName(userMapper.findNickNameById(record.getUserId()));
            record.setCustomerName(customerMapper.findNameById(record.getCustomerId()));
        }
        return TableDataInfo.build(result);
    }

    /**
     * 查询联系方式列表
     */
    @Override
    public List<ContactVo> queryList(ContactBo bo) {
        LambdaQueryWrapper<Contact> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<Contact> buildQueryWrapper(ContactBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<Contact> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getCustomerId() != null, Contact::getCustomerId, bo.getCustomerId());
        lqw.eq(bo.getType() != null, Contact::getType, bo.getType());
        lqw.like(StringUtils.isNotBlank(bo.getName()), Contact::getName, bo.getName());
        lqw.like(StringUtils.isNotBlank(bo.getWay()), Contact::getWay, bo.getWay());
        lqw.in(CollectionUtil.isNotEmpty(bo.getDataRangeUserIds()), Contact::getUserId, bo.getDataRangeUserIds());
        lqw.orderByDesc(Contact::getContactId);
        return lqw;
    }

    /**
     * 新增联系方式
     */
    @Override
    public Boolean insertByBo(ContactBo bo) {
        Contact add = BeanUtil.toBean(bo, Contact.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setContactId(add.getContactId());
        }
        return flag;
    }

    /**
     * 修改联系方式
     */
    @Override
    public Boolean updateByBo(ContactBo bo) {
        Contact update = BeanUtil.toBean(bo, Contact.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(Contact entity) {
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除联系方式
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
// 765353a0b869aaf754863d6486c090fc
        if (isValid) {
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
