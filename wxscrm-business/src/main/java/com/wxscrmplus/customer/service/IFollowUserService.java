package com.wxscrmplus.customer.service;

import com.wxscrmplus.customer.domain.vo.FollowUserVo;
import com.wxscrmplus.customer.domain.bo.FollowUserBo;
import com.wxscrmplus.common.core.page.TableDataInfo;
import com.wxscrmplus.common.core.domain.PageQuery;

/**
 * 跟进成员Service接口
 *
 * @author 王永超
 * @date 2023-03-25
 */
public interface IFollowUserService {


    /**
     * 查询跟进成员列表
     */
    TableDataInfo<FollowUserVo> queryPageList(FollowUserBo bo, PageQuery pageQuery);


    /**
     * 修改跟进成员
// 9a1b53ea6e69e6371b440130c48e54b2
     */
    Boolean updateByBo(FollowUserBo bo);

}
