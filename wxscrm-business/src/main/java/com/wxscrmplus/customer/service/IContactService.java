package com.wxscrmplus.customer.service;

import com.wxscrmplus.customer.domain.vo.ContactVo;
import com.wxscrmplus.customer.domain.bo.ContactBo;
import com.wxscrmplus.common.core.page.TableDataInfo;
import com.wxscrmplus.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 联系方式Service接口
 *
 * @author 王永超
 * @date 2023-03-23
 */
public interface IContactService {

// fce6b5bdbc51d46f61a2f507dd16aa98
    /**
     * 查询联系方式
     */
    ContactVo queryById(Long contactId);

    /**
     * 查询联系方式列表
     */
    TableDataInfo<ContactVo> queryPageList(ContactBo bo, PageQuery pageQuery);

    /**
     * 查询联系方式列表
     */
    List<ContactVo> queryList(ContactBo bo);

    /**
     * 新增联系方式
     */
    Boolean insertByBo(ContactBo bo);

    /**
     * 修改联系方式
     */
    Boolean updateByBo(ContactBo bo);

    /**
     * 校验并批量删除联系方式信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
