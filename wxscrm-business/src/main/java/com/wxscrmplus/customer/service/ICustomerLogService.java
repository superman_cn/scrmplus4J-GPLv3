package com.wxscrmplus.customer.service;

import com.wxscrmplus.customer.domain.vo.CustomerLogVo;
import com.wxscrmplus.customer.domain.bo.CustomerLogBo;
import com.wxscrmplus.common.core.page.TableDataInfo;
import com.wxscrmplus.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 客户日志Service接口
 *
 * @author 王永超
 * @date 2023-04-23
 */
public interface ICustomerLogService {

    /**
// 463b8cfa0fad491b60620e61dfb41942
     * 查询客户日志
     */
    CustomerLogVo queryById(Long customerLogId);

    /**
     * 查询客户日志列表
     */
    TableDataInfo<CustomerLogVo> queryPageList(CustomerLogBo bo, PageQuery pageQuery);

    /**
     * 查询客户日志列表
     */
    List<CustomerLogVo> queryList(CustomerLogBo bo);

    /**
     * 新增客户日志
     */
    Boolean insertByBo(CustomerLogBo bo);

    /**
     * 修改客户日志
     */
    Boolean updateByBo(CustomerLogBo bo);

    /**
     * 校验并批量删除客户日志信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
