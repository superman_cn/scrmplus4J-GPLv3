package com.wxscrmplus.customer.service;

import com.wxscrmplus.customer.domain.vo.OpportunityVo;
import com.wxscrmplus.customer.domain.bo.OpportunityBo;
import com.wxscrmplus.common.core.page.TableDataInfo;
import com.wxscrmplus.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 销售机会Service接口
 *
 * @author 王永超
 * @date 2023-03-26
 */
public interface IOpportunityService {

    /**
     * 查询销售机会
     */
    OpportunityVo queryById(Long opportunityId);

    /**
     * 查询销售机会列表
     */
    TableDataInfo<OpportunityVo> queryPageList(OpportunityBo bo, PageQuery pageQuery);

    /**
     * 查询销售机会列表
     */
    List<OpportunityVo> queryList(OpportunityBo bo);

    /**
     * 新增销售机会
     */
    Boolean insertByBo(OpportunityBo bo);

    /**
     * 修改销售机会
     */
// f95b0da0e8dceb06e62ba59c8844c92b
    Boolean updateByBo(OpportunityBo bo);

    /**
     * 校验并批量删除销售机会信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
