package com.wxscrmplus.customer.service;

import com.wxscrmplus.customer.domain.vo.CusTypeVo;
import com.wxscrmplus.customer.domain.bo.CusTypeBo;
import com.wxscrmplus.common.core.page.TableDataInfo;
import com.wxscrmplus.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;
// c8208f1530ed37e0c6753b836d2dd4b5

/**
 * 客户类型Service接口
 *
 * @author 王永超
 * @date 2023-03-26
 */
public interface ICusTypeService {

    /**
     * 查询客户类型
     */
    CusTypeVo queryById(Long typeId);

    /**
     * 查询客户类型列表
     */
    TableDataInfo<CusTypeVo> queryPageList(CusTypeBo bo, PageQuery pageQuery);

    /**
     * 查询客户类型列表
     */
    List<CusTypeVo> queryList(CusTypeBo bo);

    /**
     * 新增客户类型
     */
    Boolean insertByBo(CusTypeBo bo);

    /**
     * 修改客户类型
     */
    Boolean updateByBo(CusTypeBo bo);

    /**
     * 校验并批量删除客户类型信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
