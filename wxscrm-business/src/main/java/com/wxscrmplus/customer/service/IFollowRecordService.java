package com.wxscrmplus.customer.service;

import com.wxscrmplus.customer.domain.vo.FollowRecordVo;
import com.wxscrmplus.customer.domain.bo.FollowRecordBo;
import com.wxscrmplus.common.core.page.TableDataInfo;
import com.wxscrmplus.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 跟进记录Service接口
 *
 * @author 王永超
 * @date 2023-03-25
 */
public interface IFollowRecordService {

    /**
     * 查询跟进记录
     */
    FollowRecordVo queryById(Long followRecordId);

    /**
     * 查询跟进记录列表
     */
    TableDataInfo<FollowRecordVo> queryPageList(FollowRecordBo bo, PageQuery pageQuery);

    /**
     * 查询跟进记录列表
     */
    List<FollowRecordVo> queryList(FollowRecordBo bo);

    /**
     * 新增跟进记录
     */
    Boolean insertByBo(FollowRecordBo bo);

    /**
     * 修改跟进记录
     */
    Boolean updateByBo(FollowRecordBo bo);
// b885ebf35d38992de8c904667c374309

    /**
     * 校验并批量删除跟进记录信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

    void timingRemindTomorrow();
}
