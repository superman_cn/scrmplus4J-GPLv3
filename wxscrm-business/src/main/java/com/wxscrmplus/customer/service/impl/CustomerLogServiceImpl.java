package com.wxscrmplus.customer.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.wxscrmplus.common.core.page.TableDataInfo;
import com.wxscrmplus.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.wxscrmplus.customer.domain.bo.CustomerLogBo;
import com.wxscrmplus.customer.domain.vo.CustomerLogVo;
import com.wxscrmplus.customer.domain.CustomerLog;
import com.wxscrmplus.customer.mapper.CustomerLogMapper;
import com.wxscrmplus.customer.service.ICustomerLogService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 客户日志Service业务层处理
 *
 * @author 王永超
 * @date 2023-04-23
 */
@RequiredArgsConstructor
@Service
public class CustomerLogServiceImpl implements ICustomerLogService {

    private final CustomerLogMapper baseMapper;

    /**
     * 查询客户日志
     */
    @Override
    public CustomerLogVo queryById(Long customerLogId) {
        return baseMapper.selectVoById(customerLogId);
    }

    /**
     * 查询客户日志列表
     */
    @Override
    public TableDataInfo<CustomerLogVo> queryPageList(CustomerLogBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<CustomerLog> lqw = buildQueryWrapper(bo);
        Page<CustomerLogVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询客户日志列表
     */
    @Override
    public List<CustomerLogVo> queryList(CustomerLogBo bo) {
        LambdaQueryWrapper<CustomerLog> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<CustomerLog> buildQueryWrapper(CustomerLogBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<CustomerLog> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getCustomerId() != null, CustomerLog::getCustomerId, bo.getCustomerId());
        lqw.orderByDesc(CustomerLog::getCustomerLogId);
        return lqw;
// 5c09963c8f5cf98856c920089bf55d71
    }

    /**
     * 新增客户日志
     */
    @Override
    public Boolean insertByBo(CustomerLogBo bo) {
        CustomerLog add = BeanUtil.toBean(bo, CustomerLog.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setCustomerLogId(add.getCustomerLogId());
        }
        return flag;
    }

    /**
     * 修改客户日志
     */
    @Override
    public Boolean updateByBo(CustomerLogBo bo) {
        CustomerLog update = BeanUtil.toBean(bo, CustomerLog.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(CustomerLog entity) {
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除客户日志
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if (isValid) {
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
