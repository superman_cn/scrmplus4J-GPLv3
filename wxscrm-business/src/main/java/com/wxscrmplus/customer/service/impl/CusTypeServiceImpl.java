package com.wxscrmplus.customer.service.impl;
// 697f44013d3d795d949f6c1e2dc9cd25

import cn.hutool.core.bean.BeanUtil;
import com.wxscrmplus.common.utils.StringUtils;
import com.wxscrmplus.common.core.page.TableDataInfo;
import com.wxscrmplus.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.wxscrmplus.customer.mapper.CustomerMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.wxscrmplus.customer.domain.bo.CusTypeBo;
import com.wxscrmplus.customer.domain.vo.CusTypeVo;
import com.wxscrmplus.customer.domain.CusType;
import com.wxscrmplus.customer.mapper.CusTypeMapper;
import com.wxscrmplus.customer.service.ICusTypeService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 客户类型Service业务层处理
 *
 * @author 王永超
 * @date 2023-03-26
 */
@RequiredArgsConstructor
@Service
public class CusTypeServiceImpl implements ICusTypeService {

    private final CusTypeMapper baseMapper;
    private final CustomerMapper customerMapper;

    /**
     * 查询客户类型
     */
    @Override
    public CusTypeVo queryById(Long typeId) {
        return baseMapper.selectVoById(typeId);
    }

    /**
     * 查询客户类型列表
     */
    @Override
    public TableDataInfo<CusTypeVo> queryPageList(CusTypeBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<CusType> lqw = buildQueryWrapper(bo);
        Page<CusTypeVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询客户类型列表
     */
    @Override
    public List<CusTypeVo> queryList(CusTypeBo bo) {
        LambdaQueryWrapper<CusType> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<CusType> buildQueryWrapper(CusTypeBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<CusType> lqw = Wrappers.lambdaQuery();
        lqw.like(StringUtils.isNotBlank(bo.getName()), CusType::getName, bo.getName());
        lqw.orderByDesc(CusType::getTypeId);
        return lqw;
    }

    /**
     * 新增客户类型
     */
    @Override
    public Boolean insertByBo(CusTypeBo bo) {
        CusType add = BeanUtil.toBean(bo, CusType.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setTypeId(add.getTypeId());
        }
        return flag;
    }

    /**
     * 修改客户类型
     */
    @Override
    public Boolean updateByBo(CusTypeBo bo) {
        CusType update = BeanUtil.toBean(bo, CusType.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(CusType entity) {
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除客户类型
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if (isValid) {
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }

}
