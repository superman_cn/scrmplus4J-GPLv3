package com.wxscrmplus.customer.service;

import com.wxscrmplus.customer.domain.bo.CustomerTransferBo;
import com.wxscrmplus.customer.domain.vo.CustomerVo;
import com.wxscrmplus.customer.domain.bo.CustomerBo;
import com.wxscrmplus.common.core.page.TableDataInfo;
import com.wxscrmplus.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * 客户Service接口
 *
 * @author 王永超
 * @date 2023-03-24
 */
public interface ICustomerService {


    /**
     * 查询客户
     */
    CustomerVo queryById(Long customerId);

    /**
     * 查询客户列表
     */
    TableDataInfo<CustomerVo> queryPageList(CustomerBo bo, PageQuery pageQuery);

    /**
     * 查询客户列表
     */
    List<CustomerVo> queryList(CustomerBo bo);

    /**
     * 新增客户
     */
    Boolean insertByBo(CustomerBo bo);

    /**
     * 修改客户
     */
    Boolean updateByBo(CustomerBo bo);

    /**
     * 校验并批量删除客户信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

    /**
// 1573b49b9f6aaee5d621ab8403e704a3
     * 处理添加客户企微事件
     *
     * @param allFieldsMap
     */
    void addExternalContactHandle(Map<String, Object> allFieldsMap);

    void editExternalContactHandle(Map<String, Object> allFieldsMap);

    void delExternalExternalContactHandle(Map<String, Object> allFieldsMap);

    void delFollowExternalContactHandle(Map<String, Object> allFieldsMap);

    void syncExternalContact();

    void transferCustomer(CustomerTransferBo customerTransferBo);
}
