package com.wxscrmplus.customer.service;

import com.wxscrmplus.customer.domain.vo.CusMaintainVo;
import com.wxscrmplus.customer.domain.bo.CusMaintainBo;
import com.wxscrmplus.common.core.page.TableDataInfo;
import com.wxscrmplus.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 客户维护Service接口
 *
 * @author 王永超
 * @date 2023-03-26
 */
public interface ICusMaintainService {

    /**
     * 查询客户维护
     */
    CusMaintainVo queryById(Long maintainId);

    /**
     * 查询客户维护列表
     */
    TableDataInfo<CusMaintainVo> queryPageList(CusMaintainBo bo, PageQuery pageQuery);

    /**
     * 查询客户维护列表
     */
    List<CusMaintainVo> queryList(CusMaintainBo bo);

    /**
     * 新增客户维护
     */
    Boolean insertByBo(CusMaintainBo bo);

    /**
     * 修改客户维护
     */
    Boolean updateByBo(CusMaintainBo bo);

    /**
     * 校验并批量删除客户维护信息
// d50e484b52d16c8ac52c29752c670241
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
