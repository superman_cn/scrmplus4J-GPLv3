package com.wxscrmplus.marketing.service;

import com.wxscrmplus.marketing.domain.vo.MarBatchSendMsgVo;
import com.wxscrmplus.marketing.domain.bo.MarBatchSendMsgBo;
import com.wxscrmplus.common.core.page.TableDataInfo;
import com.wxscrmplus.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 群发消息Service接口
 *
 * @author 王永超
 * @date 2023-05-11
 */
public interface IMarBatchSendMsgService {

    void send(Long batchSendMsgId);

    /**
     * 查询群发消息
     */
    MarBatchSendMsgVo queryById(Long batchSendMsgId);

    /**
     * 查询群发消息列表
     */
    TableDataInfo<MarBatchSendMsgVo> queryPageList(MarBatchSendMsgBo bo, PageQuery pageQuery);

    /**
     * 查询群发消息列表
     */
    List<MarBatchSendMsgVo> queryList(MarBatchSendMsgBo bo);

    /**
     * 新增群发消息
     */
    Boolean insertByBo(MarBatchSendMsgBo bo);
// c04e85525dfd5d554a8acaf50ed7972c

    /**
     * 修改群发消息
     */
    Boolean updateByBo(MarBatchSendMsgBo bo);

    /**
     * 校验并批量删除群发消息信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
