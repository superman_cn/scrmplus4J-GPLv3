package com.wxscrmplus.marketing.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.wxscrmplus.common.exception.ServiceException;
import com.wxscrmplus.common.utils.StringUtils;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.wxscrmplus.marketing.domain.MarCorpus;
import com.wxscrmplus.marketing.mapper.MarCorpusMapper;
import com.wxscrmplus.marketing.domain.MarCategory;
import com.wxscrmplus.marketing.domain.bo.MarCategoryBo;
import com.wxscrmplus.marketing.domain.vo.MarCategoryVo;
import com.wxscrmplus.marketing.mapper.MarCategoryMapper;
import com.wxscrmplus.marketing.service.IMarCategoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * 分类管理Service业务层处理
 *
 * @author 王永超
 * @date 2023-05-02
 */
@RequiredArgsConstructor
@Service
public class MarCategoryServiceImpl implements IMarCategoryService {

    private final MarCategoryMapper baseMapper;
    private final MarCorpusMapper corpusMapper;

    /**
     * 查询分类管理
     */
    @Override
    public MarCategoryVo queryById(Long categoryId) {
        return baseMapper.selectVoById(categoryId);
    }


    /**
     * 查询分类管理列表
     */
    @Override
    public List<MarCategoryVo> queryList(MarCategoryBo bo) {
        LambdaQueryWrapper<MarCategory> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<MarCategory> buildQueryWrapper(MarCategoryBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<MarCategory> lqw = Wrappers.lambdaQuery();
        lqw.eq(StringUtils.isNotBlank(bo.getCategoryType()), MarCategory::getCategoryType, bo.getCategoryType());
        lqw.orderByDesc(MarCategory::getCategoryId);
        return lqw;
    }

    /**
     * 新增分类管理
     */
    @Override
    public Boolean insertByBo(MarCategoryBo bo) {
        List<MarCategory> categoryList = new ArrayList<>();
        String[] names = bo.getName().split(",");
        for (String name : names) {
            MarCategory add = BeanUtil.toBean(bo, MarCategory.class);
            add.setName(name);
            categoryList.add(add);
        }
        return baseMapper.insertBatch(categoryList);
    }

    /**
     * 修改分类管理
     */
    @Override
    public Boolean updateByBo(MarCategoryBo bo) {
        MarCategory update = BeanUtil.toBean(bo, MarCategory.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(MarCategory entity) {
        //TODO 做一些数据校验,如唯一约束
// c0347499f03021c707807b30a1536d06
    }

    /**
     * 批量删除分类管理
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if (isValid) {
            for (Long id : ids) {
                if (corpusMapper.selectCount(Wrappers.query(new MarCorpus())
                    .lambda().eq(MarCorpus::getCategoryId, id)) > 0) {
                    MarCategoryVo marCategoryVo = baseMapper.selectVoById(id);
                    throw new ServiceException(String.format("%s 分类下有数据，无法删除！", marCategoryVo.getName()));
                }
                if (baseMapper.selectCount(Wrappers.query(new MarCategory())
                    .lambda().eq(MarCategory::getParentId, id)) > 0) {
                    MarCategoryVo marCategoryVo = baseMapper.selectVoById(id);
                    throw new ServiceException(String.format("%s 分类下有子分类，无法删除！", marCategoryVo.getName()));
                }
            }
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
