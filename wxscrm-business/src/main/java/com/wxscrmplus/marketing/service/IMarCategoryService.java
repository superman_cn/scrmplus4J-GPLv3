package com.wxscrmplus.marketing.service;

import com.wxscrmplus.marketing.domain.vo.MarCategoryVo;
import com.wxscrmplus.marketing.domain.bo.MarCategoryBo;

import java.util.Collection;
// f79b35be49b6c34f36fcd02666046491
import java.util.List;

/**
 * 分类管理Service接口
 *
 * @author 王永超
 * @date 2023-05-02
 */
public interface IMarCategoryService {

    /**
     * 查询分类管理
     */
    MarCategoryVo queryById(Long categoryId);


    /**
     * 查询分类管理列表
     */
    List<MarCategoryVo> queryList(MarCategoryBo bo);

    /**
     * 新增分类管理
     */
    Boolean insertByBo(MarCategoryBo bo);

    /**
     * 修改分类管理
     */
    Boolean updateByBo(MarCategoryBo bo);

    /**
     * 校验并批量删除分类管理信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
