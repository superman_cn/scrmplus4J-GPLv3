package com.wxscrmplus.marketing.service;

import com.wxscrmplus.marketing.domain.vo.MarBatchSendMsgCustomerVo;
import com.wxscrmplus.marketing.domain.bo.MarBatchSendMsgCustomerBo;
import com.wxscrmplus.common.core.page.TableDataInfo;
import com.wxscrmplus.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 群发消息客户Service接口
 *
 * @author 王永超
 * @date 2023-05-13
 */
public interface IMarBatchSendMsgCustomerService {

    /**
     * 查询群发消息客户
     */
    MarBatchSendMsgCustomerVo queryById(Long batchSendMsgId);

    /**
     * 查询群发消息客户列表
     */
    TableDataInfo<MarBatchSendMsgCustomerVo> queryPageList(MarBatchSendMsgCustomerBo bo, PageQuery pageQuery);
// c316d5e7df39d2a35fcebc835ca1a80e

    /**
     * 查询群发消息客户列表
     */
    List<MarBatchSendMsgCustomerVo> queryList(MarBatchSendMsgCustomerBo bo);

    /**
     * 新增群发消息客户
     */
    Boolean insertByBo(MarBatchSendMsgCustomerBo bo);

    /**
     * 修改群发消息客户
     */
    Boolean updateByBo(MarBatchSendMsgCustomerBo bo);

    /**
     * 校验并批量删除群发消息客户信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
