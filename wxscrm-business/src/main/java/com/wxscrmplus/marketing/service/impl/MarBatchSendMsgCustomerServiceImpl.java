package com.wxscrmplus.marketing.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.wxscrmplus.common.core.page.TableDataInfo;
import com.wxscrmplus.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.wxscrmplus.marketing.domain.MarBatchSendMsgCustomer;
import com.wxscrmplus.marketing.mapper.MarBatchSendMsgCustomerMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.wxscrmplus.marketing.domain.bo.MarBatchSendMsgCustomerBo;
import com.wxscrmplus.marketing.domain.vo.MarBatchSendMsgCustomerVo;
import com.wxscrmplus.marketing.service.IMarBatchSendMsgCustomerService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 群发消息客户Service业务层处理
 *
 * @author 王永超
 * @date 2023-05-13
 */
@RequiredArgsConstructor
@Service
public class MarBatchSendMsgCustomerServiceImpl implements IMarBatchSendMsgCustomerService {
// 0e5fa99764292e4b2e03de59455e4bc2

    private final MarBatchSendMsgCustomerMapper baseMapper;

    /**
     * 查询群发消息客户
     */
    @Override
    public MarBatchSendMsgCustomerVo queryById(Long batchSendMsgId){
        return baseMapper.selectVoById(batchSendMsgId);
    }

    /**
     * 查询群发消息客户列表
     */
    @Override
    public TableDataInfo<MarBatchSendMsgCustomerVo> queryPageList(MarBatchSendMsgCustomerBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<MarBatchSendMsgCustomer> lqw = buildQueryWrapper(bo);
        Page<MarBatchSendMsgCustomerVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询群发消息客户列表
     */
    @Override
    public List<MarBatchSendMsgCustomerVo> queryList(MarBatchSendMsgCustomerBo bo) {
        LambdaQueryWrapper<MarBatchSendMsgCustomer> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<MarBatchSendMsgCustomer> buildQueryWrapper(MarBatchSendMsgCustomerBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<MarBatchSendMsgCustomer> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getCustomerId() != null, MarBatchSendMsgCustomer::getCustomerId, bo.getCustomerId());
        return lqw;
    }

    /**
     * 新增群发消息客户
     */
    @Override
    public Boolean insertByBo(MarBatchSendMsgCustomerBo bo) {
        MarBatchSendMsgCustomer add = BeanUtil.toBean(bo, MarBatchSendMsgCustomer.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setBatchSendMsgId(add.getBatchSendMsgId());
        }
        return flag;
    }

    /**
     * 修改群发消息客户
     */
    @Override
    public Boolean updateByBo(MarBatchSendMsgCustomerBo bo) {
        MarBatchSendMsgCustomer update = BeanUtil.toBean(bo, MarBatchSendMsgCustomer.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(MarBatchSendMsgCustomer entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除群发消息客户
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
