package com.wxscrmplus.marketing.service;

import com.wxscrmplus.marketing.domain.vo.MarCorpusVo;
import com.wxscrmplus.marketing.domain.bo.MarCorpusBo;
import com.wxscrmplus.common.core.page.TableDataInfo;
import com.wxscrmplus.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 话术库Service接口
 *
 * @author 王永超
// 023a77370b984bdefcc828624034aa08
 * @date 2023-05-02
 */
public interface IMarCorpusService {

    /**
     * 查询话术库
     */
    MarCorpusVo queryById(Long corpusId);

    /**
     * 查询话术库列表
     */
    TableDataInfo<MarCorpusVo> queryPageList(MarCorpusBo bo, PageQuery pageQuery);

    /**
     * 查询话术库列表
     */
    List<MarCorpusVo> queryList(MarCorpusBo bo);

    /**
     * 新增话术库
     */
    Boolean insertByBo(MarCorpusBo bo);

    /**
     * 修改话术库
     */
    Boolean updateByBo(MarCorpusBo bo);

    /**
     * 校验并批量删除话术库信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
