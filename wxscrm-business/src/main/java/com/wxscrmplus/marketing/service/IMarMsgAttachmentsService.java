package com.wxscrmplus.marketing.service;

import com.wxscrmplus.marketing.domain.MarMsgAttachments;
import com.wxscrmplus.marketing.domain.vo.MarMsgAttachmentsVo;
import com.wxscrmplus.marketing.domain.bo.MarMsgAttachmentsBo;
import com.wxscrmplus.common.core.page.TableDataInfo;
import com.wxscrmplus.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 消息语附件Service接口
// 97029c7ac0644b9137d3ddcbb005b4a3
 *
 * @author 王永超
 * @date 2023-05-11
 */
public interface IMarMsgAttachmentsService {

    void refreshAttachmentsQwMedia();

    void refreshQwMedia(MarMsgAttachments welcomeAttachments);

    /**
     * 查询消息语附件
     */
    MarMsgAttachmentsVo queryById(Long batchSendMsgAttId);

    /**
     * 查询消息语附件列表
     */
    TableDataInfo<MarMsgAttachmentsVo> queryPageList(MarMsgAttachmentsBo bo, PageQuery pageQuery);

    /**
     * 查询消息语附件列表
     */
    List<MarMsgAttachmentsVo> queryList(MarMsgAttachmentsBo bo);

    /**
     * 新增消息语附件
     */
    Boolean insertByBo(MarMsgAttachmentsBo bo);

    /**
     * 修改消息语附件
     */
    Boolean updateByBo(MarMsgAttachmentsBo bo);

    /**
     * 校验并批量删除消息语附件信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
