package com.wxscrmplus.marketing.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import com.wxscrmplus.common.utils.StringUtils;
import com.wxscrmplus.common.core.page.TableDataInfo;
import com.wxscrmplus.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.wxscrmplus.marketing.mapper.MarCategoryMapper;
import com.wxscrmplus.marketing.domain.MarCorpus;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.wxscrmplus.marketing.domain.bo.MarCorpusBo;
import com.wxscrmplus.marketing.domain.vo.MarCorpusVo;
import com.wxscrmplus.marketing.mapper.MarCorpusMapper;
import com.wxscrmplus.marketing.service.IMarCorpusService;

import java.util.*;

/**
 * 话术库Service业务层处理
 *
 * @author 王永超
 * @date 2023-05-02
 */
@RequiredArgsConstructor
@Service
public class MarCorpusServiceImpl implements IMarCorpusService {

    private final MarCorpusMapper baseMapper;
    private final MarCategoryMapper categoryMapper;

    /**
     * 查询话术库
     */
    @Override
    public MarCorpusVo queryById(Long corpusId) {
        MarCorpusVo marCorpusVo = baseMapper.selectVoById(corpusId);
        marCorpusVo.setCategoryLabel(categoryMapper.findNameById(marCorpusVo.getCategoryId()));
        return marCorpusVo;
    }

    /**
     * 查询话术库列表
     */
    @Override
    public TableDataInfo<MarCorpusVo> queryPageList(MarCorpusBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<MarCorpus> lqw = buildQueryWrapper(bo);
        Page<MarCorpusVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        for (MarCorpusVo record : result.getRecords()) {
            record.setCategoryLabel(categoryMapper.findNameById(record.getCategoryId()));
        }
        return TableDataInfo.build(result);
    }

    /**
     * 查询话术库列表
     */
    @Override
    public List<MarCorpusVo> queryList(MarCorpusBo bo) {
        LambdaQueryWrapper<MarCorpus> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<MarCorpus> buildQueryWrapper(MarCorpusBo bo) {
        Set<Long> categoryIdList = new HashSet<>();
        if (ObjectUtil.isNotNull(bo.getCategoryId())) {
            categoryIdList.add(bo.getCategoryId());
        }
        if (CollUtil.isNotEmpty(bo.getCategoryIdList())) {
            categoryIdList.addAll(bo.getCategoryIdList());
        }
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<MarCorpus> lqw = Wrappers.lambdaQuery();
        lqw.in(CollUtil.isNotEmpty(categoryIdList), MarCorpus::getCategoryId, categoryIdList);
        lqw.like(StringUtils.isNotBlank(bo.getQuestion()), MarCorpus::getQuestion, bo.getQuestion());
        lqw.orderByAsc(MarCorpus::getSort);
        lqw.orderByDesc(MarCorpus::getCorpusId);
        return lqw;
    }

    /**
     * 新增话术库
     */
    @Override
    public Boolean insertByBo(MarCorpusBo bo) {
        MarCorpus add = BeanUtil.toBean(bo, MarCorpus.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setCorpusId(add.getCorpusId());
        }
        return flag;
    }

// 25fa6a887b80bdbdfc1b1c270d5b676c
    /**
     * 修改话术库
     */
    @Override
    public Boolean updateByBo(MarCorpusBo bo) {
        MarCorpus update = BeanUtil.toBean(bo, MarCorpus.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(MarCorpus entity) {
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除话术库
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if (isValid) {
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
