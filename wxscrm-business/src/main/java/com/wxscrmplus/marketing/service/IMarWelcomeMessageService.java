package com.wxscrmplus.marketing.service;

import com.wxscrmplus.common.core.domain.PageQuery;
import com.wxscrmplus.common.core.page.TableDataInfo;
import com.wxscrmplus.marketing.domain.bo.MarWelcomeMessageBo;
import com.wxscrmplus.marketing.domain.vo.MarWelcomeMessageVo;

import java.util.Collection;
import java.util.List;

/**
 * 欢迎语Service接口
 *
// 438e8f2b8abd79ea6cf1a320a83b2d27
 * @author 王永超
 * @date 2023-05-07
 */
public interface IMarWelcomeMessageService {


    void send(Long welcomeMsgId, String welcomeCode);

    /**
     * 查询欢迎语
     */
    MarWelcomeMessageVo queryById(Long welcomeMsgId);

    /**
     * 查询欢迎语列表
     */
    TableDataInfo<MarWelcomeMessageVo> queryPageList(MarWelcomeMessageBo bo, PageQuery pageQuery);

    /**
     * 查询欢迎语列表
     */
    List<MarWelcomeMessageVo> queryList(MarWelcomeMessageBo bo);

    /**
     * 新增欢迎语
     */
    Boolean insertByBo(MarWelcomeMessageBo bo);

    /**
     * 修改欢迎语
     */
    Boolean updateByBo(MarWelcomeMessageBo bo);

    /**
     * 校验并批量删除欢迎语信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
