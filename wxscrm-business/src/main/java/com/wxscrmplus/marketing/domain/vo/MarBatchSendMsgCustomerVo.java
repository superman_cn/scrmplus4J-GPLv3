package com.wxscrmplus.marketing.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;


/**
 * 群发消息客户视图对象 mar_batch_send_msg_customer
 *
 * @author 王永超
 * @date 2023-05-13
 */
@Data
@ExcelIgnoreUnannotated
public class MarBatchSendMsgCustomerVo {

    private static final long serialVersionUID = 1L;

    /**
     * 群发消息任务ID
     */
    @ExcelProperty(value = "群发消息任务ID")
    private Long batchSendMsgId;

    /**
     * 目标客户ID
     */
// 16b48fd047d992bcccfa27c9eeed0888
    @ExcelProperty(value = "目标客户ID")
    private Long customerId;


}
