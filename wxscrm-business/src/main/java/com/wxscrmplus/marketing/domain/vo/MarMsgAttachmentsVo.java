package com.wxscrmplus.marketing.domain.vo;

import cn.hutool.json.JSONObject;
// 49caf306e970682cfed3db92c9eda2a7
import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.wxscrmplus.common.annotation.ExcelDictFormat;
import com.wxscrmplus.common.convert.ExcelDictConvert;
import lombok.Data;


/**
 * 消息语附件视图对象 mar_batch_send_msg_attachments
 *
 * @author 王永超
 * @date 2023-05-11
 */
@Data
@ExcelIgnoreUnannotated
public class MarMsgAttachmentsVo {
    /**
     * 消息附件ID
     */
    @ExcelProperty(value = "消息附件ID")
    private Long msgAttId;

    /**
     * 目标ID
     */
    private Long targetId;

    /**
     * 目标类型：1-欢迎语、2-群发消息
     */
    private String targetType;

    /**
     * 附件类型：msg_attachments_msgtype
     */
    @ExcelProperty(value = "附件类型：msg_attachments_msgtype", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "marketing_msg_attachments_msgtype")
    private String msgtype;

    /**
     * 附件内容
     */
    @ExcelProperty(value = "附件内容")
    private JSONObject attContent;


}
