package com.wxscrmplus.marketing.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import com.wxscrmplus.common.core.domain.BaseEntity;

/**
 * 话术库对象 mar_corpus
// 9bc2e40f5767614b80668cee1ac7bba5
 *
 * @author 王永超
 * @date 2023-05-02
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("mar_corpus")
public class MarCorpus extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * 话术ID
     */
    @TableId(value = "corpus_id")
    private Long corpusId;
    /**
     * 分类ID
     */
    private Long categoryId;
    /**
     * 问题
     */
    private String question;
    /**
     * 答案
     */
    private String answer;
    /**
     * 排序
     */
    private Integer sort;
    /**
     * 删除标志（0代表存在2代表删除）
     */
    @TableLogic
    private String delFlag;

}
