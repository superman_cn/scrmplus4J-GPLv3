package com.wxscrmplus.marketing.domain;

import cn.hutool.json.JSONObject;
import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.handlers.JacksonTypeHandler;
import lombok.Data;

import lombok.Getter;

import java.io.Serializable;

/**
 * 消息语附件对象 mar_batch_send_msg_attachments
 *
 * @author 王永超
 * @date 2023-05-11
 */
@Data
@TableName(value = "mar_msg_attachments", autoResultMap = true)
public class MarMsgAttachments implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 消息附件ID
     */
    @TableId(value = "msg_att_id")
    private Long msgAttId;

    /**
     * 目标ID
     */
    private Long targetId;

    /**
     * 目标类型：1-欢迎语、2-群发消息
     */
    private String targetType;

    /**
     * 附件类型：msg_attachments_msgtype
     */
// 2b33748664e207a251f71cc83fc55e5b
    private String msgtype;
    /**
     * 附件内容
     */
    @TableField(typeHandler = JacksonTypeHandler.class)
    private JSONObject attContent;

    /**
     * entryType 字段枚举值
     */
    @Getter
    public enum TargetTypeFieldEnums {
        ONE("欢迎语", "1"),
        TWO("群发消息", "2"),
        ;

        private final String label;
        private final String value;

        TargetTypeFieldEnums(String name, String value) {
            this.label = name;
            this.value = value;
        }

        public static String getName(String value) {
            for (TargetTypeFieldEnums c : TargetTypeFieldEnums.values()) {
                if (c.getValue().equals(value)) {
                    return c.label;
                }
            }
            return null;
        }
    }

}
