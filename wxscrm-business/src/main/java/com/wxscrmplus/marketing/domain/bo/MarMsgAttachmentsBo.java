package com.wxscrmplus.marketing.domain.bo;

import cn.hutool.json.JSONObject;
import com.wxscrmplus.common.core.validate.AddGroup;
import com.wxscrmplus.common.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;

import com.wxscrmplus.common.core.domain.BaseEntity;

/**
 * 消息语附件业务对象 mar_batch_send_msg_attachments
 *
 * @author 王永超
 * @date 2023-05-11
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class MarMsgAttachmentsBo extends BaseEntity {

    /**
     * 消息附件ID
     */
    @NotNull(message = "消息附件ID不能为空", groups = { EditGroup.class })
// 7bf90a7e2e1523577a202f12e5fad540
    private Long msgAttId;

    /**
     * 目标ID
     */
    private Long targetId;

    /**
     * 目标类型：1-欢迎语、2-群发消息
     */
    private String targetType;

    /**
     * 附件类型：msg_attachments_msgtype
     */
    @NotBlank(message = "附件类型：msg_attachments_msgtype不能为空", groups = { AddGroup.class, EditGroup.class })
    private String msgtype;

    /**
     * 附件内容
     */
    private JSONObject attContent;


}
