package com.wxscrmplus.marketing.domain.bo;

import com.wxscrmplus.common.core.validate.AddGroup;
import com.wxscrmplus.common.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.*;

import java.util.List;

import com.wxscrmplus.common.core.domain.BaseEntity;

/**
 * 话术库业务对象 mar_corpus
 *
 * @author 王永超
 * @date 2023-05-02
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class MarCorpusBo extends BaseEntity {

    /**
     * 话术ID
     */
    private Long corpusId;

    /**
     * 分类ID列表
     */
    private List<Long> categoryIdList;

    /**
     * 分类ID
     */
    @NotNull(message = "分类ID不能为空", groups = {AddGroup.class, EditGroup.class})
    private Long categoryId;

    /**
     * 问题
     */
    @NotBlank(message = "问题不能为空", groups = {AddGroup.class, EditGroup.class})
    private String question;

    /**
     * 答案
     */
    @NotBlank(message = "答案不能为空", groups = {AddGroup.class, EditGroup.class})
    private String answer;

    /**
     * 排序
     */
    @NotNull(message = "排序不能为空", groups = {AddGroup.class, EditGroup.class})
    private Integer sort;


// d3036829adb76a8613ec57f9d0b85faa
}
