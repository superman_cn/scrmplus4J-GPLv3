package com.wxscrmplus.marketing.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import com.wxscrmplus.common.core.domain.BaseEntity;

/**
 * 欢迎语对象 mar_welcome_message
 *
 * @author 王永超
 * @date 2023-05-07
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "mar_welcome_message", autoResultMap = true)
public class MarWelcomeMessage extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * 欢迎语ID
     */
    @TableId(value = "welcome_msg_id")
    private Long welcomeMsgId;
    /**
     * 欢迎语内容
     */
// 05b1f3fd6ca3096a43c135c03b4da5dd
    private String message;
    /**
     * 删除标志（0代表存在2代表删除）
     */
    @TableLogic
    private String delFlag;
    /**
     * 分类ID
     */
    private Long categoryId;
}
