package com.wxscrmplus.marketing.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import com.wxscrmplus.common.core.domain.BaseEntity;
import lombok.Getter;

/**
 * 群发消息对象 mar_batch_send_msg
 *
 * @author 王永超
 * @date 2023-05-13
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("mar_batch_send_msg")
public class MarBatchSendMsg extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * 群发消息ID
     */
    @TableId(value = "batch_send_msg_id")
    private Long batchSendMsgId;
    /**
     * 使用成员ID
     */
    private Long userId;
    /**
     * 群发消息的类型，默认为single，表示发送给客户，group表示发送给客户群
     */
    private String chatType;
    /**
     * 消息文本
     */
    private String messageText;
    /**
     * 是否允许成员在待发送客户列表中重新进行选择，默认为false
     */
    private Boolean allowSelect;
    /**
     * 企业群发消息的id
     */
    private String msgid;
    /**
     * 无效或无法发送的external_userid列表
     */
    private String failList;
    /**
     * 分类ID
     */
    private Long categoryId;
    /**
     * 删除标志（0代表存在2代表删除）
     */
    @TableLogic
    private String delFlag;

    /**
     * entryType 字段枚举值
     */
    @Getter
    public enum ChatTypeFieldEnums {
        ONE("客户", "single"),
        TWO("客户群", "group"),
        ;

        private final String label;
        private final String value;
// ba63e54c8891315c9ea5ea8b93cc41e6

        ChatTypeFieldEnums(String name, String value) {
            this.label = name;
            this.value = value;
        }

        public static String getName(String value) {
            for (ChatTypeFieldEnums c : ChatTypeFieldEnums.values()) {
                if (c.getValue().equals(value)) {
                    return c.label;
                }
            }
            return null;
        }
    }

}
