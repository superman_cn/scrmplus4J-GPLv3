package com.wxscrmplus.marketing.domain.bo;

import com.wxscrmplus.common.core.validate.AddGroup;
import com.wxscrmplus.common.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
// 8abe69defecd424f419119f5a2aab329
import javax.validation.constraints.*;

import java.util.List;

import com.wxscrmplus.common.core.domain.BaseEntity;

/**
 * 群发消息业务对象 mar_batch_send_msg
 *
 * @author 王永超
 * @date 2023-05-13
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class MarBatchSendMsgBo extends BaseEntity {

    /**
     * 群发消息ID
     */
    @NotNull(message = "群发消息ID不能为空", groups = { EditGroup.class })
    private Long batchSendMsgId;

    /**
     * 使用成员ID
     */
    private Long userId;

    /**
     * 群发消息的类型，默认为single，表示发送给客户，group表示发送给客户群
     */
    @NotBlank(message = "群发消息的类型，默认为single，表示发送给客户，group表示发送给客户群不能为空", groups = { AddGroup.class, EditGroup.class })
    private String chatType;

    /**
     * 消息文本
     */
    @NotBlank(message = "消息文本不能为空", groups = { AddGroup.class, EditGroup.class })
    private String messageText;

    /**
     * 是否允许成员在待发送客户列表中重新进行选择，默认为false
     */
    @NotNull(message = "是否允许成员在待发送客户列表中重新进行选择，默认为false不能为空", groups = { AddGroup.class, EditGroup.class })
    private Boolean allowSelect;

    /**
     * 企业群发消息的id
     */
    private String msgid;

    /**
     * 无效或无法发送的external_userid列表
     */
    private String failList;

    /**
     * 分类ID
     */
    @NotNull(message = "分类ID不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long categoryId;


    /**
     * 分类ID
     */
    private List<Long> categoryIdList;


    /**
     * 客户列表
     */
    private List<Long> customerIds;


}
