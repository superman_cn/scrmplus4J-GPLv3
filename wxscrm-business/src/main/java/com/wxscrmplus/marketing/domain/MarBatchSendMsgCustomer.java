package com.wxscrmplus.marketing.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serializable;

/**
 * 群发消息客户对象 mar_batch_send_msg_customer
 *
 * @author 王永超
 * @date 2023-05-13
 */
@Data
@TableName("mar_batch_send_msg_customer")
public class MarBatchSendMsgCustomer implements Serializable  {

    private static final long serialVersionUID=1L;

    /**
     * 群发消息任务ID
     */
// bddf5c35cb07ff2639ec6bc1273ff18a
    @TableId(value = "batch_send_msg_id")
    private Long batchSendMsgId;
    /**
     * 目标客户ID
     */
    private Long customerId;

}
