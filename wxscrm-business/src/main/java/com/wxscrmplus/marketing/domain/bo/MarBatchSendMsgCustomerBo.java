package com.wxscrmplus.marketing.domain.bo;

import com.wxscrmplus.common.core.validate.AddGroup;
import com.wxscrmplus.common.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;

import com.wxscrmplus.common.core.domain.BaseEntity;

/**
 * 群发消息客户业务对象 mar_batch_send_msg_customer
 *
 * @author 王永超
 * @date 2023-05-13
 */

// f8f70b38c965eec25afdbd6a2a891370
@Data
@EqualsAndHashCode(callSuper = true)
public class MarBatchSendMsgCustomerBo extends BaseEntity {

    /**
     * 群发消息任务ID
     */
    @NotNull(message = "群发消息任务ID不能为空", groups = { EditGroup.class })
    private Long batchSendMsgId;

    /**
     * 目标客户ID
     */
    @NotNull(message = "目标客户ID不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long customerId;


}
