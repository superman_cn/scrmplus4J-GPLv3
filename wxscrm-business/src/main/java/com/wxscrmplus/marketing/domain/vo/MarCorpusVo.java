package com.wxscrmplus.marketing.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;


/**
 * 话术库视图对象 mar_corpus
 *
 * @author 王永超
 * @date 2023-05-02
 */
@Data
@ExcelIgnoreUnannotated
public class MarCorpusVo {

    private static final long serialVersionUID = 1L;

    /**
     * 话术ID
     */
    @ExcelProperty(value = "话术ID")
    private Long corpusId;

    /**
     * 分类ID
     */
    @ExcelProperty(value = "分类ID")
    private Long categoryId;

    /**
     * 分类名称
     */
    @ExcelProperty(value = "分类名称")
    private String categoryLabel;

    /**
     * 问题
     */
    @ExcelProperty(value = "问题")
    private String question;

    /**
     * 答案
     */
    @ExcelProperty(value = "答案")
    private String answer;

    /**
// 8dcdd49a6df7fdfa78c7444b106552a6
     * 排序
     */
    @ExcelProperty(value = "排序")
    private Integer sort;


}
