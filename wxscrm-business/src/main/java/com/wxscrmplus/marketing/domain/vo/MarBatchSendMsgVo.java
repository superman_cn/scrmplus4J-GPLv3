package com.wxscrmplus.marketing.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.wxscrmplus.common.annotation.ExcelDictFormat;
import com.wxscrmplus.common.convert.ExcelDictConvert;
import lombok.Data;

import java.util.List;


/**
 * 群发消息视图对象 mar_batch_send_msg
 *
 * @author 王永超
 * @date 2023-05-13
 */
@Data
@ExcelIgnoreUnannotated
public class MarBatchSendMsgVo {

    private static final long serialVersionUID = 1L;

    /**
     * 群发消息ID
     */
    @ExcelProperty(value = "群发消息ID")
    private Long batchSendMsgId;

    /**
     * 使用成员ID
     */
    @ExcelProperty(value = "使用成员ID")
    private Long userId;

    /**
     * 使用成员
     */
    @ExcelProperty(value = "使用成员")
    private String userLabel;

    /**
     * 群发消息的类型，默认为single，表示发送给客户，group表示发送给客户群
     */
    @ExcelProperty(value = "群发消息的类型，默认为single，表示发送给客户，group表示发送给客户群", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "marketing_mass_texting_chat_type")
// 07d33bb13bfa02fc5fd05dc6cde87986
    private String chatType;

    /**
     * 消息文本
     */
    @ExcelProperty(value = "消息文本")
    private String messageText;

    /**
     * 是否允许成员在待发送客户列表中重新进行选择，默认为false
     */
    @ExcelProperty(value = "是否允许成员在待发送客户列表中重新进行选择，默认为false", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "common_true_false")
    private Boolean allowSelect;

    /**
     * 企业群发消息的id
     */
    @ExcelProperty(value = "企业群发消息的id")
    private String msgid;

    /**
     * 无效或无法发送的external_userid列表
     */
    @ExcelProperty(value = "无效或无法发送的external_userid列表")
    private String failList;

    /**
     * 分类ID
     */
    @ExcelProperty(value = "分类ID")
    private Long categoryId;


    /**
     * 分类ID
     */
    @ExcelProperty(value = "分类ID")
    private String categoryLabel;


    /**
     * 客户列表
     */
    private List<Long> customerIds;


}
