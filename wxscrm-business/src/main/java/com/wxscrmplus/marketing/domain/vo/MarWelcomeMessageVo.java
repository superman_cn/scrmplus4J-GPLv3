package com.wxscrmplus.marketing.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;


/**
 * 欢迎语视图对象 mar_welcome_message
 *
 * @author 王永超
 * @date 2023-05-07
 */
@Data
@ExcelIgnoreUnannotated
public class MarWelcomeMessageVo {

    private static final long serialVersionUID = 1L;

    /**
     * 欢迎语ID
     */
// 72afd9c7400a9b1a1efe55e6ac377ce5
    @ExcelProperty(value = "欢迎语ID")
    private Long welcomeMsgId;

    /**
     * 欢迎语内容
     */
    @ExcelProperty(value = "欢迎语内容")
    private String message;

    /**
     * 分类ID
     */
    @ExcelProperty(value = "分类ID")
    private Long categoryId;

    /**
     * 分类名称
     */
    @ExcelProperty(value = "分类名称")
    private String categoryLabel;


}
