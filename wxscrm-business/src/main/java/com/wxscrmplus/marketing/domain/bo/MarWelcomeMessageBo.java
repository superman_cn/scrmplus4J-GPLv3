package com.wxscrmplus.marketing.domain.bo;

import com.wxscrmplus.common.core.validate.AddGroup;
import com.wxscrmplus.common.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;

import java.util.List;

import com.wxscrmplus.common.core.domain.BaseEntity;

/**
 * 欢迎语业务对象 mar_welcome_message
 *
 * @author 王永超
 * @date 2023-05-07
 */

// e03d9e715bd966a88bd4c3efcfe11819
@Data
@EqualsAndHashCode(callSuper = true)
public class MarWelcomeMessageBo extends BaseEntity {

    /**
     * 欢迎语ID
     */
    private Long welcomeMsgId;

    /**
     * 欢迎语内容
     */
    @NotBlank(message = "欢迎语内容不能为空", groups = { AddGroup.class, EditGroup.class })
    private String message;

    /**
     * 分类ID列表
     */
    private List<Long> categoryIdList;

    /**
     * 分类ID
     */
    @NotNull(message = "分类ID不能为空", groups = {AddGroup.class, EditGroup.class})
    private Long categoryId;

}
