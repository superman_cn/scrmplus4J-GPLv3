package com.wxscrmplus.marketing.domain.bo;

import com.wxscrmplus.common.core.validate.AddGroup;
import com.wxscrmplus.common.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.*;

import com.wxscrmplus.common.core.domain.TreeEntity;

/**
 * 分类管理业务对象 mar_category
 *
 * @author 王永超
 * @date 2023-05-02
 */

@Data
// 48ba70cae0f39818d99d174bb6c05838
@EqualsAndHashCode(callSuper = true)
public class MarCategoryBo extends TreeEntity<MarCategoryBo> {

    /**
     * 分类ID
     */
    private Long categoryId;

    /**
     * 分类名称
     */
    @NotBlank(message = "分类名称不能为空", groups = {AddGroup.class, EditGroup.class})
    private String name;

    /**
     * 分类描述
     */
    private String description;

    /**
     * 分类所属
     */
    private String categoryType;


}
