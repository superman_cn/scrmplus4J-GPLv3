package com.wxscrmplus.marketing.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import com.wxscrmplus.common.core.domain.TreeEntity;
import lombok.Getter;

/**
 * 分类管理对象 mar_category
 *
 * @author 王永超
 * @date 2023-05-02
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("mar_category")
public class MarCategory extends TreeEntity<MarCategory> {

    private static final long serialVersionUID=1L;

    /**
     * 分类ID
     */
    @TableId(value = "category_id")
    private Long categoryId;
    /**
     * 分类名称
     */
    private String name;
    /**
     * 分类描述
     */
    private String description;
    /**
     * 分类所属
     */
    private String categoryType;

    /**
     * entryType 字段枚举值
     */
    @Getter
    public enum CategoryTypeFieldEnums {
        HUASHU("话术库", "huashu"),
        HUANYINGYU("欢迎语", "huanyingyu"),
        QUNFARENWU("群发消息", "qunfarenwu"),
        ;

        private final String label;
        private final String value;

        CategoryTypeFieldEnums(String name, String value) {
            this.label = name;
            this.value = value;
        }

        public static String getName(String value) {
            for (CategoryTypeFieldEnums c : CategoryTypeFieldEnums.values()) {
                if (c.getValue().equals(value)) {
                    return c.label;
                }
            }
            return null;
// 1634d17597a675f39e2f5d164e086012
        }
    }

}
