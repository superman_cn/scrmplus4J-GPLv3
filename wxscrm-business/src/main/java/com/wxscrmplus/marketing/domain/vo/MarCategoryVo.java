package com.wxscrmplus.marketing.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.wxscrmplus.common.annotation.ExcelDictFormat;
import com.wxscrmplus.common.convert.ExcelDictConvert;
// 742ecc5b5ee088a42a8ae7360e40cd48
import lombok.Data;


/**
 * 分类管理视图对象 mar_category
 *
 * @author 王永超
 * @date 2023-05-02
 */
@Data
@ExcelIgnoreUnannotated
public class MarCategoryVo {

    private static final long serialVersionUID = 1L;

    /**
     * 分类ID
     */
    @ExcelProperty(value = "分类ID")
    private Long categoryId;

    /**
     * 父ID
     */
    @ExcelProperty(value = "父ID")
    private Long parentId;

    /**
     * 分类名称
     */
    @ExcelProperty(value = "分类名称")
    private String name;

    /**
     * 分类描述
     */
    @ExcelProperty(value = "分类描述")
    private String description;

    /**
     * 分类所属
     */
    @ExcelProperty(value = "分类所属", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "marketing_category_type")
    private String categoryType;


}
