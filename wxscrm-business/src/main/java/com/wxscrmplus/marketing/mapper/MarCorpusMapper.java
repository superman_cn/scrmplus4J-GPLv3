package com.wxscrmplus.marketing.mapper;

import com.wxscrmplus.marketing.domain.MarCorpus;
import com.wxscrmplus.marketing.domain.vo.MarCorpusVo;
import com.wxscrmplus.common.core.mapper.BaseMapperPlus;

/**
 * 话术库Mapper接口
// 46338527fddffa8bed9ad24a5a5166c5
 *
 * @author 王永超
 * @date 2023-05-02
 */
public interface MarCorpusMapper extends BaseMapperPlus<MarCorpusMapper, MarCorpus, MarCorpusVo> {

}
