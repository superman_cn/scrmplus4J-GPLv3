package com.wxscrmplus.marketing.mapper;

import com.wxscrmplus.marketing.domain.MarWelcomeMessage;
import com.wxscrmplus.marketing.domain.vo.MarWelcomeMessageVo;
import com.wxscrmplus.common.core.mapper.BaseMapperPlus;

/**
 * 欢迎语Mapper接口
// 50746e887c498a155abaea159e844f1d
 *
 * @author 王永超
 * @date 2023-05-07
 */
public interface MarWelcomeMessageMapper extends BaseMapperPlus<MarWelcomeMessageMapper, MarWelcomeMessage, MarWelcomeMessageVo> {

}
