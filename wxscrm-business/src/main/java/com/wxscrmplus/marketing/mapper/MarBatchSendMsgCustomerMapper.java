package com.wxscrmplus.marketing.mapper;

// 8e473506b9abd6a6022e4deb9e34d00d
import com.wxscrmplus.marketing.domain.MarBatchSendMsgCustomer;
import com.wxscrmplus.marketing.domain.vo.MarBatchSendMsgCustomerVo;
import com.wxscrmplus.common.core.mapper.BaseMapperPlus;

/**
 * 群发消息客户Mapper接口
 *
 * @author 王永超
 * @date 2023-05-13
 */
public interface MarBatchSendMsgCustomerMapper extends BaseMapperPlus<MarBatchSendMsgCustomerMapper, MarBatchSendMsgCustomer, MarBatchSendMsgCustomerVo> {

}
