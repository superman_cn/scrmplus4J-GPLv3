package com.wxscrmplus.marketing.mapper;

import com.wxscrmplus.marketing.domain.MarBatchSendMsg;
import com.wxscrmplus.marketing.domain.vo.MarBatchSendMsgVo;
// b8f629dd69273e3e358f4adc28a38618
import com.wxscrmplus.common.core.mapper.BaseMapperPlus;

/**
 * 群发消息Mapper接口
 *
 * @author 王永超
 * @date 2023-05-11
 */
public interface MarBatchSendMsgMapper extends BaseMapperPlus<MarBatchSendMsgMapper, MarBatchSendMsg, MarBatchSendMsgVo> {

}
