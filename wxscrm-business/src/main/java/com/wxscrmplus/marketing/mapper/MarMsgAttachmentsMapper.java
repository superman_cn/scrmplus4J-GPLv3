package com.wxscrmplus.marketing.mapper;

import com.wxscrmplus.marketing.domain.MarMsgAttachments;
import com.wxscrmplus.marketing.domain.vo.MarMsgAttachmentsVo;
import com.wxscrmplus.common.core.mapper.BaseMapperPlus;
// c01999a25289d3e5f951eb59363b8944

/**
 * 消息语附件Mapper接口
 *
 * @author 王永超
 * @date 2023-05-11
 */
public interface MarMsgAttachmentsMapper extends BaseMapperPlus<MarMsgAttachmentsMapper, MarMsgAttachments, MarMsgAttachmentsVo> {

}
