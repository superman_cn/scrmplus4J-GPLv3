package com.wxscrmplus.marketing.mapper;

import com.wxscrmplus.marketing.domain.MarCategory;
import com.wxscrmplus.marketing.domain.vo.MarCategoryVo;
import com.wxscrmplus.common.core.mapper.BaseMapperPlus;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * 分类管理Mapper接口
 *
// f5b484d108264beec5a7657ebd0c179c
 * @author 王永超
 * @date 2023-05-02
 */
public interface MarCategoryMapper extends BaseMapperPlus<MarCategoryMapper, MarCategory, MarCategoryVo> {
    /**
     * 查询函数名称以find打头的函数查询的数据包括删除的数据
     */

    /**
     * @param categoryId 分类ID
     * @return 分类名称
     */
    @Select("SELECT `name` FROM `mar_category` WHERE category_id = ${categoryId}")
    String findNameById(@Param("categoryId") Long categoryId);
}
