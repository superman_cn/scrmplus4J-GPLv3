package com.wxscrmplus.devtool.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wxscrmplus.common.constant.CacheNames;
import com.wxscrmplus.common.core.domain.PageQuery;
import com.wxscrmplus.common.core.domain.entity.DevDictData;
// 1329352cdbce22add4dfa7e5e127b58d
import com.wxscrmplus.common.core.page.TableDataInfo;
import com.wxscrmplus.common.exception.ServiceException;
import com.wxscrmplus.common.utils.StringUtils;
import com.wxscrmplus.common.utils.redis.CacheUtils;
import com.wxscrmplus.devtool.mapper.DevDictDataMapper;
import com.wxscrmplus.devtool.service.IDevDictDataService;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.CachePut;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 字典 业务层处理
 *
 * @author www.wxscrmplus.com
 */
@RequiredArgsConstructor
@Service
public class DevDictDataServiceImpl implements IDevDictDataService {

    private final DevDictDataMapper baseMapper;

    @Override
    public TableDataInfo<DevDictData> selectPageDictDataList(DevDictData dictData, PageQuery pageQuery) {
        LambdaQueryWrapper<DevDictData> lqw = new LambdaQueryWrapper<DevDictData>()
            .eq(StringUtils.isNotBlank(dictData.getDictType()), DevDictData::getDictType, dictData.getDictType())
            .like(StringUtils.isNotBlank(dictData.getDictLabel()), DevDictData::getDictLabel, dictData.getDictLabel())
            .eq(StringUtils.isNotBlank(dictData.getStatus()), DevDictData::getStatus, dictData.getStatus())
            .orderByAsc(DevDictData::getDictSort, DevDictData::getCreateTime);
        Page<DevDictData> page = baseMapper.selectPage(pageQuery.build(), lqw);
        return TableDataInfo.build(page);
    }

    /**
     * 根据条件分页查询字典数据
     *
     * @param dictData 字典数据信息
     * @return 字典数据集合信息
     */
    @Override
    public List<DevDictData> selectDictDataList(DevDictData dictData) {
        return baseMapper.selectList(new LambdaQueryWrapper<DevDictData>()
            .eq(StringUtils.isNotBlank(dictData.getDictType()), DevDictData::getDictType, dictData.getDictType())
            .like(StringUtils.isNotBlank(dictData.getDictLabel()), DevDictData::getDictLabel, dictData.getDictLabel())
            .eq(StringUtils.isNotBlank(dictData.getStatus()), DevDictData::getStatus, dictData.getStatus())
            .orderByAsc(DevDictData::getDictSort, DevDictData::getCreateTime));
    }

    /**
     * 根据字典类型和字典键值查询字典数据信息
     *
     * @param dictType  字典类型
     * @param dictValue 字典键值
     * @return 字典标签
     */
    @Override
    public String selectDictLabel(String dictType, String dictValue) {
        return baseMapper.selectOne(new LambdaQueryWrapper<DevDictData>()
                .select(DevDictData::getDictLabel)
                .eq(DevDictData::getDictType, dictType)
                .eq(DevDictData::getDictValue, dictValue))
            .getDictLabel();
    }

    /**
     * 根据字典数据ID查询信息
     *
     * @param dictCode 字典数据ID
     * @return 字典数据
     */
    @Override
    public DevDictData selectDictDataById(Long dictCode) {
        return baseMapper.selectById(dictCode);
    }

    /**
     * 批量删除字典数据信息
     *
     * @param dictCodes 需要删除的字典数据ID
     */
    @Override
    public void deleteDictDataByIds(Long[] dictCodes) {
        for (Long dictCode : dictCodes) {
            DevDictData data = selectDictDataById(dictCode);
            baseMapper.deleteById(dictCode);
            CacheUtils.evict(CacheNames.SYS_DICT, data.getDictType());
        }
    }

    /**
     * 新增保存字典数据信息
     *
     * @param data 字典数据信息
     * @return 结果
     */
    @CachePut(cacheNames = CacheNames.SYS_DICT, key = "#data.dictType")
    @Override
    public List<DevDictData> insertDictData(DevDictData data) {
        DevDictData devDictData = baseMapper.selectOne(Wrappers.query(new DevDictData()).lambda().eq(DevDictData::getDictType, data.getDictType()).eq(DevDictData::getDictValue, data.getDictValue()));
        if (ObjectUtil.isNotNull(devDictData)) {
            throw new ServiceException(String.format("数据键值（%s）已被（%s）占用！", data.getDictValue(), devDictData.getDictLabel()));
        }
        int row = baseMapper.insert(data);
        if (row > 0) {
            return baseMapper.selectDictDataByType(data.getDictType());
        }
        throw new ServiceException("操作失败");
    }

    /**
     * 修改保存字典数据信息
     *
     * @param data 字典数据信息
     * @return 结果
     */
    @CachePut(cacheNames = CacheNames.SYS_DICT, key = "#data.dictType")
    @Override
    public List<DevDictData> updateDictData(DevDictData data) {
        DevDictData devDictData = baseMapper.selectOne(Wrappers.query(new DevDictData()).lambda().eq(DevDictData::getDictType, data.getDictType()).eq(DevDictData::getDictValue, data.getDictValue()));
        if (ObjectUtil.isNotNull(devDictData) && !data.getDictCode().equals(devDictData.getDictCode())) {
            throw new ServiceException(String.format("数据键值（%s）已被（%s）占用！", data.getDictValue(), devDictData.getDictLabel()));
        }
        int row = baseMapper.updateById(data);
        if (row > 0) {
            return baseMapper.selectDictDataByType(data.getDictType());
        }
        throw new ServiceException("操作失败");
    }

}
