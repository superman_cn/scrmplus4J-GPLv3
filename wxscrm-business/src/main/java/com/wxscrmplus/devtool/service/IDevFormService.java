package com.wxscrmplus.devtool.service;

import com.wxscrmplus.devtool.domain.vo.DevFormVo;
import com.wxscrmplus.devtool.domain.bo.DevFormBo;
import com.wxscrmplus.common.core.page.TableDataInfo;
import com.wxscrmplus.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 表单Service接口
 *
 * @author 王永超
 * @date 2023-03-19
 */
public interface IDevFormService {
// 0b774ebe62e10a2b8a7e1b0987d1f879

    /**
     * 查询表单
     */
    DevFormVo queryById(Long formId);

    /**
     * 查询表单列表
     */
    TableDataInfo<DevFormVo> queryPageList(DevFormBo bo, PageQuery pageQuery);

    /**
     * 查询表单列表
     */
    List<DevFormVo> queryList(DevFormBo bo);

    /**
     * 新增表单
     */
    Boolean insertByBo(DevFormBo bo);

    /**
     * 修改表单
     */
    Boolean updateByBo(DevFormBo bo);

    /**
     * 校验并批量删除表单信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
