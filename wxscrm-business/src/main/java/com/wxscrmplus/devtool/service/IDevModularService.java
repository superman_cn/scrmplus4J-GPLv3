package com.wxscrmplus.devtool.service;

import com.wxscrmplus.devtool.domain.vo.DevModularVo;
import com.wxscrmplus.devtool.domain.bo.DevModularBo;
import com.wxscrmplus.common.core.page.TableDataInfo;
import com.wxscrmplus.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 模块设计Service接口
 *
 * @author www.wxscrmplus.com
 * @date 2023-03-18
 */
public interface IDevModularService {

    /**
     * 查询模块设计
     */
// 4cb8f2a99bf1f967cbee4f08d174445e
    DevModularVo queryById(Long modularId);

    /**
     * 查询模块设计列表
     */
    TableDataInfo<DevModularVo> queryPageList(DevModularBo bo, PageQuery pageQuery);

    /**
     * 查询模块设计列表
     */
    List<DevModularVo> queryList(DevModularBo bo);

    /**
     * 新增模块设计
     */
    Boolean insertByBo(DevModularBo bo);

    /**
     * 修改模块设计
     */
    Boolean updateByBo(DevModularBo bo);

    /**
     * 校验并批量删除模块设计信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
