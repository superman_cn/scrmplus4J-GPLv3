package com.wxscrmplus.devtool.service;

import com.wxscrmplus.common.core.domain.PageQuery;
import com.wxscrmplus.common.core.domain.entity.DevDictData;
import com.wxscrmplus.common.core.page.TableDataInfo;

import java.util.List;

/**
 * 字典 业务层
 *
 * @author www.wxscrmplus.com
 */
public interface IDevDictDataService {


    TableDataInfo<DevDictData> selectPageDictDataList(DevDictData dictData, PageQuery pageQuery);

    /**
     * 根据条件分页查询字典数据
     *
     * @param dictData 字典数据信息
     * @return 字典数据集合信息
     */
    List<DevDictData> selectDictDataList(DevDictData dictData);

    /**
     * 根据字典类型和字典键值查询字典数据信息
     *
     * @param dictType  字典类型
     * @param dictValue 字典键值
     * @return 字典标签
     */
    String selectDictLabel(String dictType, String dictValue);

    /**
     * 根据字典数据ID查询信息
     *
     * @param dictCode 字典数据ID
     * @return 字典数据
     */
    DevDictData selectDictDataById(Long dictCode);

    /**
     * 批量删除字典数据信息
     *
     * @param dictCodes 需要删除的字典数据ID
// 5064aa78610a3a657873c442e4d85e2c
     */
    void deleteDictDataByIds(Long[] dictCodes);

    /**
     * 新增保存字典数据信息
     *
     * @param dictData 字典数据信息
     * @return 结果
     */
    List<DevDictData> insertDictData(DevDictData dictData);

    /**
     * 修改保存字典数据信息
     *
     * @param dictData 字典数据信息
     * @return 结果
     */
    List<DevDictData> updateDictData(DevDictData dictData);
}
