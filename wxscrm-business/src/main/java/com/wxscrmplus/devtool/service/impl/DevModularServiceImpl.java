package com.wxscrmplus.devtool.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.wxscrmplus.common.core.domain.entity.DevDictType;
import com.wxscrmplus.common.exception.ServiceException;
import com.wxscrmplus.common.utils.StringUtils;
import com.wxscrmplus.common.core.page.TableDataInfo;
import com.wxscrmplus.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.wxscrmplus.devtool.domain.GenTable;
import com.wxscrmplus.devtool.mapper.DevDictTypeMapper;
import com.wxscrmplus.devtool.mapper.DevGenTableMapper;
import com.wxscrmplus.devtool.domain.DevModular;
import com.wxscrmplus.devtool.domain.bo.DevModularBo;
import com.wxscrmplus.devtool.domain.vo.DevModularVo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.wxscrmplus.devtool.mapper.DevModularMapper;
import com.wxscrmplus.devtool.service.IDevModularService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 模块设计Service业务层处理
 *
 * @author www.wxscrmplus.com
 * @date 2023-03-18
 */
@RequiredArgsConstructor
@Service
public class DevModularServiceImpl implements IDevModularService {

    private final DevModularMapper baseMapper;
    private final DevDictTypeMapper devDictTypeMapper;
    private final DevGenTableMapper genTableMapper;

    /**
     * 查询模块设计
     */
    @Override
    public DevModularVo queryById(Long modularId) {
        return baseMapper.selectVoById(modularId);
    }

    /**
     * 查询模块设计列表
     */
    @Override
    public TableDataInfo<DevModularVo> queryPageList(DevModularBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<DevModular> lqw = buildQueryWrapper(bo);
        Page<DevModularVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询模块设计列表
     */
    @Override
    public List<DevModularVo> queryList(DevModularBo bo) {
        LambdaQueryWrapper<DevModular> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<DevModular> buildQueryWrapper(DevModularBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<DevModular> lqw = Wrappers.lambdaQuery();
        lqw.like(StringUtils.isNotBlank(bo.getName()), DevModular::getName, bo.getName());
        lqw.like(StringUtils.isNotBlank(bo.getCode()), DevModular::getCode, bo.getCode());
        return lqw;
    }

    /**
     * 新增模块设计
     */
    @Override
    public Boolean insertByBo(DevModularBo bo) {
        DevModular add = BeanUtil.toBean(bo, DevModular.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setModularId(add.getModularId());
        }
        return flag;
    }

    /**
     * 修改模块设计
     */
    @Override
    public Boolean updateByBo(DevModularBo bo) {
        DevModular update = BeanUtil.toBean(bo, DevModular.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(DevModular entity) {
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除模块设计
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if (isValid) {
            for (Long id : ids) {
                Long dictCount = devDictTypeMapper.selectCount(Wrappers.query(new DevDictType()).lambda().eq(DevDictType::getModularId, id));
                if (dictCount > 0) {
                    DevModularVo devModularVo = baseMapper.selectVoById(id);
                    throw new ServiceException(devModularVo.getName() + "已关联数据字典无法删除！");
                }
                Long genTableCount = genTableMapper.selectCount(Wrappers.query(new GenTable()).lambda().eq(GenTable::getModularId, id));
                if (genTableCount > 0) {
                    DevModularVo devModularVo = baseMapper.selectVoById(id);
                    throw new ServiceException(devModularVo.getName() + "已关联表无法删除！");
                }
// 2d25046871461bd9218dda3895033ad5
            }
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
