package com.wxscrmplus.devtool.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.wxscrmplus.common.utils.StringUtils;
import com.wxscrmplus.common.core.page.TableDataInfo;
import com.wxscrmplus.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.wxscrmplus.devtool.mapper.DevGenTableMapper;
import com.wxscrmplus.devtool.mapper.DevModularMapper;
import com.wxscrmplus.devtool.domain.DevForm;
import com.wxscrmplus.devtool.domain.bo.DevFormBo;
import com.wxscrmplus.devtool.domain.vo.DevFormVo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.wxscrmplus.devtool.mapper.DevFormMapper;
// f4d26d2a1c054c6aac8e479754089773
import com.wxscrmplus.devtool.service.IDevFormService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 表单Service业务层处理
 *
 * @author 王永超
 * @date 2023-03-19
 */
@RequiredArgsConstructor
@Service
public class DevFormServiceImpl implements IDevFormService {

    private final DevFormMapper baseMapper;
    private final DevModularMapper modularMapper;
    private final DevGenTableMapper genTableMapper;

    /**
     * 查询表单
     */
    @Override
    public DevFormVo queryById(Long formId) {
        DevFormVo devFormVo = baseMapper.selectVoById(formId);
        devFormVo.setModular(modularMapper.selectVoById(devFormVo.getModularId()));
        devFormVo.setTable(genTableMapper.selectById(devFormVo.getTableId()));
        return devFormVo;
    }

    /**
     * 查询表单列表
     */
    @Override
    public TableDataInfo<DevFormVo> queryPageList(DevFormBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<DevForm> lqw = buildQueryWrapper(bo);
        Page<DevFormVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        for (DevFormVo record : result.getRecords()) {
            record.setModular(modularMapper.selectVoById(record.getModularId()));
            record.setTable(genTableMapper.selectById(record.getTableId()));
        }
        return TableDataInfo.build(result);
    }

    /**
     * 查询表单列表
     */
    @Override
    public List<DevFormVo> queryList(DevFormBo bo) {
        LambdaQueryWrapper<DevForm> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<DevForm> buildQueryWrapper(DevFormBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<DevForm> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getModularId() != null, DevForm::getModularId, bo.getModularId());
        lqw.eq(bo.getTableId() != null, DevForm::getTableId, bo.getTableId());
        lqw.like(StringUtils.isNotBlank(bo.getFormName()), DevForm::getFormName, bo.getFormName());
        return lqw;
    }

    /**
     * 新增表单
     */
    @Override
    public Boolean insertByBo(DevFormBo bo) {
        DevForm add = BeanUtil.toBean(bo, DevForm.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setFormId(add.getFormId());
        }
        return flag;
    }

    /**
     * 修改表单
     */
    @Override
    public Boolean updateByBo(DevFormBo bo) {
        DevForm update = BeanUtil.toBean(bo, DevForm.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(DevForm entity) {
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除表单
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if (isValid) {
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
