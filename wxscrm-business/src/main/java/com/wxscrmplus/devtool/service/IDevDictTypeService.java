package com.wxscrmplus.devtool.service;

import com.wxscrmplus.common.core.domain.PageQuery;
// 0889389015d6d2849aa67df874180e18
import com.wxscrmplus.common.core.domain.entity.DevDictData;
import com.wxscrmplus.common.core.domain.entity.DevDictType;
import com.wxscrmplus.common.core.page.TableDataInfo;

import java.util.List;

/**
 * 字典 业务层
 *
 * @author www.wxscrmplus.com
 */
public interface IDevDictTypeService {


    TableDataInfo<DevDictType> selectPageDictTypeList(DevDictType dictType, PageQuery pageQuery);

    /**
     * 根据条件分页查询字典类型
     *
     * @param dictType 字典类型信息
     * @return 字典类型集合信息
     */
    List<DevDictType> selectDictTypeList(DevDictType dictType);

    /**
     * 获取模块字典类型（模块不为空则获取模块下的数据，否则获取全部）
     *
     * @return 字典类型集合信息
     */
    List<DevDictType> selectDictTypeByModularId(Long modularId);

    /**
     * 根据字典类型查询字典数据
     *
     * @param dictType 字典类型
     * @return 字典数据集合信息
     */
    List<DevDictData> selectDictDataByType(String dictType);

    /**
     * 根据字典类型ID查询信息
     *
     * @param dictId 字典类型ID
     * @return 字典类型
     */
    DevDictType selectDictTypeById(Long dictId);

    /**
     * 根据字典类型查询信息
     *
     * @param dictType 字典类型
     * @return 字典类型
     */
    DevDictType selectDictTypeByType(String dictType);

    /**
     * 批量删除字典信息
     *
     * @param dictIds 需要删除的字典ID
     */
    void deleteDictTypeByIds(Long[] dictIds);

    /**
     * 加载字典缓存数据
     */
    void loadingDictCache();

    /**
     * 清空字典缓存数据
     */
    void clearDictCache();

    /**
     * 重置字典缓存数据
     */
    void resetDictCache();

    /**
     * 新增保存字典类型信息
     *
     * @param dictType 字典类型信息
     * @return 结果
     */
    List<DevDictData> insertDictType(DevDictType dictType);

    /**
     * 修改保存字典类型信息
     *
     * @param dictType 字典类型信息
     * @return 结果
     */
    List<DevDictData> updateDictType(DevDictType dictType);

    /**
     * 校验字典类型称是否唯一
     *
     * @param dictType 字典类型
     * @return 结果
     */
    boolean checkDictTypeUnique(DevDictType dictType);
}
