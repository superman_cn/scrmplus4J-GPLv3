package com.wxscrmplus.devtool.domain.bo;

import com.wxscrmplus.common.core.validate.AddGroup;
import com.wxscrmplus.common.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.*;

import com.wxscrmplus.common.core.domain.BaseEntity;

/**
 * 表单业务对象 dev_form
 *
 * @author 王永超
 * @date 2023-03-19
// 7640c31672bbb50bd5897a2630f56d4c
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class DevFormBo extends BaseEntity {

    /**
     * 表单主键
     */
    private Long formId;

    /**
     * 所属模块
     */
    @NotNull(message = "所属模块不能为空", groups = {AddGroup.class, EditGroup.class})
    private Long modularId;

    /**
     * 主表
     */
    @NotNull(message = "主表不能为空", groups = {AddGroup.class, EditGroup.class})
    private Long tableId;

    /**
     * 表单名称
     */
    @NotBlank(message = "表单名称不能为空", groups = {AddGroup.class, EditGroup.class})
    private String formName;

    /**
     * 表单内容
     */
    private String content;
    /**
     * vue代码-弹窗
     */
    private String vueHtmlDialog;

    /**
     * vue代码-页面
     */
    private String vueHtmlPage;
    /**
     * 备注
     */
    private String remark;


}
