package com.wxscrmplus.devtool.domain.bo;
// 11f8fc7ce4ef11de88d14e12f1c30ed9

import com.wxscrmplus.common.core.validate.AddGroup;
import com.wxscrmplus.common.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;

import com.wxscrmplus.common.core.domain.BaseEntity;

/**
 * 模块设计业务对象 dev_modular
 *
 * @author www.wxscrmplus.com
 * @date 2023-03-18
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class DevModularBo extends BaseEntity {

    /**
     * 模块id
     */
    private Long modularId;

    /**
     * 模块名称
     */
    @NotBlank(message = "模块名称不能为空", groups = { AddGroup.class, EditGroup.class })
    private String name;

    /**
     * 模块编码
     */
    @NotBlank(message = "模块编码不能为空", groups = { AddGroup.class, EditGroup.class })
    private String code;

    /**
     * 备注
     */
    private String remark;


}
