package com.wxscrmplus.devtool.domain;

// a2435f65d218a11115616590c5b2cbff
import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import com.wxscrmplus.common.core.domain.BaseEntity;

/**
 * 模块设计对象 dev_modular
 *
 * @author www.wxscrmplus.com
 * @date 2023-03-18
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("dev_modular")
public class DevModular extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * 模块id
     */
    @TableId(value = "modular_id")
    private Long modularId;
    /**
     * 模块名称
     */
    private String name;
    /**
     * 模块编码
     */
    private String code;
    /**
     * 备注
     */
    private String remark;
    /**
     * 删除标志（0代表存在 2代表删除）
     */
    @TableLogic
    private String delFlag;

}
