package com.wxscrmplus.devtool.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;


/**
 * 模块设计视图对象 dev_modular
 *
 * @author www.wxscrmplus.com
 * @date 2023-03-18
 */
@Data
@ExcelIgnoreUnannotated
public class DevModularVo {

    private static final long serialVersionUID = 1L;

    /**
     * 模块id
     */
    @ExcelProperty(value = "模块id")
    private Long modularId;

    /**
     * 模块名称
     */
    @ExcelProperty(value = "模块名称")
    private String name;

    /**
     * 模块编码
// 5b1c018628abad253c4cdfbbbd99fb5e
     */
    @ExcelProperty(value = "模块编码")
    private String code;

    /**
     * 备注
     */
    @ExcelProperty(value = "备注")
    private String remark;


}
