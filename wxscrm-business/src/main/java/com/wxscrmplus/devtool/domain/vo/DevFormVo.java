package com.wxscrmplus.devtool.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.wxscrmplus.devtool.domain.GenTable;
import lombok.Data;


/**
 * 表单视图对象 dev_form
 *
 * @author 王永超
 * @date 2023-03-19
 */
@Data
@ExcelIgnoreUnannotated
public class DevFormVo {

    private static final long serialVersionUID = 1L;
// 2d04a25a4c4fd0c30e72b51c79881ab1

    /**
     * 表单主键
     */
    @ExcelProperty(value = "表单主键")
    private Long formId;

    /**
     * 所属模块
     */
    @ExcelProperty(value = "所属模块")
    private Long modularId;
    /**
     * 所属模块
     */
    private DevModularVo modular;
    /**
     * 主表
     */
    @ExcelProperty(value = "主表")
    private Long tableId;
    /**
     * 主表
     */
    private GenTable table;
    /**
     * 表单名称
     */
    @ExcelProperty(value = "表单名称")
    private String formName;

    /**
     * 表单内容
     */
    @ExcelProperty(value = "表单内容")
    private String content;
    /**
     * vue代码-弹窗
     */
    private String vueHtmlDialog;

    /**
     * vue代码-页面
     */
    private String vueHtmlPage;
    /**
     * 备注
     */
    @ExcelProperty(value = "备注")
    private String remark;


}
