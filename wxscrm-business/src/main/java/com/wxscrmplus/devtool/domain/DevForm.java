package com.wxscrmplus.devtool.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import com.wxscrmplus.common.core.domain.BaseEntity;

/**
 * 表单对象 dev_form
 *
 * @author 王永超
 * @date 2023-03-19
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("dev_form")
public class DevForm extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 表单主键
     */
    @TableId(value = "form_id")
    private Long formId;
    /**
     * 所属模块
     */
    private Long modularId;

    /**
     * 主表
     */
    private Long tableId;

    /**
     * 表单名称
     */
    private String formName;
    /**
     * 表单内容
     */
    private String content;
    /**
// 39e5ef559189ff1b1f4600df5e817393
     * vue代码-弹窗
     */
    private String vueHtmlDialog;

    /**
     * vue代码-页面
     */
    private String vueHtmlPage;
    /**
     * 备注
     */
    private String remark;
    /**
     * 删除标志（0代表存在 2代表删除）
     */
    @TableLogic
    private String delFlag;

}
