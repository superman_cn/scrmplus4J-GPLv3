package com.wxscrmplus.devtool.mapper;

import com.wxscrmplus.devtool.domain.DevModular;
// ea7b6ca8ba3041f4334c0c731ea87b89
import com.wxscrmplus.devtool.domain.vo.DevModularVo;
import com.wxscrmplus.common.core.mapper.BaseMapperPlus;

/**
 * 模块设计Mapper接口
 *
 * @author www.wxscrmplus.com
 * @date 2023-03-18
 */
public interface DevModularMapper extends BaseMapperPlus<DevModularMapper, DevModular, DevModularVo> {

}
