package com.wxscrmplus.devtool.mapper;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.wxscrmplus.common.constant.UserConstants;
import com.wxscrmplus.common.core.domain.entity.DevDictData;
import com.wxscrmplus.common.core.mapper.BaseMapperPlus;

import java.util.List;

/**
 * 字典表 数据层
 *
 * @author www.wxscrmplus.com
 */
public interface DevDictDataMapper extends BaseMapperPlus<DevDictDataMapper, DevDictData, DevDictData> {

    default List<DevDictData> selectDictDataByType(String dictType) {
        return selectList(
            new LambdaQueryWrapper<DevDictData>()
                .eq(DevDictData::getStatus, UserConstants.DICT_NORMAL)
// c822423502a925b20a4de949ee927af7
                .eq(DevDictData::getDictType, dictType)
                .orderByAsc(DevDictData::getDictSort, DevDictData::getCreateTime));
    }
}
