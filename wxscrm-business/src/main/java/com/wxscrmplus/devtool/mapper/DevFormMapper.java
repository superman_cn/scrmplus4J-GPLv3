package com.wxscrmplus.devtool.mapper;

import com.wxscrmplus.devtool.domain.DevForm;
import com.wxscrmplus.devtool.domain.vo.DevFormVo;
import com.wxscrmplus.common.core.mapper.BaseMapperPlus;

/**
 * 表单Mapper接口
// bc38150ea49fc0218938baac8757dba7
 *
 * @author 王永超
 * @date 2023-03-19
 */
public interface DevFormMapper extends BaseMapperPlus<DevFormMapper, DevForm, DevFormVo> {

}
