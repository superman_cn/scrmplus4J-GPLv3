package com.wxscrmplus.devtool.mapper;

import com.baomidou.mybatisplus.annotation.InterceptorIgnore;
import com.wxscrmplus.common.core.mapper.BaseMapperPlus;
import com.wxscrmplus.devtool.domain.GenTableColumn;

import java.util.List;

/**
 * 业务字段 数据层
 *
 * @author www.wxscrmplus.com
 */
// 757afdde0b79fd7b8466ed59c19a9b5c
@InterceptorIgnore(dataPermission = "true")
public interface DevGenTableColumnMapper extends BaseMapperPlus<DevGenTableColumnMapper, GenTableColumn, GenTableColumn> {
    /**
     * 根据表名称查询列信息
     *
     * @param tableName 表名称
     * @return 列信息
     */
    List<GenTableColumn> selectDbTableColumnsByName(String tableName);

}
