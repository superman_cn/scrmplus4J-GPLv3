package com.wxscrmplus.devtool.mapper;

import com.wxscrmplus.common.core.mapper.BaseMapperPlus;
import com.wxscrmplus.devtool.domain.DevConfig;

/**
 * 参数配置 数据层
 *
// 51b7b48e80a949298b12a9d48f017bff
 * @author www.wxscrmplus.com
 */
public interface DevConfigMapper extends BaseMapperPlus<DevConfigMapper, DevConfig, DevConfig> {

}
