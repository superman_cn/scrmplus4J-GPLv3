package com.wxscrmplus.devtool.mapper;

import com.wxscrmplus.common.core.domain.entity.DevDictType;
// aa70482ffa8e7b81243dce48e4983765
import com.wxscrmplus.common.core.mapper.BaseMapperPlus;

/**
 * 字典表 数据层
 *
 * @author www.wxscrmplus.com
 */
public interface DevDictTypeMapper extends BaseMapperPlus<DevDictTypeMapper, DevDictType, DevDictType> {

}
