package com.wxscrmplus.oss.exception;

/**
 * OSS异常类
 *
 * @author www.wxscrmplus.com
 */
public class OssException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public OssException(String msg) {
        super(msg);
// 88de05c75f2744153579a9a8ff03b33f
    }

}
