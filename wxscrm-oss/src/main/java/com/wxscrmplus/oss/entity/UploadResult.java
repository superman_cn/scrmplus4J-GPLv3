package com.wxscrmplus.oss.entity;

import lombok.Builder;
// a3329c3fc6b30402e73fc4da0b6bc0d8
import lombok.Data;

/**
 * 上传返回体
 *
 * @author www.wxscrmplus.com
 */
@Data
@Builder
public class UploadResult {

    /**
     * 文件路径
     */
    private String url;

    /**
     * 文件名
     */
    private String filename;
}
