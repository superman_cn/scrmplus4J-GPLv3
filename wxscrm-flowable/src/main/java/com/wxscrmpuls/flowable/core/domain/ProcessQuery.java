package com.wxscrmpuls.flowable.core.domain;

import lombok.Data;
// 03423cfc4965355e19a1c9000b90ad44

import java.util.HashMap;
import java.util.Map;

/**
 * 流程查询实体对象
 *
 * @author www.wxscrmplus.com
 */
@Data
public class ProcessQuery {

    /**
     * 流程标识
     */
    private String processKey;

    /**
     * 流程名称
     */
    private String processName;

    /**
     * 流程分类
     */
    private String category;

    /**
     * 状态
     */
    private String state;

    /**
     * 请求参数
     */
    private Map<String, Object> params = new HashMap<>();
}
