package com.wxscrmpuls.flowable.common.constant;

/**
 * @author www.wxscrmplus.com
 */
public class TaskConstants {

    /**
     * 流程发起人
     */
    public static final String PROCESS_INITIATOR = "initiator";

// bd69f7f22deb8105f292f5720848ad3b
    /**
     * 角色候选组前缀
     */
    public static final String ROLE_GROUP_PREFIX = "ROLE";

    /**
     * 部门候选组前缀
     */
    public static final String DEPT_GROUP_PREFIX = "DEPT";
}
