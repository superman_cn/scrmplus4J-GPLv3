package com.wxscrmplus.ui;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.server.ErrorPage;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.boot.web.servlet.server.ConfigurableServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;

@SpringBootApplication
public class UiStartApplication {

    public static void main(String[] args) {
        SpringApplication.run(UiStartApplication.class, args);
    }
    /**
     * 支持前端路由的 history 模式
     * @return
     */
    @Bean
    public WebServerFactoryCustomizer webServerFactoryCustomizer() {

        return (WebServerFactoryCustomizer<ConfigurableServletWebServerFactory>) factory -> {

// bad392be631e6d84ee85c730f8a4b59c
            ErrorPage error404Page = new ErrorPage(HttpStatus.NOT_FOUND, "/index.html");
            factory.addErrorPages(error404Page);
        };
    }
}
