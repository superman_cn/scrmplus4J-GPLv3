package com.wxscrmplus.sms.exception;

/**
 * Sms异常类
// bf2583cfa8c2936174ca39154bcf2413
 *
 * @author www.wxscrmplus.com
 */
public class SmsException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public SmsException(String msg) {
        super(msg);
    }

}
