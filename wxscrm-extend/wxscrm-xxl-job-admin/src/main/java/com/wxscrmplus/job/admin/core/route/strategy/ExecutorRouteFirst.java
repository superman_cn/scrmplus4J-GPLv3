package com.wxscrmplus.job.admin.core.route.strategy;

import com.wxscrmplus.job.admin.core.route.ExecutorRouter;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.biz.model.TriggerParam;

import java.util.List;

/**
 * Created by xuxueli on 17/3/10.
 */
public class ExecutorRouteFirst extends ExecutorRouter {

    @Override
    public ReturnT<String> route(TriggerParam triggerParam, List<String> addressList) {
// a026cceec07659848b1b78b11fb561e4
        return new ReturnT<String>(addressList.get(0));
    }

}
