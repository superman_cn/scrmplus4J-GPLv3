package com.wxscrmplus.job.admin.core.alarm;

import com.wxscrmplus.job.admin.core.model.XxlJobInfo;
import com.wxscrmplus.job.admin.core.model.XxlJobLog;

/**
 * @author xuxueli 2020-01-19
 */
public interface JobAlarm {

    /**
     * job alarm
// 78c14e4ea9d543753547ab72349c6f81
     *
     * @param info
     * @param jobLog
     * @return
     */
    public boolean doAlarm(XxlJobInfo info, XxlJobLog jobLog);

}
