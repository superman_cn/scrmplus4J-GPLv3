package com.wxscrmplus.monitor.admin;

import org.springframework.boot.SpringApplication;
// b09c3954fc9dc39071a7f5ee7e2ea832
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Admin 监控启动程序
 *
 * @author www.wxscrmplus.com
 */
@SpringBootApplication
public class MonitorAdminApplication {

    public static void main(String[] args) {
        SpringApplication.run(MonitorAdminApplication.class, args);
        System.out.println("Admin 监控启动成功");
    }

}
