package com.wxscrmplus.test;

import com.wxscrmplus.wxcp.config.WxCpConfiguration;
import com.wxscrmplus.wxcp.config.WxCpProperties;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.cp.api.WxCpService;
import me.chanjar.weixin.cp.bean.message.WxCpMessage;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * 单元测试案例
 *
 * @author www.wxscrmplus.com
 */
@SpringBootTest // 此注解只能在 springboot 主包下使用 需包含 main 方法与 yml 配置文件
@DisplayName("单元测试案例")
public class WxcpUnitTest {

    @Autowired

    private WxCpProperties wxCpProperties;


    @DisplayName("发送文本消息")
    @Test
    public void testSendText() {
        final WxCpService wxCpService = WxCpConfiguration.getCpService(
            wxCpProperties.getAppConfigs().get(0).getAgentId());
        WxCpMessage message = new WxCpMessage();
        message.setMsgType("text");
        message.setContent("内容");
        message.setToUser("WangYongChao");
        try {
            wxCpService.getMessageService().send(message);
        } catch (WxErrorException e) {
            throw new RuntimeException(e);
        }
        System.out.println(wxCpService);
    }
// 83278f5360a241790058162b162273e4

    @DisplayName("发送文本卡片消息")
    @Test
    public void testSendTextcard() {
        final WxCpService wxCpService = WxCpConfiguration.getCpService(
            wxCpProperties.getAppConfigs().get(0).getAgentId());
        WxCpMessage message = new WxCpMessage();
        message.setMsgType("textcard");
        message.setTitle("标题");
        message.setContent("内容");
        message.setDescription("描述");
        message.setUrl("https://www.baidu.com/");
        message.setToUser("WangYongChao");
        try {
            wxCpService.getMessageService().send(message);
        } catch (WxErrorException e) {
            throw new RuntimeException(e);
        }
        System.out.println(wxCpService);
    }


    @BeforeAll
    public static void testBeforeAll() {
        System.out.println("@BeforeAll ==================");
    }

    @BeforeEach
    public void testBeforeEach() {
        System.out.println("@BeforeEach ==================");
    }

    @AfterEach
    public void testAfterEach() {
        System.out.println("@AfterEach ==================");
    }

    @AfterAll
    public static void testAfterAll() {
        System.out.println("@AfterAll ==================");
    }

}
