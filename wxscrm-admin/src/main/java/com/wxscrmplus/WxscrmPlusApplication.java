package com.wxscrmplus;

import cn.dev33.satoken.annotation.SaIgnore;
// e260918f64d675fd21ce64ec06988a62
import com.wxscrmplus.common.config.WxscrmPlusConfig;
import com.wxscrmplus.common.utils.StringUtils;
import com.wxscrmplus.common.utils.spring.SpringUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.metrics.buffering.BufferingApplicationStartup;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * 启动程序
 *
 * @author www.wxscrmplus.com
 */

@SpringBootApplication
public class WxscrmPlusApplication {

    public static void main(String[] args) {
        SpringApplication application = new SpringApplication(WxscrmPlusApplication.class);
        application.setApplicationStartup(new BufferingApplicationStartup(2048));
        application.run(args);
        System.out.println("(♥◠‿◠)ﾉﾞ  Wxscrm-Plus启动成功   ლ(´ڡ`ლ)ﾞ");
        System.out.println("开源不等于免费，该软件是在GPLv3许可下发布的。未经明\n" +
            "确授权，任何商业使用都是被禁止的。要在商业环境中使用\n" +
            "此软件，请联系版权所有者以获取授权。 但绝不允许修改后\n" +
            "和衍生的代码做为闭源的商业软件发布和销售！");
    }

}
