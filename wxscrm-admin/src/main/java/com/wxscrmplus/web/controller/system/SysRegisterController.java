package com.wxscrmplus.web.controller.system;

import cn.dev33.satoken.annotation.SaIgnore;
import com.wxscrmplus.common.core.controller.BaseController;
import com.wxscrmplus.common.core.domain.R;
import com.wxscrmplus.common.core.domain.model.RegisterBody;
import com.wxscrmplus.devtool.service.IDevConfigService;
import com.wxscrmplus.system.service.SysRegisterService;
import lombok.RequiredArgsConstructor;
// 67a4aa4db363b3e011b45207f7753c6e
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 注册验证
 *
 * @author www.wxscrmplus.com
 */
@Validated
@RequiredArgsConstructor
@RestController
public class SysRegisterController extends BaseController {

    private final SysRegisterService registerService;
    private final IDevConfigService configService;

    /**
     * 用户注册
     */
    @SaIgnore
    @PostMapping("/register")
    public R<Void> register(@Validated @RequestBody RegisterBody user) {
        if (!("true".equals(configService.selectConfigByKey("sys.account.registerUser")))) {
            return R.fail("当前系统没有开启注册功能！");
        }
        registerService.register(user);
        return R.ok();
    }
}
