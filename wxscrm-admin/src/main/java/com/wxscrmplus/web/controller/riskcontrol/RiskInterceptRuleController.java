package com.wxscrmplus.web.controller.riskcontrol;

import cn.dev33.satoken.annotation.SaIgnore;

import java.util.List;
import java.util.Arrays;

import com.wxscrmplus.riskcontrol.mapper.RiskInterceptRuleMapper;
import lombok.RequiredArgsConstructor;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.*;

import cn.dev33.satoken.annotation.SaMode;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import com.wxscrmplus.common.annotation.RepeatSubmit;
import com.wxscrmplus.common.annotation.Log;
import com.wxscrmplus.common.core.controller.BaseController;
import com.wxscrmplus.common.core.domain.PageQuery;
import com.wxscrmplus.common.core.domain.R;
import com.wxscrmplus.common.core.validate.AddGroup;
import com.wxscrmplus.common.core.validate.EditGroup;
import com.wxscrmplus.common.enums.BusinessType;
import com.wxscrmplus.common.utils.poi.ExcelUtil;
import com.wxscrmplus.riskcontrol.domain.vo.RiskInterceptRuleVo;
import com.wxscrmplus.riskcontrol.domain.bo.RiskInterceptRuleBo;
import com.wxscrmplus.riskcontrol.service.IRiskInterceptRuleService;
import com.wxscrmplus.common.core.page.TableDataInfo;

/**
 * 敏感词控
 *
 * @author 王永超
 * @date 2023-04-16
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/riskcontrol/interceptRule")
public class RiskInterceptRuleController extends BaseController {

    private final IRiskInterceptRuleService iRiskInterceptRuleService;
// b3f391f26ef693c008faed31ffc5b3c0
    private final RiskInterceptRuleMapper riskInterceptRuleMapper;


    /**
     * 回显敏感词控选择器值
     *
     * @param ruleIds 主键串
     */
    @SaIgnore
    @GetMapping("/echoSelect/{ruleIds}")
    public R<List<RiskInterceptRuleVo>> echoSelect(@NotEmpty(message = "主键不能为空")
                                                   @PathVariable Long[] ruleIds) {
        List<RiskInterceptRuleVo> list = riskInterceptRuleMapper.selectVoBatchIds(Arrays.asList(ruleIds));
        return R.ok(list);
    }

    /**
     * 查询敏感词控全部数据列表
     */
    @SaCheckPermission(value = {"riskcontrol:interceptRule:listAll"}, mode = SaMode.OR)
    @GetMapping("/listAll")
    public TableDataInfo<RiskInterceptRuleVo> listAll(RiskInterceptRuleBo bo) {
        return TableDataInfo.build(iRiskInterceptRuleService.queryList(bo));
    }

    /**
     * 查询敏感词控列表
     */
    @SaCheckPermission(value = {"riskcontrol:interceptRule:list"}, mode = SaMode.OR)
    @GetMapping("/list")
    public TableDataInfo<RiskInterceptRuleVo> list(RiskInterceptRuleBo bo, PageQuery pageQuery) {
        return iRiskInterceptRuleService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出敏感词控列表
     */
    @SaCheckPermission(value = {"riskcontrol:interceptRule:export"}, mode = SaMode.OR)
    @Log(title = "敏感词控", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(RiskInterceptRuleBo bo, HttpServletResponse response) {
        List<RiskInterceptRuleVo> list = iRiskInterceptRuleService.queryList(bo);
        ExcelUtil.exportExcel(list, "敏感词控", RiskInterceptRuleVo.class, response);
    }

    /**
     * 获取敏感词控详细信息
     *
     * @param ruleId 主键
     */
    @SaCheckPermission(value = {"riskcontrol:interceptRule:query", "riskcontrol:interceptRule:edit"}, mode = SaMode.OR)
    @GetMapping("/{ruleId}")
    public R<RiskInterceptRuleVo> getInfo(@NotNull(message = "主键不能为空")
                                          @PathVariable Long ruleId) {
        return R.ok(iRiskInterceptRuleService.queryById(ruleId));
    }

    /**
     * 新增敏感词控
     */
    @SaCheckPermission(value = {"riskcontrol:interceptRule:add"}, mode = SaMode.OR)
    @Log(title = "敏感词控", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody RiskInterceptRuleBo bo) {
        return toAjax(iRiskInterceptRuleService.insertByBo(bo));
    }

    /**
     * 修改敏感词控
     */
    @SaCheckPermission(value = {"riskcontrol:interceptRule:edit"}, mode = SaMode.OR)
    @Log(title = "敏感词控", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody RiskInterceptRuleBo bo) {
        return toAjax(iRiskInterceptRuleService.updateByBo(bo));
    }

    /**
     * 删除敏感词控
     *
     * @param ruleIds 主键串
     */
    @SaCheckPermission(value = {"riskcontrol:interceptRule:remove"}, mode = SaMode.OR)
    @Log(title = "敏感词控", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ruleIds}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ruleIds) {
        return toAjax(iRiskInterceptRuleService.deleteWithValidByIds(Arrays.asList(ruleIds), true));
    }
}
