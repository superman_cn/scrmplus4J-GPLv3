package com.wxscrmplus.web.controller.system;

import cn.dev33.satoken.annotation.SaIgnore;
import com.wxscrmplus.common.config.WxscrmPlusConfig;
import com.wxscrmplus.common.core.controller.BaseController;
import com.wxscrmplus.common.core.domain.R;
import com.wxscrmplus.common.utils.StringUtils;
import com.wxscrmplus.common.utils.spring.SpringUtils;
import com.wxscrmplus.statistics.service.ICustomerStatsService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 首页
 *
 * @author www.wxscrmplus.com
 */
@RequiredArgsConstructor
@RestController
public class SysIndexController extends BaseController {

    private final ICustomerStatsService customerStatsService;

    /**
     * 访问首页，提示语
     */
    @SaIgnore
    @GetMapping("/")
    public String index() {
        WxscrmPlusConfig plusConfig = SpringUtils.getBean(WxscrmPlusConfig.class);
        return StringUtils.format("欢迎使用{}后台管理框架，当前版本：v{}，请通过前端地址访问。", plusConfig.getName(), plusConfig.getVersion());
    }

    /**
     * 卡片面板数据
     */
    @SaIgnore
    @GetMapping("/panelData")
    public R<Map<String, Object>> panelData() {
        Map<String, Object> map = new HashMap<>();
        //客户总数
        map.put("totalCustomer", customerStatsService.totalCustomer(getAllUserIds()));
        //流失客户数
        map.put("lossCustomer", customerStatsService.lossCustomer(getAllUserIds()));
        //商机成交数据
        map.put("totalOpportunity", customerStatsService.totalOpportunity(getAllUserIds()));
        //总利润
        map.put("totalProfit", customerStatsService.totalProfit(getAllUserIds()));
        return R.ok(map);
    }


    /**
     * 月份折线图数据
     */
    @SaIgnore
    @GetMapping("/lineChartLast12Month/{type}")
    public R<Map<String, Object>> lineChartLast12Month(@PathVariable(value = "type") String type) {
        Map<String, Object> map = new HashMap<>();
        List<Map<String, Object>> lineChartMonth;
        switch (type) {
            case "lossCustomer":
                lineChartMonth = customerStatsService.lossCustomerLast12Month(getAllUserIds());
                break;
            case "totalOpportunity":
                lineChartMonth = customerStatsService.totalOpportunityLast12Month(getAllUserIds());
                break;
            case "totalProfit":
// 25f0f5d5840f2b245eb6c888c458c9bc
                lineChartMonth = customerStatsService.totalProfitLast12Month(getAllUserIds());
                break;
            default:
                lineChartMonth = customerStatsService.totalCustomerLast12Month(getAllUserIds());

        }
        List<String> monthData = new ArrayList<>();
        List<String> totalData = new ArrayList<>();
        for (Map<String, Object> stringLongMap : lineChartMonth) {
            monthData.add(stringLongMap.get("month").toString());
            totalData.add(stringLongMap.get("count").toString());
        }
        map.put("monthData", monthData);
        map.put("totalData", totalData);
        return R.ok(map);
    }


    /**
     * 渠道客户饼图
     */
    @SaIgnore
    @GetMapping("/pieChartChannelCustomer")
    public R<Map<String, Object>> pieChartChannelCustomer() {
        Map<String, Object> result = new HashMap<>();
        List<Map<String, Object>> list = customerStatsService.channelCustomerCount(getAllUserIds());
        List<String> names = new ArrayList<>();
        for (Map<String, Object> map : list) {
            names.add(map.get("name").toString());
            map.put("value", map.get("count"));
        }
        result.put("names", names);
        result.put("data", list);
        return R.ok(result);
    }

    /**
     * 客户类型客户条形图
     */
    @SaIgnore
    @GetMapping("/barChartCusTypeCustomer")
    public R<Map<String, Object>> barChartCusTypeCustomer() {
        Map<String, Object> result = new HashMap<>();
        List<Map<String, Object>> list = customerStatsService.cusTypeCustomerCount(getAllUserIds());
        List<String> names = new ArrayList<>();
        List<String> datas = new ArrayList<>();
        for (Map<String, Object> map : list) {
            names.add(map.get("name").toString());
            datas.add(map.get("count").toString());
        }
        result.put("names", names);
        result.put("data", datas);
        return R.ok(result);
    }

    /**
     * 客户跟进阶段总数条形图
     */
    @SaIgnore
    @GetMapping("/barChartFollowStageCustomer")
    public R<Map<String, Object>> barChartFollowStageCustomer() {
        Map<String, Object> result = new HashMap<>();
        List<Map<String, Object>> list = customerStatsService.barChartFollowStageCustomer(getAllUserIds());
        List<String> names = new ArrayList<>();
        List<String> datas = new ArrayList<>();
        for (Map<String, Object> map : list) {
            names.add(map.get("name").toString());
            datas.add(map.get("count").toString());
        }
        result.put("names", names);
        result.put("data", datas);
        return R.ok(result);
    }


}
