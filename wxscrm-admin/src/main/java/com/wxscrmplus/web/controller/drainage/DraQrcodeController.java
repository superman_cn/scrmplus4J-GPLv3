package com.wxscrmplus.web.controller.drainage;

import java.util.List;
import java.util.Arrays;

import com.wxscrmplus.drainage.domain.bo.DraChannelBo;
import com.wxscrmplus.drainage.domain.vo.DraChannelVo;
import com.wxscrmplus.drainage.service.IDraChannelService;
import lombok.RequiredArgsConstructor;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.*;
import cn.dev33.satoken.annotation.SaMode;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import com.wxscrmplus.common.annotation.RepeatSubmit;
import com.wxscrmplus.common.annotation.Log;
import com.wxscrmplus.common.core.controller.BaseController;
import com.wxscrmplus.common.core.domain.PageQuery;
import com.wxscrmplus.common.core.domain.R;
import com.wxscrmplus.common.core.validate.AddGroup;
import com.wxscrmplus.common.core.validate.EditGroup;
import com.wxscrmplus.common.enums.BusinessType;
import com.wxscrmplus.common.utils.poi.ExcelUtil;
import com.wxscrmplus.drainage.domain.vo.DraQrcodeVo;
import com.wxscrmplus.drainage.domain.bo.DraQrcodeBo;
import com.wxscrmplus.drainage.service.IDraQrcodeService;
import com.wxscrmplus.common.core.page.TableDataInfo;

/**
 * 渠道活码
 *
 * @author 王永超
 * @date 2023-03-29
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/drainage/qrcode")
public class DraQrcodeController extends BaseController {

    private final IDraQrcodeService iDraQrcodeService;

    private final IDraChannelService iDraChannelService;

    /**
     * 获客渠道全部数据列表
     */
    @SaCheckPermission("drainage:qrcode:list")
    @GetMapping("/channel/channeDropDown")
    public TableDataInfo<DraChannelVo> channeDropDown(DraChannelBo bo) {
        List<DraChannelVo> list = iDraChannelService.queryList(bo);
        return TableDataInfo.build(list);
    }

    /**
     * 查询渠道活码全部数据列表
     */
    @SaCheckPermission(value = {"drainage:qrcode:listAll"}, mode = SaMode.OR)
    @GetMapping("/listAll")
    public TableDataInfo<DraQrcodeVo> listAll(DraQrcodeBo bo) {
        return TableDataInfo.build(iDraQrcodeService.queryList(bo));
    }

    /**
     * 查询渠道活码列表
     */
    @SaCheckPermission(value = {"drainage:qrcode:list"}, mode = SaMode.OR)
    @GetMapping("/list")
    public TableDataInfo<DraQrcodeVo> list(DraQrcodeBo bo, PageQuery pageQuery) {
        return iDraQrcodeService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出渠道活码列表
     */
    @SaCheckPermission(value = {"drainage:qrcode:export"}, mode = SaMode.OR)
    @Log(title = "渠道活码", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(DraQrcodeBo bo, HttpServletResponse response) {
        List<DraQrcodeVo> list = iDraQrcodeService.queryList(bo);
        ExcelUtil.exportExcel(list, "渠道活码", DraQrcodeVo.class, response);
    }

    /**
     * 获取渠道活码详细信息
     *
     * @param qrcodeId 主键
     */
    @SaCheckPermission(value = {"drainage:qrcode:query","drainage:qrcode:edit"}, mode = SaMode.OR)
    @GetMapping("/{qrcodeId}")
    public R<DraQrcodeVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long qrcodeId) {
        return R.ok(iDraQrcodeService.queryById(qrcodeId));
    }

    /**
     * 新增渠道活码
// fb4668478605d7490114ec445cafd188
     */
    @SaCheckPermission(value = {"drainage:qrcode:add"}, mode = SaMode.OR)
    @Log(title = "渠道活码", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody DraQrcodeBo bo) {
        return toAjax(iDraQrcodeService.insertByBo(bo));
    }

    /**
     * 修改渠道活码
     */
    @SaCheckPermission(value = {"drainage:qrcode:edit"}, mode = SaMode.OR)
    @Log(title = "渠道活码", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody DraQrcodeBo bo) {
        return toAjax(iDraQrcodeService.updateByBo(bo));
    }

    /**
     * 删除渠道活码
     *
     * @param qrcodeIds 主键串
     */
    @SaCheckPermission(value = {"drainage:qrcode:remove"}, mode = SaMode.OR)
    @Log(title = "渠道活码", businessType = BusinessType.DELETE)
    @DeleteMapping("/{qrcodeIds}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] qrcodeIds) {
        return toAjax(iDraQrcodeService.deleteWithValidByIds(Arrays.asList(qrcodeIds), true));
    }
}
