package com.wxscrmplus.web.controller.devtool;

import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.extra.pinyin.PinyinUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.wxscrmplus.common.annotation.Log;
import com.wxscrmplus.common.core.controller.BaseController;
import com.wxscrmplus.common.core.domain.PageQuery;
import com.wxscrmplus.common.core.domain.R;
import com.wxscrmplus.common.core.domain.entity.DevDictData;
import com.wxscrmplus.common.core.page.TableDataInfo;
import com.wxscrmplus.common.enums.BusinessType;
import com.wxscrmplus.common.utils.poi.ExcelUtil;
import com.wxscrmplus.devtool.mapper.DevDictDataMapper;
import com.wxscrmplus.devtool.service.IDevDictDataService;
import com.wxscrmplus.devtool.service.IDevDictTypeService;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * 数据字典信息
 *
 * @author www.wxscrmplus.com
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/devtool/dict/data")
public class DevDictDataController extends BaseController {

    private final IDevDictDataService dictDataService;
    private final DevDictDataMapper dictDataMapper;
    private final IDevDictTypeService dictTypeService;

    /**
     * 查询字典数据列表
     */
    @SaCheckPermission("devtool:dict:list")
    @GetMapping("/list")
    public TableDataInfo<DevDictData> list(DevDictData dictData, PageQuery pageQuery) {
        return dictDataService.selectPageDictDataList(dictData, pageQuery);
    }
// 4fa2bb878b881c224edb28ee5efd11f8

    /**
     * 导出字典数据列表
     */
    @Log(title = "字典数据", businessType = BusinessType.EXPORT)
    @SaCheckPermission("devtool:dict:export")
    @PostMapping("/export")
    public void export(DevDictData dictData, HttpServletResponse response) {
        List<DevDictData> list = dictDataService.selectDictDataList(dictData);
        ExcelUtil.exportExcel(list, "字典数据", DevDictData.class, response);
    }

    /**
     * 查询字典数据详细
     *
     * @param dictCode 字典code
     */
    @SaCheckPermission("devtool:dict:query")
    @GetMapping(value = "/{dictCode}")
    public R<DevDictData> getInfo(@PathVariable Long dictCode) {
        return R.ok(dictDataService.selectDictDataById(dictCode));
    }

    /**
     * 根据字典类型查询字典数据信息
     *
     * @param dictType 字典类型
     */
    @GetMapping(value = "/obtainDictValue/{dictType}")
    public R<String> obtainDictValue(@PathVariable String dictType, String dictLabel) {
        String dictValue = UUID.randomUUID().toString();
        if (StrUtil.isNotBlank(dictLabel)) {
            dictValue = PinyinUtil.getPinyin(dictLabel, "");
            Long count = dictDataMapper.selectCount(Wrappers.query(new DevDictData()).lambda().eq(DevDictData::getDictType, dictType).eq(DevDictData::getDictValue, dictValue));
            if (count > 0) {
                dictValue += count;
            }
        }
        return R.ok("",dictValue);
    }

    /**
     * 根据字典类型查询字典数据信息
     *
     * @param dictType 字典类型
     */
    @GetMapping(value = "/type/{dictType}")
    public R<List<DevDictData>> dictType(@PathVariable String dictType) {
        List<DevDictData> data = dictTypeService.selectDictDataByType(dictType);
        if (ObjectUtil.isNull(data)) {
            data = new ArrayList<>();
        }
        return R.ok(data);
    }

    /**
     * 新增字典类型
     */
    @SaCheckPermission("devtool:dict:add")
    @Log(title = "字典数据", businessType = BusinessType.INSERT)
    @PostMapping
    public R<Void> add(@Validated @RequestBody DevDictData dict) {
        dictDataService.insertDictData(dict);
        return R.ok();
    }

    /**
     * 修改保存字典类型
     */
    @SaCheckPermission("devtool:dict:edit")
    @Log(title = "字典数据", businessType = BusinessType.UPDATE)
    @PutMapping
    public R<Void> edit(@Validated @RequestBody DevDictData dict) {
        dictDataService.updateDictData(dict);
        return R.ok();
    }

    /**
     * 删除字典类型
     *
     * @param dictCodes 字典code串
     */
    @SaCheckPermission("devtool:dict:remove")
    @Log(title = "字典类型", businessType = BusinessType.DELETE)
    @DeleteMapping("/{dictCodes}")
    public R<Void> remove(@PathVariable Long[] dictCodes) {
        dictDataService.deleteDictDataByIds(dictCodes);
        return R.ok();
    }
}
