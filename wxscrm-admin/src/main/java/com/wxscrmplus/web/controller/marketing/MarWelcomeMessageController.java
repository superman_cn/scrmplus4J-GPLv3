package com.wxscrmplus.web.controller.marketing;

import cn.dev33.satoken.annotation.SaIgnore;

import java.util.Collections;
import java.util.List;
import java.util.Arrays;

import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.core.util.StrUtil;
import com.wxscrmplus.common.enums.QueryDataRange;
import com.wxscrmplus.common.utils.ServletUtils;
import com.wxscrmplus.marketing.domain.MarCategory;
import com.wxscrmplus.marketing.domain.MarMsgAttachments;
import com.wxscrmplus.marketing.domain.bo.MarCategoryBo;
import com.wxscrmplus.marketing.domain.bo.MarMsgAttachmentsBo;
import com.wxscrmplus.marketing.domain.vo.MarCategoryVo;
import com.wxscrmplus.marketing.domain.vo.MarMsgAttachmentsVo;
import com.wxscrmplus.marketing.mapper.MarWelcomeMessageMapper;
import com.wxscrmplus.marketing.service.IMarCategoryService;
import com.wxscrmplus.marketing.service.IMarMsgAttachmentsService;
import lombok.RequiredArgsConstructor;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.*;

import cn.dev33.satoken.annotation.SaMode;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import com.wxscrmplus.common.annotation.RepeatSubmit;
import com.wxscrmplus.common.annotation.Log;
import com.wxscrmplus.common.core.controller.BaseController;
import com.wxscrmplus.common.core.domain.PageQuery;
import com.wxscrmplus.common.core.domain.R;
import com.wxscrmplus.common.core.validate.AddGroup;
import com.wxscrmplus.common.core.validate.EditGroup;
import com.wxscrmplus.common.enums.BusinessType;
import com.wxscrmplus.common.utils.poi.ExcelUtil;
import com.wxscrmplus.marketing.domain.vo.MarWelcomeMessageVo;
import com.wxscrmplus.marketing.domain.bo.MarWelcomeMessageBo;
import com.wxscrmplus.marketing.service.IMarWelcomeMessageService;
import com.wxscrmplus.common.core.page.TableDataInfo;

/**
 * 欢迎语
 *
 * @author 王永超
 * @date 2023-05-07
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/marketing/welcomeMessage")
public class MarWelcomeMessageController extends BaseController {

    private final IMarWelcomeMessageService iMarWelcomeMessageService;
    private final MarWelcomeMessageMapper marWelcomeMessageMapper;

    private final IMarMsgAttachmentsService iMarMsgAttachmentsService;
    private final IMarCategoryService iMarCategoryService;


    /**
     * 回显欢迎语选择器值
     *
     * @param welcomeMsgIds 主键串
     */
    @SaIgnore
    @GetMapping("/echoSelect/{welcomeMsgIds}")
    public R<List<MarWelcomeMessageVo>> echoSelect(@NotEmpty(message = "主键不能为空")
                                                   @PathVariable Long[] welcomeMsgIds) {
        List<MarWelcomeMessageVo> list = marWelcomeMessageMapper.selectVoBatchIds(Arrays.asList(welcomeMsgIds));
        return R.ok(list);
    }

    /**
     * 查询欢迎语全部数据列表
     */
    @SaCheckPermission(value = {"marketing:welcomeMessage:listAll"}, mode = SaMode.OR)
    @GetMapping("/listAll")
    public TableDataInfo<MarWelcomeMessageVo> listAll(MarWelcomeMessageBo bo) {
        return TableDataInfo.build(iMarWelcomeMessageService.queryList(bo));
    }

    /**
     * 查询欢迎语列表
     */
    @SaCheckPermission(value = {"marketing:welcomeMessage:list"}, mode = SaMode.OR)
    @GetMapping("/list")
    public TableDataInfo<MarWelcomeMessageVo> list(MarWelcomeMessageBo bo, PageQuery pageQuery) {
        List<Long> dataRangeUserIds = Collections.singletonList(getUserId());
        String queryDataRange = ServletUtils.getParameter("queryDataRange");
        if (StrUtil.isNotBlank(queryDataRange)) {
// 885bd600f21e219df10d8d74e7a88a0d
            if (queryDataRange.equals(QueryDataRange.BENBUMENDE.getValue())) {
                StpUtil.checkPermission("marketing:welcomeMessage:list:dept");
                dataRangeUserIds = getDeptUserIds();
            } else if (queryDataRange.equals(QueryDataRange.SUOYOUDE.getValue())) {
                StpUtil.checkPermission("marketing:welcomeMessage:list:all");
                dataRangeUserIds = getAllUserIds();
            }
        }
        bo.setDataRangeUserIds(dataRangeUserIds);
        return iMarWelcomeMessageService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出欢迎语列表
     */
    @SaCheckPermission(value = {"marketing:welcomeMessage:export"}, mode = SaMode.OR)
    @Log(title = "欢迎语", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(MarWelcomeMessageBo bo, HttpServletResponse response) {
        List<MarWelcomeMessageVo> list = iMarWelcomeMessageService.queryList(bo);
        ExcelUtil.exportExcel(list, "欢迎语", MarWelcomeMessageVo.class, response);
    }

    /**
     * 获取欢迎语详细信息
     *
     * @param welcomeMsgId 主键
     */
    @SaCheckPermission(value = {"marketing:welcomeMessage:query", "marketing:welcomeMessage:edit"}, mode = SaMode.OR)
    @GetMapping("/{welcomeMsgId}")
    public R<MarWelcomeMessageVo> getInfo(@NotNull(message = "主键不能为空")
                                          @PathVariable Long welcomeMsgId) {
        return R.ok(iMarWelcomeMessageService.queryById(welcomeMsgId));
    }

    /**
     * 新增欢迎语
     */
    @SaCheckPermission(value = {"marketing:welcomeMessage:add"}, mode = SaMode.OR)
    @Log(title = "欢迎语", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody MarWelcomeMessageBo bo) {
        return toAjax(iMarWelcomeMessageService.insertByBo(bo));
    }

    /**
     * 修改欢迎语
     */
    @SaCheckPermission(value = {"marketing:welcomeMessage:edit"}, mode = SaMode.OR)
    @Log(title = "欢迎语", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody MarWelcomeMessageBo bo) {
        return toAjax(iMarWelcomeMessageService.updateByBo(bo));
    }

    /**
     * 删除欢迎语
     *
     * @param welcomeMsgIds 主键串
     */
    @SaCheckPermission(value = {"marketing:welcomeMessage:remove"}, mode = SaMode.OR)
    @Log(title = "欢迎语", businessType = BusinessType.DELETE)
    @DeleteMapping("/{welcomeMsgIds}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] welcomeMsgIds) {
        return toAjax(iMarWelcomeMessageService.deleteWithValidByIds(Arrays.asList(welcomeMsgIds), true));
    }

    //-----------------------分类---------------------------

    /**
     * 查询分类管理列表
     */
    @SaCheckPermission(value = {"marketing:welcomeMessage:category:list"}, mode = SaMode.OR)
    @GetMapping("/category/list")
    public R<List<MarCategoryVo>> listCategory(MarCategoryBo bo) {
        bo.setCategoryType(MarCategory.CategoryTypeFieldEnums.HUANYINGYU.getValue());
        List<MarCategoryVo> list = iMarCategoryService.queryList(bo);
        return R.ok(list);
    }

    /**
     * 获取分类管理详细信息
     *
     * @param categoryId 主键
     */
    @SaCheckPermission(value = {"marketing:welcomeMessage:category:query", "marketing:welcomeMessage:category:edit"}, mode = SaMode.OR)
    @GetMapping("/category/{categoryId}")
    public R<MarCategoryVo> getCategoryInfo(@NotNull(message = "主键不能为空")
                                            @PathVariable Long categoryId) {
        return R.ok(iMarCategoryService.queryById(categoryId));
    }

    /**
     * 新增分类管理
     */
    @SaCheckPermission(value = {"marketing:welcomeMessage:category:add"}, mode = SaMode.OR)
    @Log(title = "分类管理", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping("/category")
    public R<Void> addCategory(@Validated(AddGroup.class) @RequestBody MarCategoryBo bo) {
        bo.setCategoryType(MarCategory.CategoryTypeFieldEnums.HUANYINGYU.getValue());
        return toAjax(iMarCategoryService.insertByBo(bo));
    }

    /**
     * 修改分类管理
     */
    @SaCheckPermission(value = {"marketing:welcomeMessage:category:edit"}, mode = SaMode.OR)
    @Log(title = "分类管理", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping("/category")
    public R<Void> editCategory(@Validated(EditGroup.class) @RequestBody MarCategoryBo bo) {
        bo.setCategoryType(MarCategory.CategoryTypeFieldEnums.HUANYINGYU.getValue());
        return toAjax(iMarCategoryService.updateByBo(bo));
    }

    /**
     * 删除分类管理
     *
     * @param categoryIds 主键串
     */
    @SaCheckPermission(value = {"marketing:welcomeMessage:category:remove"}, mode = SaMode.OR)
    @Log(title = "分类管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/category/{categoryIds}")
    public R<Void> removeCategory(@NotEmpty(message = "主键不能为空")
                                  @PathVariable Long[] categoryIds) {
        return toAjax(iMarCategoryService.deleteWithValidByIds(Arrays.asList(categoryIds), true));
    }
    //--------------附件管理--------------

    /**
     * 查询欢迎语附件列表
     */
    @SaCheckPermission(value = {"marketing:welcomeMessage:attachments:list"}, mode = SaMode.OR)
    @GetMapping("/attachments/list")
    public TableDataInfo<MarMsgAttachmentsVo> listAttachments(MarMsgAttachmentsBo bo, PageQuery pageQuery) {
        bo.setTargetType(MarMsgAttachments.TargetTypeFieldEnums.ONE.getValue());
        return iMarMsgAttachmentsService.queryPageList(bo, pageQuery);
    }


    /**
     * 获取欢迎语附件详细信息
     *
     * @param msgAttId 主键
     */
    @SaCheckPermission(value = {"marketing:welcomeMessage:attachments:query", "marketing:welcomeMessage:attachments:edit"}, mode = SaMode.OR)
    @GetMapping("/attachments/{msgAttId}")
    public R<MarMsgAttachmentsVo> getAttachmentsInfo(@NotNull(message = "主键不能为空")
                                                     @PathVariable Long msgAttId) {
        return R.ok(iMarMsgAttachmentsService.queryById(msgAttId));
    }

    /**
     * 新增欢迎语附件
     */
    @SaCheckPermission(value = {"marketing:welcomeMessage:attachments:add"}, mode = SaMode.OR)
    @Log(title = "欢迎语附件", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping("/attachments")
    public R<Void> addAttachments(@Validated(AddGroup.class) @RequestBody MarMsgAttachmentsBo bo) {
        bo.setTargetType(MarMsgAttachments.TargetTypeFieldEnums.ONE.getValue());
        return toAjax(iMarMsgAttachmentsService.insertByBo(bo));
    }

    /**
     * 修改欢迎语附件
     */
    @SaCheckPermission(value = {"marketing:welcomeMessage:attachments:edit"}, mode = SaMode.OR)
    @Log(title = "欢迎语附件", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping("/attachments")
    public R<Void> editAttachments(@Validated(EditGroup.class) @RequestBody MarMsgAttachmentsBo bo) {
        bo.setTargetType(MarMsgAttachments.TargetTypeFieldEnums.ONE.getValue());
        return toAjax(iMarMsgAttachmentsService.updateByBo(bo));
    }

    /**
     * 删除欢迎语附件
     *
     * @param msgAttIds 主键串
     */
    @SaCheckPermission(value = {"marketing:welcomeMessage:attachments:remove"}, mode = SaMode.OR)
    @Log(title = "欢迎语附件", businessType = BusinessType.DELETE)
    @DeleteMapping("/attachments/{msgAttIds}")
    public R<Void> removeAttachments(@NotEmpty(message = "主键不能为空")
                                     @PathVariable Long[] msgAttIds) {
        return toAjax(iMarMsgAttachmentsService.deleteWithValidByIds(Arrays.asList(msgAttIds), true));
    }
}
