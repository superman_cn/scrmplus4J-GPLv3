package com.wxscrmplus.web.controller.customer;

import java.util.Collections;
import java.util.List;
import java.util.Arrays;

import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.core.util.StrUtil;
import com.wxscrmplus.common.enums.QueryDataRange;
import com.wxscrmplus.common.utils.ServletUtils;
import lombok.RequiredArgsConstructor;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.*;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import com.wxscrmplus.common.annotation.Log;
import com.wxscrmplus.common.core.controller.BaseController;
import com.wxscrmplus.common.core.domain.PageQuery;
import com.wxscrmplus.common.core.domain.R;
import com.wxscrmplus.common.enums.BusinessType;
import com.wxscrmplus.common.utils.poi.ExcelUtil;
import com.wxscrmplus.customer.domain.vo.FollowRecordVo;
import com.wxscrmplus.customer.domain.bo.FollowRecordBo;
import com.wxscrmplus.customer.service.IFollowRecordService;
import com.wxscrmplus.common.core.page.TableDataInfo;

/**
 * 跟进记录
 *
 * @author 王永超
 * @date 2023-03-25
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/customer/followRecord")
public class FollowRecordController extends BaseController {

    private final IFollowRecordService iFollowRecordService;

    /**
     * 查询跟进记录列表
     */
    @SaCheckPermission("customer:followRecord:list")
    @GetMapping("/list")
    public TableDataInfo<FollowRecordVo> list(FollowRecordBo bo, PageQuery pageQuery) {
        List<Long> userIds = Collections.singletonList(getUserId());
        String queryDataRange = ServletUtils.getParameter("queryDataRange");
        if (StrUtil.isNotBlank(queryDataRange)) {
            if (queryDataRange.equals(QueryDataRange.BENBUMENDE.getValue())) {
                StpUtil.checkPermission("customer:followRecord:list:dept");
                userIds = getDeptUserIds();
            } else if (queryDataRange.equals(QueryDataRange.SUOYOUDE.getValue())) {
                StpUtil.checkPermission("customer:followRecord:list:all");
                userIds = getAllUserIds();
            }
        }
        bo.setDataRangeUserIds(userIds);
        return iFollowRecordService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出跟进记录列表
     */
    @SaCheckPermission("customer:followRecord:export")
    @Log(title = "跟进记录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(FollowRecordBo bo, HttpServletResponse response) {
        List<FollowRecordVo> list = iFollowRecordService.queryList(bo);
        ExcelUtil.exportExcel(list, "跟进记录", FollowRecordVo.class, response);
    }

    /**
     * 获取跟进记录详细信息
     *
     * @param followRecordId 主键
     */
    @SaCheckPermission("customer:followRecord:query")
    @GetMapping("/{followRecordId}")
// 38ae0afac7affd5d5bb45152e565d9ac
    public R<FollowRecordVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long followRecordId) {
        return R.ok(iFollowRecordService.queryById(followRecordId));
    }

    /**
     * 删除跟进记录
     *
     * @param followRecordIds 主键串
     */
    @SaCheckPermission("customer:followRecord:remove")
    @Log(title = "跟进记录", businessType = BusinessType.DELETE)
    @DeleteMapping("/{followRecordIds}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] followRecordIds) {
        return toAjax(iFollowRecordService.deleteWithValidByIds(Arrays.asList(followRecordIds), true));
    }
}
