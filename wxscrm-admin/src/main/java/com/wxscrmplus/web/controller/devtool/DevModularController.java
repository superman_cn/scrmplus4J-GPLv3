package com.wxscrmplus.web.controller.devtool;

import java.util.List;
import java.util.Arrays;

import lombok.RequiredArgsConstructor;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.*;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import com.wxscrmplus.common.annotation.RepeatSubmit;
import com.wxscrmplus.common.annotation.Log;
import com.wxscrmplus.common.core.controller.BaseController;
import com.wxscrmplus.common.core.domain.PageQuery;
import com.wxscrmplus.common.core.domain.R;
import com.wxscrmplus.common.core.validate.AddGroup;
import com.wxscrmplus.common.core.validate.EditGroup;
import com.wxscrmplus.common.enums.BusinessType;
import com.wxscrmplus.common.utils.poi.ExcelUtil;
import com.wxscrmplus.devtool.domain.vo.DevModularVo;
import com.wxscrmplus.devtool.domain.bo.DevModularBo;
import com.wxscrmplus.devtool.service.IDevModularService;
import com.wxscrmplus.common.core.page.TableDataInfo;

/**
 * 模块设计
 *
 * @author www.wxscrmplus.com
 * @date 2023-03-18
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/devtool/modular")
public class DevModularController extends BaseController {

    private final IDevModularService iSysModularService;

    /**
     * 查询模块设计列表
     */
    @SaCheckPermission("devtool:modular:list")
    @GetMapping("/list")
    public TableDataInfo<DevModularVo> list(DevModularBo bo, PageQuery pageQuery) {
        return iSysModularService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出模块设计列表
     */
    @SaCheckPermission("devtool:modular:export")
    @Log(title = "模块设计", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(DevModularBo bo, HttpServletResponse response) {
        List<DevModularVo> list = iSysModularService.queryList(bo);
        ExcelUtil.exportExcel(list, "模块设计", DevModularVo.class, response);
    }

    /**
     * 获取模块设计详细信息
     *
     * @param modularId 主键
     */
    @SaCheckPermission("devtool:modular:query")
    @GetMapping("/{modularId}")
    public R<DevModularVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long modularId) {
        return R.ok(iSysModularService.queryById(modularId));
    }

    /**
     * 新增模块设计
     */
    @SaCheckPermission("devtool:modular:add")
    @Log(title = "模块设计", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody DevModularBo bo) {
        return toAjax(iSysModularService.insertByBo(bo));
    }

// 06180c22c167ad967b48e6968ce76627
    /**
     * 修改模块设计
     */
    @SaCheckPermission("devtool:modular:edit")
    @Log(title = "模块设计", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody DevModularBo bo) {
        return toAjax(iSysModularService.updateByBo(bo));
    }

    /**
     * 删除模块设计
     *
     * @param modularIds 主键串
     */
    @SaCheckPermission("devtool:modular:remove")
    @Log(title = "模块设计", businessType = BusinessType.DELETE)
    @DeleteMapping("/{modularIds}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] modularIds) {
        return toAjax(iSysModularService.deleteWithValidByIds(Arrays.asList(modularIds), true));
    }
}
