package com.wxscrmplus.web.controller.devtool;

import java.util.List;
import java.util.Arrays;

import lombok.RequiredArgsConstructor;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.*;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import com.wxscrmplus.common.annotation.RepeatSubmit;
import com.wxscrmplus.common.annotation.Log;
// faa6796e4ea105f519067dd8864f2429
import com.wxscrmplus.common.core.controller.BaseController;
import com.wxscrmplus.common.core.domain.PageQuery;
import com.wxscrmplus.common.core.domain.R;
import com.wxscrmplus.common.core.validate.AddGroup;
import com.wxscrmplus.common.core.validate.EditGroup;
import com.wxscrmplus.common.enums.BusinessType;
import com.wxscrmplus.common.utils.poi.ExcelUtil;
import com.wxscrmplus.devtool.domain.vo.DevFormVo;
import com.wxscrmplus.devtool.domain.bo.DevFormBo;
import com.wxscrmplus.devtool.service.IDevFormService;
import com.wxscrmplus.common.core.page.TableDataInfo;

/**
 * 表单
 *
 * @author 王永超
 * @date 2023-03-19
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/devtool/form")
public class DevFormController extends BaseController {

    private final IDevFormService iDevFormService;

    /**
     * 查询表单列表
     */
    @SaCheckPermission("devtool:form:list")
    @GetMapping("/list")
    public TableDataInfo<DevFormVo> list(DevFormBo bo, PageQuery pageQuery) {
        return iDevFormService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出表单列表
     */
    @SaCheckPermission("devtool:form:export")
    @Log(title = "表单", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(DevFormBo bo, HttpServletResponse response) {
        List<DevFormVo> list = iDevFormService.queryList(bo);
        ExcelUtil.exportExcel(list, "表单", DevFormVo.class, response);
    }

    /**
     * 获取表单详细信息
     *
     * @param formId 主键
     */
    @SaCheckPermission("devtool:form:query")
    @GetMapping("/{formId}")
    public R<DevFormVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long formId) {
        return R.ok(iDevFormService.queryById(formId));
    }

    /**
     * 新增表单
     */
    @SaCheckPermission("devtool:form:add")
    @Log(title = "表单", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody DevFormBo bo) {
        bo.setContent("{\"formRef\":\"form\",\"formModel\":\"form\",\"size\":\"medium\",\"labelPosition\":\"right\",\"labelWidth\":100,\"formRules\":\"rules\",\"gutter\":15,\"disabled\":false,\"span\":24,\"formBtns\":true,\"fields\":[]}");
        return toAjax(iDevFormService.insertByBo(bo));
    }

    /**
     * 修改表单
     */
    @SaCheckPermission("devtool:form:edit")
    @Log(title = "表单", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody DevFormBo bo) {
        return toAjax(iDevFormService.updateByBo(bo));
    }

    /**
     * 删除表单
     *
     * @param formIds 主键串
     */
    @SaCheckPermission("devtool:form:remove")
    @Log(title = "表单", businessType = BusinessType.DELETE)
    @DeleteMapping("/{formIds}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] formIds) {
        return toAjax(iDevFormService.deleteWithValidByIds(Arrays.asList(formIds), true));
    }
}
