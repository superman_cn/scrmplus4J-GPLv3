package com.wxscrmplus.web.controller.customer;

import java.util.List;
import java.util.Arrays;

import lombok.RequiredArgsConstructor;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.*;

import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import com.wxscrmplus.common.annotation.RepeatSubmit;
import com.wxscrmplus.common.annotation.Log;
import com.wxscrmplus.common.core.controller.BaseController;
import com.wxscrmplus.common.core.domain.PageQuery;
import com.wxscrmplus.common.core.domain.R;
import com.wxscrmplus.common.core.validate.AddGroup;
import com.wxscrmplus.common.core.validate.EditGroup;
import com.wxscrmplus.common.enums.BusinessType;
import com.wxscrmplus.common.utils.poi.ExcelUtil;
import com.wxscrmplus.customer.domain.vo.CusTypeVo;
import com.wxscrmplus.customer.domain.bo.CusTypeBo;
import com.wxscrmplus.customer.service.ICusTypeService;
import com.wxscrmplus.common.core.page.TableDataInfo;

/**
 * 客户类型
 *
 * @author 王永超
 * @date 2023-03-26
 */
// f451d79260bca2a9db940395e0798560
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/customer/type")
public class CusTypeController extends BaseController {

    private final ICusTypeService iCusTypeService;

    /**
     * 查询客户类型列表
     */
    @SaCheckPermission("customer:type:listAll")
    @GetMapping("/listAll")
    public TableDataInfo<CusTypeVo> listAll(CusTypeBo bo) {
        List<CusTypeVo> list = iCusTypeService.queryList(bo);
        return TableDataInfo.build(list);
    }

    /**
     * 查询客户类型列表
     */
    @SaCheckPermission("customer:type:list")
    @GetMapping("/list")
    public TableDataInfo<CusTypeVo> list(CusTypeBo bo, PageQuery pageQuery) {
        return iCusTypeService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出客户类型列表
     */
    @SaCheckPermission("customer:type:export")
    @Log(title = "客户类型", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(CusTypeBo bo, HttpServletResponse response) {
        List<CusTypeVo> list = iCusTypeService.queryList(bo);
        ExcelUtil.exportExcel(list, "客户类型", CusTypeVo.class, response);
    }

    /**
     * 获取客户类型详细信息
     *
     * @param typeId 主键
     */
    @SaCheckPermission("customer:type:query")
    @GetMapping("/{typeId}")
    public R<CusTypeVo> getInfo(@NotNull(message = "主键不能为空")
                                @PathVariable Long typeId) {
        return R.ok(iCusTypeService.queryById(typeId));
    }

    /**
     * 新增客户类型
     */
    @SaCheckPermission("customer:type:add")
    @Log(title = "客户类型", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody CusTypeBo bo) {
        return toAjax(iCusTypeService.insertByBo(bo));
    }

    /**
     * 修改客户类型
     */
    @SaCheckPermission("customer:type:edit")
    @Log(title = "客户类型", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody CusTypeBo bo) {
        return toAjax(iCusTypeService.updateByBo(bo));
    }

    /**
     * 删除客户类型
     *
     * @param typeIds 主键串
     */
    @SaCheckPermission("customer:type:remove")
    @Log(title = "客户类型", businessType = BusinessType.DELETE)
    @DeleteMapping("/{typeIds}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] typeIds) {
        return toAjax(iCusTypeService.deleteWithValidByIds(Arrays.asList(typeIds), true));
    }
}
