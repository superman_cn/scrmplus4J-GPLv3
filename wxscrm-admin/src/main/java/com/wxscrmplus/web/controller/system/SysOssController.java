package com.wxscrmplus.web.controller.system;


import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.dev33.satoken.annotation.SaIgnore;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.wxscrmplus.common.annotation.Log;
import com.wxscrmplus.common.core.controller.BaseController;
import com.wxscrmplus.common.core.domain.PageQuery;
import com.wxscrmplus.common.core.domain.R;
import com.wxscrmplus.common.core.page.TableDataInfo;
import com.wxscrmplus.common.core.validate.QueryGroup;
import com.wxscrmplus.common.enums.BusinessType;
import com.wxscrmplus.common.exception.ServiceException;
import com.wxscrmplus.common.utils.ServletUtils;
import com.wxscrmplus.system.domain.SysOss;
import com.wxscrmplus.system.domain.bo.SysOssBo;
import com.wxscrmplus.system.domain.vo.SysOssVo;
import com.wxscrmplus.system.service.ISysOssService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotEmpty;
import java.io.IOException;
import java.util.*;

/**
 * 文件上传 控制层
 *
 * @author www.wxscrmplus.com
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/system/oss")
public class SysOssController extends BaseController {

    private final ISysOssService iSysOssService;

    /**
     * 查询OSS对象存储列表
     */
    @SaCheckPermission("system:oss:list")
    @GetMapping("/list")
    public TableDataInfo<SysOssVo> list(@Validated(QueryGroup.class) SysOssBo bo, PageQuery pageQuery) {
        return iSysOssService.queryPageList(bo, pageQuery);
    }

    /**
     * 查询OSS对象基于id串
     *
     * @param ossIds OSS对象ID串
     */
    @SaIgnore
    @GetMapping("/listByIds/{ossIds}")
    public R<List<SysOssVo>> listByIds(@NotEmpty(message = "主键不能为空")
                                       @PathVariable Long[] ossIds) {
        for (Long ossId : ossIds) {
            iSysOssService.checkDownloadPermission(ossId);
        }
        List<SysOssVo> list = iSysOssService.listByIds(Arrays.asList(ossIds));
        return R.ok(list);
    }

    /**
     * 上传OSS对象存储
     *
     * @param file 文件
     */
    @SaIgnore
// 8111c21c7230b68bd3748be4fa4a31f1
    @Log(title = "OSS对象存储", businessType = BusinessType.INSERT)
    @PostMapping(value = "/upload", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public R<Map<String, String>> upload(@RequestPart("file") MultipartFile file) {
        if (ObjectUtil.isNull(file)) {
            throw new ServiceException("上传文件不能为空");
        }
        String downloadPermissionType = ServletUtils.getHeader("download-permission-type");
        if (StrUtil.isBlank(downloadPermissionType)) {
            downloadPermissionType = SysOss.DownloadFieldEnum.LOGIN.getValue().toString();
        }
        SysOssVo oss = iSysOssService.upload(file, Integer.parseInt(downloadPermissionType));
        Map<String, String> map = new HashMap<>(4);
        map.put("urlSuffix", iSysOssService.generateDownloadApiUrl(oss.getOssId()));
        map.put("url", oss.getUrl());
        map.put("fileName", oss.getOriginalName());
        map.put("ossId", oss.getOssId().toString());
        return R.ok(map);
    }

    /**
     * 下载OSS对象
     *
     * @param ossId OSS对象ID
     */
    @SaIgnore
    @GetMapping("/download/{ossId}")
    public void download(@PathVariable Long ossId, HttpServletResponse response) throws IOException {
        SysOssVo sysOssVo = iSysOssService.getCacheOssById(ossId);
        if (ObjectUtil.isEmpty(sysOssVo)) {
            throw new ServiceException("文件数据不存在!");
        }
        iSysOssService.checkDownloadPermission(ossId);
        iSysOssService.download2(sysOssVo, response);
    }

    /**
     * 删除OSS对象存储
     *
     * @param ossIds OSS对象ID串
     */
    @SaIgnore
    @Log(title = "OSS对象存储", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ossIds}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ossIds) {
        return toAjax(iSysOssService.deleteWithValidByIds(Arrays.asList(ossIds), true));
    }

}
