package com.wxscrmplus.web.controller.marketing;

import cn.dev33.satoken.annotation.SaIgnore;

import java.util.Collections;
import java.util.List;
import java.util.Arrays;

import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.core.util.StrUtil;
import com.wxscrmplus.common.enums.QueryDataRange;
import com.wxscrmplus.common.utils.ServletUtils;
import com.wxscrmplus.marketing.domain.MarCategory;
import com.wxscrmplus.marketing.domain.bo.MarCategoryBo;
import com.wxscrmplus.marketing.domain.vo.MarCategoryVo;
import com.wxscrmplus.marketing.mapper.MarCorpusMapper;
import com.wxscrmplus.marketing.service.IMarCategoryService;
import lombok.RequiredArgsConstructor;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.*;

import cn.dev33.satoken.annotation.SaMode;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import com.wxscrmplus.common.annotation.RepeatSubmit;
import com.wxscrmplus.common.annotation.Log;
import com.wxscrmplus.common.core.controller.BaseController;
import com.wxscrmplus.common.core.domain.PageQuery;
import com.wxscrmplus.common.core.domain.R;
import com.wxscrmplus.common.core.validate.AddGroup;
import com.wxscrmplus.common.core.validate.EditGroup;
import com.wxscrmplus.common.enums.BusinessType;
import com.wxscrmplus.common.utils.poi.ExcelUtil;
import com.wxscrmplus.marketing.domain.vo.MarCorpusVo;
import com.wxscrmplus.marketing.domain.bo.MarCorpusBo;
import com.wxscrmplus.marketing.service.IMarCorpusService;
import com.wxscrmplus.common.core.page.TableDataInfo;

/**
 * 话术库
 *
 * @author 王永超
 * @date 2023-05-02
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/marketing/corpus")
public class MarCorpusController extends BaseController {
// f7e59cd5229c55e3f3538159082c64bd

    private final IMarCorpusService iMarCorpusService;
    private final MarCorpusMapper marCorpusMapper;

    private final IMarCategoryService iMarCategoryService;


    /**
     * 回显话术库选择器值
     *
     * @param corpusIds 主键串
     */
    @SaIgnore
    @GetMapping("/echoSelect/{corpusIds}")
    public R<List<MarCorpusVo>> echoSelect(@NotEmpty(message = "主键不能为空")
                                           @PathVariable Long[] corpusIds) {
        List<MarCorpusVo> list = marCorpusMapper.selectVoBatchIds(Arrays.asList(corpusIds));
        return R.ok(list);
    }

    /**
     * 查询话术库全部数据列表
     */
    @SaCheckPermission(value = {"marketing:corpus:listAll"}, mode = SaMode.OR)
    @GetMapping("/listAll")
    public TableDataInfo<MarCorpusVo> listAll(MarCorpusBo bo) {
        return TableDataInfo.build(iMarCorpusService.queryList(bo));
    }

    /**
     * 查询话术库列表
     */
    @SaCheckPermission(value = {"marketing:corpus:list"}, mode = SaMode.OR)
    @GetMapping("/list")
    public TableDataInfo<MarCorpusVo> list(MarCorpusBo bo, PageQuery pageQuery) {
        List<Long> dataRangeUserIds = Collections.singletonList(getUserId());
        String queryDataRange = ServletUtils.getParameter("queryDataRange");
        if (StrUtil.isNotBlank(queryDataRange)) {
            if (queryDataRange.equals(QueryDataRange.BENBUMENDE.getValue())) {
                StpUtil.checkPermission("marketing:corpus:list:dept");
                dataRangeUserIds = getDeptUserIds();
            } else if (queryDataRange.equals(QueryDataRange.SUOYOUDE.getValue())) {
                StpUtil.checkPermission("marketing:corpus:list:all");
                dataRangeUserIds = getAllUserIds();
            }
        }
        bo.setDataRangeUserIds(dataRangeUserIds);
        return iMarCorpusService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出话术库列表
     */
    @SaCheckPermission(value = {"marketing:corpus:export"}, mode = SaMode.OR)
    @Log(title = "话术库", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(MarCorpusBo bo, HttpServletResponse response) {
        List<MarCorpusVo> list = iMarCorpusService.queryList(bo);
        ExcelUtil.exportExcel(list, "话术库", MarCorpusVo.class, response);
    }

    /**
     * 获取话术库详细信息
     *
     * @param corpusId 主键
     */
    @SaCheckPermission(value = {"marketing:corpus:query", "marketing:corpus:edit"}, mode = SaMode.OR)
    @GetMapping("/{corpusId}")
    public R<MarCorpusVo> getInfo(@NotNull(message = "主键不能为空")
                                  @PathVariable Long corpusId) {
        return R.ok(iMarCorpusService.queryById(corpusId));
    }

    /**
     * 新增话术库
     */
    @SaCheckPermission(value = {"marketing:corpus:add"}, mode = SaMode.OR)
    @Log(title = "话术库", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody MarCorpusBo bo) {
        return toAjax(iMarCorpusService.insertByBo(bo));
    }

    /**
     * 修改话术库
     */
    @SaCheckPermission(value = {"marketing:corpus:edit"}, mode = SaMode.OR)
    @Log(title = "话术库", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody MarCorpusBo bo) {
        return toAjax(iMarCorpusService.updateByBo(bo));
    }

    /**
     * 删除话术库
     *
     * @param corpusIds 主键串
     */
    @SaCheckPermission(value = {"marketing:corpus:remove"}, mode = SaMode.OR)
    @Log(title = "话术库", businessType = BusinessType.DELETE)
    @DeleteMapping("/{corpusIds}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] corpusIds) {
        return toAjax(iMarCorpusService.deleteWithValidByIds(Arrays.asList(corpusIds), true));
    }

    //-----------------------分类---------------------------

    /**
     * 查询分类管理列表
     */
    @SaCheckPermission(value = {"marketing:corpus:category:list"}, mode = SaMode.OR)
    @GetMapping("/category/list")
    public R<List<MarCategoryVo>> listCategory(MarCategoryBo bo) {
        bo.setCategoryType(MarCategory.CategoryTypeFieldEnums.HUASHU.getValue());
        List<MarCategoryVo> list = iMarCategoryService.queryList(bo);
        return R.ok(list);
    }

    /**
     * 获取分类管理详细信息
     *
     * @param categoryId 主键
     */
    @SaCheckPermission(value = {"marketing:corpus:category:query", "marketing:corpus:category:edit"}, mode = SaMode.OR)
    @GetMapping("/category/{categoryId}")
    public R<MarCategoryVo> getCategoryInfo(@NotNull(message = "主键不能为空")
                                            @PathVariable Long categoryId) {
        return R.ok(iMarCategoryService.queryById(categoryId));
    }

    /**
     * 新增分类管理
     */
    @SaCheckPermission(value = {"marketing:corpus:category:add"}, mode = SaMode.OR)
    @Log(title = "分类管理", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping("/category")
    public R<Void> addCategory(@Validated(AddGroup.class) @RequestBody MarCategoryBo bo) {
        bo.setCategoryType(MarCategory.CategoryTypeFieldEnums.HUASHU.getValue());
        return toAjax(iMarCategoryService.insertByBo(bo));
    }

    /**
     * 修改分类管理
     */
    @SaCheckPermission(value = {"marketing:corpus:category:edit"}, mode = SaMode.OR)
    @Log(title = "分类管理", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping("/category")
    public R<Void> editCategory(@Validated(EditGroup.class) @RequestBody MarCategoryBo bo) {
        bo.setCategoryType(MarCategory.CategoryTypeFieldEnums.HUASHU.getValue());
        return toAjax(iMarCategoryService.updateByBo(bo));
    }

    /**
     * 删除分类管理
     *
     * @param categoryIds 主键串
     */
    @SaCheckPermission(value = {"marketing:corpus:category:remove"}, mode = SaMode.OR)
    @Log(title = "分类管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/category/{categoryIds}")
    public R<Void> removeCategory(@NotEmpty(message = "主键不能为空")
                                  @PathVariable Long[] categoryIds) {
        return toAjax(iMarCategoryService.deleteWithValidByIds(Arrays.asList(categoryIds), true));
    }
}
