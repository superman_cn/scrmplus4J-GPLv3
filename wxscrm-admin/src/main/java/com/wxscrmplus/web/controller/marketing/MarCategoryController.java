package com.wxscrmplus.web.controller.marketing;

import cn.dev33.satoken.annotation.SaIgnore;
import java.util.List;
import java.util.Arrays;

import com.wxscrmplus.common.core.page.TableDataInfo;
import com.wxscrmplus.marketing.mapper.MarCategoryMapper;
import lombok.RequiredArgsConstructor;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.*;

import cn.dev33.satoken.annotation.SaMode;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import com.wxscrmplus.common.annotation.RepeatSubmit;
import com.wxscrmplus.common.annotation.Log;
import com.wxscrmplus.common.core.controller.BaseController;
import com.wxscrmplus.common.core.domain.R;
import com.wxscrmplus.common.core.validate.AddGroup;
import com.wxscrmplus.common.core.validate.EditGroup;
import com.wxscrmplus.common.enums.BusinessType;
import com.wxscrmplus.common.utils.poi.ExcelUtil;
import com.wxscrmplus.marketing.domain.vo.MarCategoryVo;
import com.wxscrmplus.marketing.domain.bo.MarCategoryBo;
import com.wxscrmplus.marketing.service.IMarCategoryService;

/**
 * 分类管理
 *
 * @author 王永超
 * @date 2023-05-02
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/marketing/category")
public class MarCategoryController extends BaseController {

    private final IMarCategoryService iMarCategoryService;
    private final MarCategoryMapper marCategoryMapper;


    /**
     * 回显分类管理选择器值
     *
     * @param categoryIds 主键串
     */
    @SaIgnore
    @GetMapping("/echoSelect/{categoryIds}")
    public R<List<MarCategoryVo>> echoSelect(@NotEmpty(message = "主键不能为空")
                                                        @PathVariable Long[] categoryIds) {
        List<MarCategoryVo> list = marCategoryMapper.selectVoBatchIds(Arrays.asList(categoryIds));
        return R.ok(list);
    }

    /**
     * 查询分类管理全部数据列表
     */
    @SaCheckPermission(value = {"marketing:category:listAll"}, mode = SaMode.OR)
    @GetMapping("/listAll")
    public TableDataInfo<MarCategoryVo> listAll(MarCategoryBo bo) {
        return TableDataInfo.build(iMarCategoryService.queryList(bo));
    }

    /**
     * 查询分类管理列表
     */
    @SaCheckPermission(value = {"marketing:category:list"}, mode = SaMode.OR)
    @GetMapping("/list")
    public R<List<MarCategoryVo>> list(MarCategoryBo bo) {
        List<MarCategoryVo> list = iMarCategoryService.queryList(bo);
        return R.ok(list);
    }

    /**
     * 导出分类管理列表
     */
    @SaCheckPermission(value = {"marketing:category:export"}, mode = SaMode.OR)
    @Log(title = "分类管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(MarCategoryBo bo, HttpServletResponse response) {
        List<MarCategoryVo> list = iMarCategoryService.queryList(bo);
// 4bf0bade79dd02478746ef27fe250b61
        ExcelUtil.exportExcel(list, "分类管理", MarCategoryVo.class, response);
    }

    /**
     * 获取分类管理详细信息
     *
     * @param categoryId 主键
     */
    @SaCheckPermission(value = {"marketing:category:query","marketing:category:edit"}, mode = SaMode.OR)
    @GetMapping("/{categoryId}")
    public R<MarCategoryVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long categoryId) {
        return R.ok(iMarCategoryService.queryById(categoryId));
    }

    /**
     * 新增分类管理
     */
    @SaCheckPermission(value = {"marketing:category:add"}, mode = SaMode.OR)
    @Log(title = "分类管理", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody MarCategoryBo bo) {
        return toAjax(iMarCategoryService.insertByBo(bo));
    }

    /**
     * 修改分类管理
     */
    @SaCheckPermission(value = {"marketing:category:edit"}, mode = SaMode.OR)
    @Log(title = "分类管理", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody MarCategoryBo bo) {
        return toAjax(iMarCategoryService.updateByBo(bo));
    }

    /**
     * 删除分类管理
     *
     * @param categoryIds 主键串
     */
    @SaCheckPermission(value = {"marketing:category:remove"}, mode = SaMode.OR)
    @Log(title = "分类管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{categoryIds}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] categoryIds) {
        return toAjax(iMarCategoryService.deleteWithValidByIds(Arrays.asList(categoryIds), true));
    }
}
