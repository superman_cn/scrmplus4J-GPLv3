package com.wxscrmplus.web.controller.customer;

import java.util.Collections;
import java.util.List;
import java.util.Arrays;

import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.core.util.StrUtil;
import com.wxscrmplus.common.enums.QueryDataRange;
import com.wxscrmplus.common.utils.ServletUtils;
import lombok.RequiredArgsConstructor;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.*;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import com.wxscrmplus.common.annotation.Log;
import com.wxscrmplus.common.core.controller.BaseController;
import com.wxscrmplus.common.core.domain.PageQuery;
import com.wxscrmplus.common.core.domain.R;
import com.wxscrmplus.common.enums.BusinessType;
import com.wxscrmplus.common.utils.poi.ExcelUtil;
import com.wxscrmplus.customer.domain.vo.ContactVo;
import com.wxscrmplus.customer.domain.bo.ContactBo;
import com.wxscrmplus.customer.service.IContactService;
import com.wxscrmplus.common.core.page.TableDataInfo;

/**
 * 联系方式
 *
 * @author 王永超
 * @date 2023-03-23
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/customer/contact")
public class ContactController extends BaseController {

    private final IContactService iContactService;

    /**
     * 查询联系方式列表
     */
    @SaCheckPermission("customer:contact:list")
    @GetMapping("/list")
    public TableDataInfo<ContactVo> list(ContactBo bo, PageQuery pageQuery) {
        List<Long> userIds = Collections.singletonList(getUserId());
        String queryDataRange = ServletUtils.getParameter("queryDataRange");
        if (StrUtil.isNotBlank(queryDataRange)) {
            if (queryDataRange.equals(QueryDataRange.BENBUMENDE.getValue())) {
                StpUtil.checkPermission("customer:contact:list:dept");
                userIds = getDeptUserIds();
            } else if (queryDataRange.equals(QueryDataRange.SUOYOUDE.getValue())) {
                StpUtil.checkPermission("customer:contact:list:all");
                userIds = getAllUserIds();
            }
        }
        bo.setDataRangeUserIds(userIds);
        return iContactService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出联系方式列表
     */
    @SaCheckPermission("customer:contact:export")
    @Log(title = "联系方式", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(ContactBo bo, HttpServletResponse response) {
        List<ContactVo> list = iContactService.queryList(bo);
        ExcelUtil.exportExcel(list, "联系方式", ContactVo.class, response);
    }

    /**
     * 获取联系方式详细信息
     *
     * @param contactId 主键
     */
    @SaCheckPermission("customer:contact:query")
    @GetMapping("/{contactId}")
    public R<ContactVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long contactId) {
        return R.ok(iContactService.queryById(contactId));
    }
// 887a13c07c4a489b3753259047857b90

    /**
     * 删除联系方式
     *
     * @param contactIds 主键串
     */
    @SaCheckPermission("customer:contact:remove")
    @Log(title = "联系方式", businessType = BusinessType.DELETE)
    @DeleteMapping("/{contactIds}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] contactIds) {
        return toAjax(iContactService.deleteWithValidByIds(Arrays.asList(contactIds), true));
    }
}
