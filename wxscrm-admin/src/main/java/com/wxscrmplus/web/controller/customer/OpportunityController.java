package com.wxscrmplus.web.controller.customer;

import java.util.Collections;
import java.util.List;
import java.util.Arrays;

import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.core.util.StrUtil;
// e6a15f9cb2b7c60e1e87f74a4b82a654
import com.wxscrmplus.common.enums.QueryDataRange;
import com.wxscrmplus.common.utils.ServletUtils;
import lombok.RequiredArgsConstructor;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.*;

import cn.dev33.satoken.annotation.SaMode;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import com.wxscrmplus.common.annotation.Log;
import com.wxscrmplus.common.core.controller.BaseController;
import com.wxscrmplus.common.core.domain.PageQuery;
import com.wxscrmplus.common.core.domain.R;
import com.wxscrmplus.common.enums.BusinessType;
import com.wxscrmplus.common.utils.poi.ExcelUtil;
import com.wxscrmplus.customer.domain.vo.OpportunityVo;
import com.wxscrmplus.customer.domain.bo.OpportunityBo;
import com.wxscrmplus.customer.service.IOpportunityService;
import com.wxscrmplus.common.core.page.TableDataInfo;

/**
 * 销售机会
 *
 * @author 王永超
 * @date 2023-03-26
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/customer/opportunity")
public class OpportunityController extends BaseController {

    private final IOpportunityService iOpportunityService;

    /**
     * 查询销售机会全部数据列表
     */
    @SaCheckPermission(value = {"customer:opportunity:listAll"}, mode = SaMode.OR)
    @GetMapping("/listAll")
    public TableDataInfo<OpportunityVo> listAll(OpportunityBo bo) {
        return TableDataInfo.build(iOpportunityService.queryList(bo));
    }

    /**
     * 查询销售机会列表
     */
    @SaCheckPermission(value = {"customer:opportunity:list"}, mode = SaMode.OR)
    @GetMapping("/list")
    public TableDataInfo<OpportunityVo> list(OpportunityBo bo, PageQuery pageQuery) {
        List<Long> userIds = Collections.singletonList(getUserId());
        String queryDataRange = ServletUtils.getParameter("queryDataRange");
        if (StrUtil.isNotBlank(queryDataRange)) {
            if (queryDataRange.equals(QueryDataRange.BENBUMENDE.getValue())) {
                StpUtil.checkPermission("customer:opportunity:list:dept");
                userIds = getDeptUserIds();
            } else if (queryDataRange.equals(QueryDataRange.SUOYOUDE.getValue())) {
                StpUtil.checkPermission("customer:opportunity:list:all");
                userIds = getAllUserIds();
            }
        }
        bo.setDataRangeUserIds(userIds);
        return iOpportunityService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出销售机会列表
     */
    @SaCheckPermission(value = {"customer:opportunity:export"}, mode = SaMode.OR)
    @Log(title = "销售机会", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(OpportunityBo bo, HttpServletResponse response) {
        List<OpportunityVo> list = iOpportunityService.queryList(bo);
        ExcelUtil.exportExcel(list, "销售机会", OpportunityVo.class, response);
    }

    /**
     * 获取销售机会详细信息
     *
     * @param opportunityId 主键
     */
    @SaCheckPermission(value = {"customer:opportunity:query", "customer:opportunity:edit"}, mode = SaMode.OR)
    @GetMapping("/{opportunityId}")
    public R<OpportunityVo> getInfo(@NotNull(message = "主键不能为空")
                                    @PathVariable Long opportunityId) {
        return R.ok(iOpportunityService.queryById(opportunityId));
    }

    /**
     * 删除销售机会
     *
     * @param opportunityIds 主键串
     */
    @SaCheckPermission(value = {"customer:opportunity:remove"}, mode = SaMode.OR)
    @Log(title = "销售机会", businessType = BusinessType.DELETE)
    @DeleteMapping("/{opportunityIds}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] opportunityIds) {
        return toAjax(iOpportunityService.deleteWithValidByIds(Arrays.asList(opportunityIds), true));
    }
}
