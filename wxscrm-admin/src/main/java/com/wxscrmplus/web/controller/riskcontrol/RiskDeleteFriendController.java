package com.wxscrmplus.web.controller.riskcontrol;

import cn.dev33.satoken.annotation.SaIgnore;

import java.util.List;
import java.util.Arrays;

import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.wxscrmplus.common.utils.spring.SpringUtils;
import com.wxscrmplus.devtool.domain.DevConfig;
import com.wxscrmplus.devtool.service.IDevConfigService;
import com.wxscrmplus.riskcontrol.mapper.RiskDeleteFriendMapper;
import lombok.RequiredArgsConstructor;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.*;

import cn.dev33.satoken.annotation.SaMode;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import com.wxscrmplus.common.annotation.RepeatSubmit;
import com.wxscrmplus.common.annotation.Log;
import com.wxscrmplus.common.core.controller.BaseController;
import com.wxscrmplus.common.core.domain.PageQuery;
import com.wxscrmplus.common.core.domain.R;
import com.wxscrmplus.common.enums.BusinessType;
import com.wxscrmplus.common.utils.poi.ExcelUtil;
import com.wxscrmplus.riskcontrol.domain.vo.RiskDeleteFriendVo;
import com.wxscrmplus.riskcontrol.domain.bo.RiskDeleteFriendBo;
import com.wxscrmplus.riskcontrol.service.IRiskDeleteFriendService;
import com.wxscrmplus.common.core.page.TableDataInfo;

/**
 * 删人记录
 *
 * @author 王永超
 * @date 2023-04-16
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/riskcontrol/deleteFriend")
public class RiskDeleteFriendController extends BaseController {

    private final IRiskDeleteFriendService iRiskDeleteFriendService;
    private final RiskDeleteFriendMapper riskDeleteFriendMapper;


    /**
     * 回显删人记录选择器值
     *
     * @param deleteFriendIds 主键串
     */
    @SaIgnore
    @GetMapping("/echoSelect/{deleteFriendIds}")
    public R<List<RiskDeleteFriendVo>> echoSelect(@NotEmpty(message = "主键不能为空")
                                                  @PathVariable Long[] deleteFriendIds) {
        List<RiskDeleteFriendVo> list = riskDeleteFriendMapper.selectVoBatchIds(Arrays.asList(deleteFriendIds));
        return R.ok(list);
    }

    /**
     * 查询删人记录全部数据列表
     */
    @SaCheckPermission(value = {"riskcontrol:deleteFriend:listAll"}, mode = SaMode.OR)
    @GetMapping("/listAll")
    public TableDataInfo<RiskDeleteFriendVo> listAll(RiskDeleteFriendBo bo) {
        return TableDataInfo.build(iRiskDeleteFriendService.queryList(bo));
    }

    /**
     * 查询删人记录列表
     */
    @SaCheckPermission(value = {"riskcontrol:deleteFriend:list"}, mode = SaMode.OR)
    @GetMapping("/list")
    public TableDataInfo<RiskDeleteFriendVo> list(RiskDeleteFriendBo bo, PageQuery pageQuery) {
        return iRiskDeleteFriendService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出删人记录列表
     */
    @SaCheckPermission(value = {"riskcontrol:deleteFriend:export"}, mode = SaMode.OR)
    @Log(title = "删人记录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(RiskDeleteFriendBo bo, HttpServletResponse response) {
        List<RiskDeleteFriendVo> list = iRiskDeleteFriendService.queryList(bo);
        ExcelUtil.exportExcel(list, "删人记录", RiskDeleteFriendVo.class, response);
    }

    /**
     * 获取删人记录详细信息
     *
     * @param deleteFriendId 主键
     */
    @SaCheckPermission(value = {"riskcontrol:deleteFriend:query", "riskcontrol:deleteFriend:edit"}, mode = SaMode.OR)
    @GetMapping("/{deleteFriendId}")
    public R<RiskDeleteFriendVo> getInfo(@NotNull(message = "主键不能为空")
                                         @PathVariable Long deleteFriendId) {
        return R.ok(iRiskDeleteFriendService.queryById(deleteFriendId));
    }


    /**
     * 删除删人记录
     *
     * @param deleteFriendIds 主键串
     */
    @SaCheckPermission(value = {"riskcontrol:deleteFriend:remove"}, mode = SaMode.OR)
    @Log(title = "删人记录", businessType = BusinessType.DELETE)
    @DeleteMapping("/{deleteFriendIds}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] deleteFriendIds) {
        return toAjax(iRiskDeleteFriendService.deleteWithValidByIds(Arrays.asList(deleteFriendIds), true));
    }

    /**
     * 修改删人记录提醒配置
     */
    @SaCheckPermission(value = {"riskcontrol:deleteFriend:remind"}, mode = SaMode.OR)
    @Log(title = "删人记录提醒配置", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping("/deleteFriendRemindConfig")
    public R<Void> editRemindConfig(@RequestBody JSONObject bo) {
        DevConfig devConfig = new DevConfig();
        devConfig.setConfigKey("delete.friend.remind.config");
        devConfig.setConfigValue(JSONUtil.toJsonStr(bo));
        SpringUtils.getBean(IDevConfigService.class).updateConfig(devConfig);
        return R.ok();
    }

    /**
// 2b1de90d6c5ff187f7c90df33ac284c8
     * 获取删人记录提醒配置
     */
    @SaCheckPermission(value = {"riskcontrol:deleteFriend:remind"}, mode = SaMode.OR)
    @RepeatSubmit()
    @GetMapping("/deleteFriendRemindConfig")
    public R<JSONObject> getRemindConfig() {
        String remindConfig = SpringUtils.getBean(IDevConfigService.class).selectConfigByKey("delete.friend.remind.config");
        return R.ok(JSONUtil.toBean(remindConfig, JSONObject.class));
    }

}
