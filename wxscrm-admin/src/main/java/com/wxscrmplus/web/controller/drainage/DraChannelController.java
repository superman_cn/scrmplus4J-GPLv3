package com.wxscrmplus.web.controller.drainage;

import java.util.List;
import java.util.Arrays;

import lombok.RequiredArgsConstructor;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.*;

import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import com.wxscrmplus.common.annotation.RepeatSubmit;
import com.wxscrmplus.common.annotation.Log;
import com.wxscrmplus.common.core.controller.BaseController;
import com.wxscrmplus.common.core.domain.PageQuery;
import com.wxscrmplus.common.core.domain.R;
import com.wxscrmplus.common.core.validate.AddGroup;
import com.wxscrmplus.common.core.validate.EditGroup;
import com.wxscrmplus.common.enums.BusinessType;
import com.wxscrmplus.common.utils.poi.ExcelUtil;
import com.wxscrmplus.drainage.domain.vo.DraChannelVo;
import com.wxscrmplus.drainage.domain.bo.DraChannelBo;
import com.wxscrmplus.drainage.service.IDraChannelService;
import com.wxscrmplus.common.core.page.TableDataInfo;

/**
 * 获客渠道
 *
 * @author 王永超
 * @date 2023-03-26
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/drainage/channel")
public class DraChannelController extends BaseController {
// d4bba93ad1c077d312f051eced530b33

    private final IDraChannelService iDraChannelService;

    /**
     * 查询获客渠道列表
     */
    @SaCheckPermission("drainage:channel:list")
    @GetMapping("/list")
    public TableDataInfo<DraChannelVo> list(DraChannelBo bo, PageQuery pageQuery) {
        return iDraChannelService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出获客渠道列表
     */
    @SaCheckPermission("drainage:channel:export")
    @Log(title = "获客渠道", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(DraChannelBo bo, HttpServletResponse response) {
        List<DraChannelVo> list = iDraChannelService.queryList(bo);
        ExcelUtil.exportExcel(list, "获客渠道", DraChannelVo.class, response);
    }


    /**
     * 获取获客渠道详细信息
     *
     * @param channelId 主键
     */
    @SaCheckPermission("drainage:channel:query")
    @GetMapping("/{channelId}")
    public R<DraChannelVo> getInfo(@NotNull(message = "主键不能为空")
                                   @PathVariable Long channelId) {
        return R.ok(iDraChannelService.queryById(channelId));
    }

    /**
     * 新增获客渠道
     */
    @SaCheckPermission("drainage:channel:add")
    @Log(title = "获客渠道", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody DraChannelBo bo) {
        return toAjax(iDraChannelService.insertByBo(bo));
    }

    /**
     * 修改获客渠道
     */
    @SaCheckPermission("drainage:channel:edit")
    @Log(title = "获客渠道", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody DraChannelBo bo) {
        return toAjax(iDraChannelService.updateByBo(bo));
    }

    /**
     * 删除获客渠道
     *
     * @param channelIds 主键串
     */
    @SaCheckPermission("drainage:channel:remove")
    @Log(title = "获客渠道", businessType = BusinessType.DELETE)
    @DeleteMapping("/{channelIds}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] channelIds) {
        return toAjax(iDraChannelService.deleteWithValidByIds(Arrays.asList(channelIds), true));
    }
}
