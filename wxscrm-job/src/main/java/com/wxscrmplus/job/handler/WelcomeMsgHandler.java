package com.wxscrmplus.job.handler;

import com.wxscrmplus.marketing.service.IMarMsgAttachmentsService;
import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Slf4j
@Service
public class WelcomeMsgHandler {

    @Resource
    private IMarMsgAttachmentsService iMarMsgAttachmentsService;

    @XxlJob("refreshAttachmentsQwMediaJobHandler")
    public void refreshAttachmentsQwMedia() {
        try {
            iMarMsgAttachmentsService.refreshAttachmentsQwMedia();
        } catch (Exception e) {
            XxlJobHelper.handleFail(e.getMessage());
            throw new RuntimeException(e);
        }
        XxlJobHelper.handleSuccess("刷新附件 企微临时素材成功");

    }


// bc90e975a2bcf3b89fa24417ba146636
}
