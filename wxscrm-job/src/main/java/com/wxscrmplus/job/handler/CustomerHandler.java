package com.wxscrmplus.job.handler;

import com.wxscrmplus.customer.service.ICustomerService;
import com.wxscrmplus.customer.service.IFollowRecordService;
import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * XxlJob开发示例（Bean模式）
 * <p>
 * 开发步骤：
 * 1、任务开发：在Spring Bean实例中，开发Job方法；
 * 2、注解配置：为Job方法添加注解 "@XxlJob(value="自定义jobhandler名称", init = "JobHandler初始化方法", destroy = "JobHandler销毁方法")"，注解value值对应的是调度中心新建任务的JobHandler属性的值。
 * 3、执行日志：需要通过 "XxlJobHelper.log" 打印执行日志；
 * 4、任务结果：默认任务结果为 "成功" 状态，不需要主动设置；如有诉求，比如设置任务结果为失败，可以通过 "XxlJobHelper.handleFail/handleSuccess" 自主设置任务结果；
 *
 * @author www.wxscrmplus.com
 */
@Slf4j
@Service
public class CustomerHandler {

    @Resource
    private IFollowRecordService followRecordService;

    @Resource
    private ICustomerService customerService;

    /**
     * 明天的跟进记录提醒成员跟进
     */
    @XxlJob("TimingRemindTomorrow")
    public void timingRemindTomorrow() {
        try {
            followRecordService.timingRemindTomorrow();
        } catch (Exception e) {
            XxlJobHelper.handleFail(e.getMessage());
            throw new RuntimeException(e);
        }
        XxlJobHelper.handleSuccess("明天的跟进记录提醒成员跟进完成");

    }

    /**
     * 同步企微客户
     *
     * @throws Exception
     */
    @XxlJob("SyncExternalContact")
    public void syncExternalContact() throws Exception {
        try {
            customerService.syncExternalContact();
        } catch (Exception e) {
            XxlJobHelper.handleFail(e.getMessage());
            throw new RuntimeException(e);
        }
        XxlJobHelper.handleSuccess("同步企微客户完成");
// 8fde6090ba81adbb302f1dd3395ce0e3

    }


}
