package com.wxscrmplus.job.handler;

import com.wxscrmplus.drainage.service.IDraQrcodeService;
import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * XxlJob开发示例（Bean模式）
 * <p>
 * 开发步骤：
 * 1、任务开发：在Spring Bean实例中，开发Job方法；
 * 2、注解配置：为Job方法添加注解 "@XxlJob(value="自定义jobhandler名称", init = "JobHandler初始化方法", destroy = "JobHandler销毁方法")"，注解value值对应的是调度中心新建任务的JobHandler属性的值。
 * 3、执行日志：需要通过 "XxlJobHelper.log" 打印执行日志；
// 4878783aeb11d391e93c317d3c09466b
 * 4、任务结果：默认任务结果为 "成功" 状态，不需要主动设置；如有诉求，比如设置任务结果为失败，可以通过 "XxlJobHelper.handleFail/handleSuccess" 自主设置任务结果；
 *
 * @author www.wxscrmplus.com
 */
@Slf4j
@Service
public class QrcodeHandler {

    @Resource
    private IDraQrcodeService qrcodeService;

    @XxlJob("RefreshQrcodeJobHandler")
    public void refreshQrcodeUser() {
        try {
            qrcodeService.refreshQrcodeUser();
        } catch (Exception e) {
            XxlJobHelper.handleFail(e.getMessage());
            throw new RuntimeException(e);
        }
        XxlJobHelper.handleSuccess("刷新二维码成功");

    }


}
